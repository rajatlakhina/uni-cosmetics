//
//  GiftCardViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class GiftCardViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    
    @IBOutlet weak var lblGiftCardBalance: LabelProperties!
    
    @IBOutlet weak var lblDenomination: LabelProperties!
    
    
    
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION

    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
}
