//
//  CatagoriesListViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class CatagoriesListViewController: UIViewControllerBase, LeftMenuViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewCatagoriesList: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    var catagoriesList: [ProductCatagories]! = nil;
    var cat_id:String = "";
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnCart.isHidden = true;
        self.vwHeader.btnSearch.isHidden = true;
        self.vwHeader.btnNotification.isHidden = true;
        self.tableViewCatagoriesList.isHidden = true;
        self.tableViewCatagoriesList.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell");
        
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getCatagoriesList();
    }
    
    func getCatagoriesList()
    {
        self.startLoader();
        ApiHelper.getCategoriesList(lang: "en", cat_id: self.cat_id, controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.catagoriesList = data;
            self.tableViewCatagoriesList.reloadData();
            self.tableViewCatagoriesList.isHidden = false;
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnSeeMoreAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        vc.catagoriesList = self.catagoriesList;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if !CommonFunctions.isNull(s: self.catagoriesList)
        {
            var count:Double = 0;
            if self.catagoriesList.count % 2 == 0
            {
                count = Double(self.catagoriesList.count / 2)
            }
            else
            {
                count = Double((self.catagoriesList.count / 2) + 1);
            }
            
            let row = ceil(count);
            
            let height = ((tableView.frame.size.width - 20) / 2);
            
            return CGFloat((CGFloat(row) * height) + 55);
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.selectionStyle = .none;
        
        cell.vwHeader.isHidden = true;
        
        cell.lblBtnTitle.text = "See All";
        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: ((indexPath.section ) + 1));
        cell.btnSeeMore.tag = ((indexPath.section) + 1);
        
        cell.btnSeeMore.addTarget(self, action: #selector(self.btnSeeMoreAction(_:)), for: .touchUpInside);
        return cell
    }
    //MARK:- COLLECTION VIEW DELEGATE AND DATASOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if !CommonFunctions.isNull(s: self.catagoriesList)
        {
            return self.catagoriesList.count;
        }
        else
        {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatagoryCollectionViewCell", for: indexPath) as! ProductCatagoryCollectionViewCell;
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        
        let perfume = self.catagoriesList[indexPath.item];
        
        let url = perfume.banner;
        let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
        
        cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
        
        cell.imgProduct.backgroundColor = UIColor.black.withAlphaComponent(0.05);
        
        cell.lblProduct.text = String(htmlEncodedString: perfume.name);
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        vc.sub_cat_id = self.catagoriesList[indexPath.item].id;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width - 20) / 2, height: (collectionView.frame.width - 30) / 2);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(5, 7, 5, 7);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
}
