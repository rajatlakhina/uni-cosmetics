//
//  IntroductionViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 13/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class IntroductionViewController: UIViewControllerBase
{
    //MARK:- OUTLETS
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LIFE CYCLE
    
    //MARK:- Button action
    
    @IBAction func btnLoginAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
        
        self.navigationController?.pushViewController(vc, animated: true);
        
    }
    
    @IBAction func btnRegisterAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnSkipAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
}
