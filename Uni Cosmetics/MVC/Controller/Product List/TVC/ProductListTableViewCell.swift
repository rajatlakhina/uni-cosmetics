//
//  ProductListTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProduct: LabelProperties!
    @IBOutlet weak var lblCurrentPrice: LabelProperties!
    
    @IBOutlet weak var lblEarlierPrice: LabelProperties!
    
    @IBOutlet weak var vwDiscount: UIView!
    @IBOutlet weak var lblDiscount: LabelProperties!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
