//
//  ProductListViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit
import SDWebImage

class ProductListViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLET
    
    @IBOutlet weak var tableViewProductList: UITableView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
   
    
    //MARK:- VARIABLE DECLARATION
    
    var isFilterSelected:Bool = false;
    var catagoriesList: [ProductCatagories]! = nil;
    var subCatagoriesList: ProductCatagories! = nil
    var sub_cat_id:String = "";
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.tableViewProductList.register(UINib(nibName: "ProductListTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductListTableViewCell");
        self.tableViewProductList.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductHeaderTableViewCell");
        
        self.tableViewProductList.register(UINib(nibName: "ProductSortOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductSortOptionTableViewCell");
        
        self.vwHeader.btnNotification.setImage(UIImage(named:"ic_filter"), for: .normal)
        
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.vwHeader.btnNotification.addTarget(self, action: #selector(self.btnFilterAction(_:)), for: .touchUpInside);
        self.vwHeader.btnSearch.isHidden = true;
        self.tableViewProductList.isHidden = true;
        
        if CommonFunctions.isEmpty(s: self.sub_cat_id, checkWhitespace: true)
        {
            self.tableViewProductList.isHidden = false;
            self.tableViewProductList.reloadData()
        }
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        
        if !CommonFunctions.isEmpty(s: self.sub_cat_id, checkWhitespace: true)
        {
            self.getProductList()
        }
        else
        {
            self.tableViewProductList.isHidden = false;
        }
    }
    
    func getProductList()
    {
        self.startLoader();
        ApiHelper.getProductList(lang: "en", cat_id: self.sub_cat_id, controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.subCatagoriesList = data;
            self.tableViewProductList.reloadData();
            self.tableViewProductList.isHidden = false;
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION HANDLER
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton)
    {
        self.isFilterSelected = !isFilterSelected;
        self.tableViewProductList.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true);
        self.tableViewProductList.reloadData();
    }
    
    @IBAction func btnShowFilterAction(_ sender: UIButton)
    {
        let y:CGFloat = 0;
        
        UIView.animate(withDuration: 0.6, animations:
            {
                
            BlurBackGroundView.show(controller: self, y: y, alpha: 1.0, color: .clear);
            if sender.tag == 1002
            {
                self.filterView.isHidden = false;
                self.filterView.alpha = 1.0;
                self.view.bringSubview(toFront: self.filterView);
            }
            else if sender.tag == 2002
            {
                self.sortByView.isHidden = false;
                self.sortByView.alpha = 1.0;
                self.view.bringSubview(toFront: self.sortByView);
            }
            
        }, completion:
            {
            finished in
            if sender.tag == 1002
            {
                self.filterView.isHidden = false;
                self.filterView.alpha = 1.0;
                self.view.bringSubview(toFront: self.filterView);
            }
            else if sender.tag == 2002
            {
                self.sortByView.isHidden = false;
                self.sortByView.alpha = 1.0;
                self.view.bringSubview(toFront: self.sortByView);
            }
        })
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if !CommonFunctions.isNull(s: self.catagoriesList)
        {
            if self.isFilterSelected
            {
                return (self.catagoriesList.count + 1);
            }
            else
            {
                return (self.catagoriesList.count);
            }
        }
        else if !CommonFunctions.isNull(s: self.subCatagoriesList)
        {
            if self.isFilterSelected
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.isFilterSelected
        {
            if section == 0
            {
                return 0;
            }
            else
            {
                if !CommonFunctions.isNull(s: self.catagoriesList)
                {
                    return 60;
                }
                else
                {
                    return 0;
                }
            }
        }
        else
        {
            if !CommonFunctions.isNull(s: self.catagoriesList)
            {
                return 60;
            }
            else
            {
                return 0;
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if !self.isFilterSelected
        {
            if indexPath.section == 0
            {
                return 60;
            }
            else
            {
                if !CommonFunctions.isNull(s: self.catagoriesList)
                {
                    return 100
                }
                else if !CommonFunctions.isNull(s: self.subCatagoriesList)
                {
                    return 170
                }
                else
                {
                    return (0)
                }
            }
        }
        else
        {
            if !CommonFunctions.isNull(s: self.catagoriesList)
            {
                return 100
            }
            else if !CommonFunctions.isNull(s: self.subCatagoriesList)
            {
                return 170
            }
            else
            {
                return (0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isFilterSelected
        {
            if indexPath.section == 0
            {
                return 60;
            }
            else
            {
                if !CommonFunctions.isNull(s: self.catagoriesList)
                {
                    return UITableViewAutomaticDimension
                }
                else if !CommonFunctions.isNull(s: self.subCatagoriesList)
                {
                    return UITableViewAutomaticDimension
                }
                else
                {
                    return (0)
                }
            }
        }
        else
        {
            if !CommonFunctions.isNull(s: self.catagoriesList)
            {
                return UITableViewAutomaticDimension
            }
            else if !CommonFunctions.isNull(s: self.subCatagoriesList)
            {
                return UITableViewAutomaticDimension
            }
            else
            {
                return (0)
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isFilterSelected
        {
            if section == 0
            {
                return 1
            }
            else
            {
                if !CommonFunctions.isNull(s: self.catagoriesList)
                {
                    return catagoriesList[section - 1].product.count;
                }
                else if !CommonFunctions.isNull(s: self.subCatagoriesList)
                {
                    return self.subCatagoriesList.product.count
                }
                else
                {
                    return 0
                }
            }
        }
        else
        {
            if !CommonFunctions.isNull(s: self.catagoriesList)
            {
                return catagoriesList[section].product.count;
            }
            else if !CommonFunctions.isNull(s: self.subCatagoriesList)
            {
                return self.subCatagoriesList.product.count
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerSection = tableView.dequeueReusableCell(withIdentifier: "ProductHeaderTableViewCell") as! ProductHeaderTableViewCell
        
        headerSection.centralhorizontallyCostraint.isActive = true
        headerSection.bottomConstraint.isActive = false;
        
        headerSection.lblTitle.text = String(htmlEncodedString: self.catagoriesList[section].name);
        
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.isFilterSelected
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSortOptionTableViewCell") as! ProductSortOptionTableViewCell
                cell.selectionStyle = .none;
                cell.btnFilter.tag = 1000 + indexPath.section + 2;
                cell.btnSortBy.tag = 2000 + indexPath.section + 2;
                
                cell.btnFilter.addTarget(self, action: #selector(self.btnShowFilterAction(_:)), for: .touchUpInside);
                
                cell.btnSortBy.addTarget(self, action: #selector(self.btnShowFilterAction(_:)), for: .touchUpInside);
                
                if IS_STANDARD_IPHONE_X
                {
                    cell.btnSortBy.imageEdgeInsets = UIEdgeInsetsMake(0, 205, 0, 0);
                }
                else if IS_STANDARD_IPHONE_X_R || IS_STANDARD_IPHONE_6_PLUS
                {
                    cell.btnSortBy.imageEdgeInsets = UIEdgeInsetsMake(0, 230, 0, 0);
                }
                else if IS_iPHONE_5
                {
                    cell.btnSortBy.imageEdgeInsets = UIEdgeInsetsMake(0, 180, 0, 0);
                }
                else
                {
                    cell.btnSortBy.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 25);
                    cell.btnSortBy.imageEdgeInsets = UIEdgeInsetsMake(0, 205, 0, 0);
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListTableViewCell") as! ProductListTableViewCell;
                cell.selectionStyle = .none;
                
                var products = Product();
                if !CommonFunctions.isNull(s: self.catagoriesList) && !CommonFunctions.isNull(s: self.catagoriesList[indexPath.section - 1].product)
                {
                    products = self.catagoriesList[indexPath.section - 1].product[indexPath.row]
                }
                else
                {
                    products = self.subCatagoriesList.product[indexPath.row]
                }
                
                let url = products.images[0];
                let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
                
                cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
                
                cell.lblProduct.text = String(htmlEncodedString: products.item_name);
                
                if CommonFunctions.isEmpty(s: products.sale_price, checkWhitespace: true) || products.sale_price == "0"
                {
                    cell.lblEarlierPrice.isHidden = true;
                    
                    let price:Double = Double(products.price)!;
                    cell.lblCurrentPrice.text = String(format: "%.2f %@", price, _currency_symbol);
                }
                else
                {
                    cell.lblEarlierPrice.isHidden = false;
                    let sale_price = Double(products.sale_price)!;
                    cell.lblCurrentPrice.text = String(format: "%.2f %@", sale_price, _currency_symbol);
                    let current_price = Double(products.price)!;
                    
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", current_price, _currency_symbol));
                    
                    attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
                    
                    cell.lblEarlierPrice.attributedText = attributeString;
                }
                
                if products.sale_percentage == "0"
                {
                    cell.vwDiscount.isHidden = true;
                    cell.lblProduct.textInsets = UIEdgeInsetsMake(1, 1, 1, 1)
                }
                else
                {
                    cell.vwDiscount.isHidden = false;
                    cell.lblDiscount.text = products.sale_percentage;
                    cell.lblProduct.textInsets = UIEdgeInsetsMake(1, 1, 1, 10)
                }
                
                return cell;
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListTableViewCell") as! ProductListTableViewCell;
            cell.selectionStyle = .none;
            
            var products = Product();
            if !CommonFunctions.isNull(s: self.catagoriesList) && !CommonFunctions.isNull(s: self.catagoriesList[indexPath.section].product)
            {
                products = self.catagoriesList[indexPath.section].product[indexPath.row]
            }
            else
            {
                products = self.subCatagoriesList.product[indexPath.row]
            }
            
            let url = products.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            cell.lblProduct.text = String(htmlEncodedString: products.item_name);
            
            if CommonFunctions.isEmpty(s: products.sale_price, checkWhitespace: true) || products.sale_price == "0"
            {
                cell.lblEarlierPrice.isHidden = true;
                
                let price:Double = Double(products.price)!;
                cell.lblCurrentPrice.text = String(format: "%.2f %@", price, _currency_symbol);
            }
            else
            {
                cell.lblEarlierPrice.isHidden = false;
                let sale_price = Double(products.sale_price)!;
                cell.lblCurrentPrice.text = String(format: "%.2f %@", sale_price, _currency_symbol);
                let current_price = Double(products.price)!;
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", current_price, _currency_symbol));
                
                attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
                
                cell.lblEarlierPrice.attributedText = attributeString;
            }
            
            if products.sale_percentage == "0"
            {
                cell.vwDiscount.isHidden = true;
                cell.lblProduct.textInsets = UIEdgeInsetsMake(1, 1, 1, 1)
            }
            else
            {
                cell.vwDiscount.isHidden = false;
                cell.lblDiscount.text = products.sale_percentage;
                cell.lblProduct.textInsets = UIEdgeInsetsMake(1, 1, 1, 40)
            }
            
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
        var products = Product();
        if !CommonFunctions.isNull(s: self.catagoriesList) && !CommonFunctions.isNull(s: self.catagoriesList[indexPath.section - 1].product)
        {
            products = self.catagoriesList[indexPath.section - 1].product[indexPath.row];
            vc.product_id = products.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else
        {
            products = self.subCatagoriesList.product[indexPath.row];
            vc.product_id = products.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        
    }
}
