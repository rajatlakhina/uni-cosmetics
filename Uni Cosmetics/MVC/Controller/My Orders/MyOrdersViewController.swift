//
//  MyOrdersViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class MyOrdersViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    @IBOutlet weak var tableViewMyOrders: UITableView!
    @IBOutlet weak var vwSearch: UIViewProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    var isBackBtnHidden:Bool = true;
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        
        self.vwHeader.btnBack.isHidden = self.isBackBtnHidden;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        
        self.tableViewMyOrders.register(UINib(nibName: "MyOrdersTableViewCell", bundle: nil), forCellReuseIdentifier: "MyOrdersTableViewCell");
        self.tableViewMyOrders.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductHeaderTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnShowSearchView(_ sender: UIButton)
    {
        self.vwSearch.isHidden = !self.vwSearch.isHidden
    }
    
    @IBAction func btnViewOrderDetailsAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }

    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 255;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerSection = tableView.dequeueReusableCell(withIdentifier: "ProductHeaderTableViewCell") as! ProductHeaderTableViewCell
        
        headerSection.centralhorizontallyCostraint.isActive = true
        headerSection.bottomConstraint.isActive = false;
        
        if section == 0
        {
            headerSection.lblTitle.text = "Manage your orders";
        }
        
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersTableViewCell") as! MyOrdersTableViewCell;
        cell.selectionStyle = .none;
        
        cell.imgProduct.image = UIImage(named: "perfume_image_four");
        cell.lblProductDate.text = "Delivered Jul 18, 2018";
        cell.lblProductName.text = "Ravlon Uniq \nOne Hair Tratment \n150ml";
        
        cell.btnOrderDetails.tag = (indexPath.section * 100) + indexPath.row;
        cell.btnOrderDetails.addTarget(self, action: #selector(self.btnViewOrderDetailsAction(_:)), for: .touchUpInside);
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }

}
