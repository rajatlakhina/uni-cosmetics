//
//  MyOrdersTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: LabelProperties!
    
    @IBOutlet weak var lblProductDate: LabelProperties!
    
    @IBOutlet weak var btnProductDetails: UIButton!
    
    @IBOutlet weak var btnBuyItAgain: UIButton!
    @IBOutlet weak var btnProductReview: UIButton!
    @IBOutlet weak var btnOrderDetails: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
