//
//  ContactUsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: HeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnViberAction(_ sender: UIButton)
    {
    }
    
    @IBAction func btnWhatsappAction(_ sender: UIButton)
    {
    }
    
    
    @IBAction func btnMessangerAction(_ sender: UIButton)
    {
    }
}
