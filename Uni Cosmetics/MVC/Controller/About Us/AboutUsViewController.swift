//
//  AboutUsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit
import SDWebImage

class AboutUsViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: HeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var imgAboutUs: UIImageView!
    @IBOutlet weak var aboutWebView: UIWebView!
    //MARK:- VARIABLE DECLARATION
    
    var aboutUs = AboutUs()
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        
        self.startLoader();
        self.aboutWebView.scrollView.bounces = false;
        self.aboutWebView.scrollView.showsVerticalScrollIndicator = false;
        self.aboutWebView.scrollView.showsHorizontalScrollIndicator = false;
        
        ApiHelper.aboutUs(lang: "en", controller: self, completionHandler: {
            (data) -> () in
            self.stopLoader();
//            self.txtViewAboutUs.text = data.content.htmlToString;
            
            self.aboutWebView.loadHTMLString(data.content, baseURL: nil);
            
            let url = data.image;
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            self.imgAboutUs.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            self.view.updateConstraints();
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
}
