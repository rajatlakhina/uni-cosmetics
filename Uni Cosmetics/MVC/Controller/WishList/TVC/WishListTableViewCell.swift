//
//  WishListTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat Lakhina on 23/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class WishListTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var lblProductName: LabelProperties!
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblNewPrice: LabelProperties!
    @IBOutlet weak var lblOldPrice: LabelProperties!
    @IBOutlet weak var lblDiscountPercent: LabelProperties!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var lblQuantity: LabelProperties!
    
    @IBOutlet weak var lblStatus: LabelProperties!
    @IBOutlet weak var lblTotalAmount: LabelProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
