//
//  WishlistViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat Lakhina on 23/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class WishlistViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDataSource, UITableViewDelegate
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewWishList: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnCart.isHidden = true;
        self.vwHeader.btnSearch.isHidden = true;
        self.vwHeader.btnNotification.isHidden = true;
        self.tableViewWishList.register(UINib(nibName: "WishListTableViewCell", bundle: nil), forCellReuseIdentifier: "WishListTableViewCell"); 
        
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnAddToCartAction(_ sender: UIButton)
    {
    }
    
    //MARK:- TABLEVIEW DELEGATES AND DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishListTableViewCell") as! WishListTableViewCell
        cell.selectionStyle = .none;
        return cell
    }
}
