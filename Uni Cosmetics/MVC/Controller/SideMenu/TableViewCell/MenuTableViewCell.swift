//
//  MenuTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 01/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell
{
     @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imgUpperBorder: UIImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblMenuItemName: LabelProperties!
    
    @IBOutlet weak var imgFlag: UIImageView!
   
    @IBOutlet weak var imgLowerBorder: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
