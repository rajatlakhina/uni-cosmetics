//
//  LeftMenuViewController.swift
//  Mart
//
//  Created by Rahul Singla on 16/05/18.
//  Copyright © 2018 Imbibe User. All rights reserved.
//

import Foundation
import UIKit

var leadingLeftDrawer: NSLayoutConstraint?
var leftSideDrawer: LeftMenu! = nil;

protocol LeftMenuViewController
{
    var baseView: UIView { get }
    var leftMenuBackground: UIView { get }
}

extension LeftMenuViewController
{
    func initialiseLeftMenu(viewControllerBase: UIViewControllerBase)
    {
        if(!CommonFunctions.isNull(s: baseView))
        {
            if(!CommonFunctions.isNull(s: leftMenuBackground))
            {
                leftSideDrawer = LeftMenu();
                baseView.addSubview(leftSideDrawer);
                let width = UIScreen.main.bounds.size.width/1.2;
                leadingLeftDrawer = NSLayoutConstraint(item: leftSideDrawer, attribute: .leading, relatedBy: .equal, toItem: baseView, attribute: .leading, multiplier: 1.0, constant: -width);
                baseView.addConstraint(leadingLeftDrawer!);
                baseView.addConstraint(NSLayoutConstraint(item: leftSideDrawer, attribute: .bottom, relatedBy: .equal, toItem: baseView, attribute: .bottom, multiplier: 1.0, constant: 0));
                baseView.addConstraint(NSLayoutConstraint(item: leftSideDrawer, attribute: .top, relatedBy: .equal, toItem: baseView, attribute: .top, multiplier: 1.0, constant: 0));
                baseView.addConstraint(NSLayoutConstraint(item: leftSideDrawer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: width));
                
                leftSideDrawer.setNeedsLayout();
                leftSideDrawer.layoutIfNeeded();
                
                leftSideDrawer.backgroundColor = UIColor.white;
                leftSideDrawer.translatesAutoresizingMaskIntoConstraints = false;
                leftSideDrawer.clipsToBounds = true;
                
                leftSideDrawer.viewController = viewControllerBase;
                
                self.leftMenuBackground.isHidden = true;
                leftSideDrawer.isHidden = true;
                
                let viewControllerBase = self as! UIViewControllerBase;
                let tapToggleMenuAction = UITapGestureRecognizer(target: viewControllerBase, action: #selector(viewControllerBase.tapToggleMenuAction));
                
                self.leftMenuBackground.addGestureRecognizer(tapToggleMenuAction);
                
                let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(viewControllerBase.handleSwipeGestureOpenMenu(_:)));
                swipeLeft.direction = .left;
                self.leftMenuBackground.addGestureRecognizer(swipeLeft);
            }
        }
    }
    
    func hideMenu()
    {
        if(!CommonFunctions.isNull(s: leftMenuBackground))
        {
            if(!CommonFunctions.isNull(s: leftSideDrawer))
            {
                leftMenuBackground.isHidden = true;
                leftSideDrawer.isHidden = true;
            }
            
        }
    }
    
    func toggleLeftMenu()
    {
        if(!CommonFunctions.isNull(s: baseView))
        {
            if(!CommonFunctions.isNull(s: leftMenuBackground))
            {
                if(!CommonFunctions.isNull(s: leadingLeftDrawer))
                {
                    if((leadingLeftDrawer?.constant)! < CGFloat(0))
                    {
                        baseView.removeConstraint(leadingLeftDrawer!);
                        leadingLeftDrawer = NSLayoutConstraint(item: leftSideDrawer, attribute: .leading, relatedBy: .equal, toItem: baseView, attribute: .leading, multiplier: 1.0, constant: 0)
                        baseView.addConstraint(leadingLeftDrawer!);
                        baseView.updateConstraints()
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.baseView.updateConstraintsIfNeeded();
                            (self as! UIViewController).view.layoutIfNeeded();
                            leftSideDrawer.isHidden = false;
                            self.leftMenuBackground.isHidden = false;
                            
                        });
                        leftSideDrawer.syncMenu();
                    }
                    else
                    {
                        self.closeDrawer();
                    }
                }
            }
        }
    }
    
    func closeDrawer()
    {
        if(!CommonFunctions.isNull(s: baseView))
        {
            if(!CommonFunctions.isNull(s: leftMenuBackground))
            {
                if(!CommonFunctions.isNull(s: leadingLeftDrawer))
                {
                    let width = UIScreen.main.bounds.size.width/1.2;
                    baseView.removeConstraint(leadingLeftDrawer!);
                    leadingLeftDrawer = NSLayoutConstraint(item: leftSideDrawer, attribute: .leading, relatedBy: .equal, toItem: baseView, attribute: .leading, multiplier: 1.0, constant: -width)
                    baseView.addConstraint(leadingLeftDrawer!);
                    baseView.updateConstraints()
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.baseView.updateConstraintsIfNeeded();
                        (self as! UIViewController).view.layoutIfNeeded();
                        leftSideDrawer.isHidden = true;
                        self.leftMenuBackground.isHidden = true;
                        
                    });
                }
            }
        }
    }
}
