//
//  LeftMenu.swift
//  Uni Cosmetics
//
//  Created by Rajat on 01/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class LeftMenu: UIView, UITableViewDelegate, UITableViewDataSource
{
    
    //MARK:- OUTLETS
    @IBOutlet weak var lblUserName: LabelProperties!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var tableViewBasic: UITableView!
    @IBOutlet weak var tableViewDeals: UITableView!
    @IBOutlet weak var tableViewSettings: UITableView!
    
    @IBOutlet weak var vwInformationMenuContainer: UIView!
    
    @IBOutlet weak var btnMainMenu: ButtonProperties!
    @IBOutlet weak var tableViewInformation: UITableView!
    @IBOutlet weak var vwSettingsContainer: UIView!
    
    @IBOutlet weak var tableViewUserSettings: UITableView!
    
    // MARK:- --------- Variable Declaration ----------//
    var viewController : UIViewControllerBase? = nil;
    
    let basic = ["Home", "About Us", "Contact"];
    let deals = ["All deal's", "Upcoming deal's", "Shop by category", "Shop by brand", "Gift Cards & Registry", "Information"];
    let settingsOptions = ["Settings", "Customer Service"];
    let userSettingsLoggedOut = ["Language", "Notifications", "Rate Our App"]
    let userSettingsLoggedIn = ["Language", "Notifications", "Rate Our App", "? Signout"];
    let information = ["How to buy", "Payment Methods", "Payments & Shipping", "FAQs", "Customer Protection", "Terms & Conditions" ];
    
    // MARK:- --------- view lifecycle ----------//
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        nibSetup();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        nibSetup();
    }
    
    func nibSetup()
    {
        Bundle.main.loadNibNamed("LeftMenu", owner: self, options: nil);
        
        guard let contentView = self.containerView else { return }
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(contentView);
        self.syncMenu();
    }
    
    // MARK:- --------- view functinality ----------//
    
    func syncMenu()
    {
        self.vwInformationMenuContainer.isHidden = true;
        self.vwSettingsContainer.isHidden = true;
        self.btnMainMenu.isHidden = true;
        
        self.registerCellForTableView(tableView: self.tableViewBasic);
        self.registerCellForTableView(tableView: self.tableViewDeals);
        self.registerCellForTableView(tableView: self.tableViewSettings);
        self.registerCellForTableView(tableView: self.tableViewInformation);
        self.registerCellForTableView(tableView: self.tableViewUserSettings);
        
        self.layoutSubviews();
        
        let user = CartHelper.getCurrentUser();
        
        if !CommonFunctions.isNull(s: user.id)
        {
            self.lblUserName.text = "Hello, " + user.first_name;
        }
        else
        {
            self.lblUserName.text = "Hello, " + "Sign In";
        }
    }
    
    func registerCellForTableView(tableView:UITableView)
    {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell");
        
        tableView.delegate = self;
        tableView.dataSource = self;
    }
    
    //MARK:- BUTTON ACTION HANDLER
    @IBAction func btnOrdersAction(_ sender: UIButton)
    {
        if !CommonFunctions.isNull(s: viewController)
        {
            let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController;
            
            viewController?.navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    @IBAction func btnListAction(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnMainMenuAction(_ sender: UIButton)
    {
        self.btnMainMenu.isHidden = true;
        self.vwInformationMenuContainer.isHidden = true;
        self.vwSettingsContainer.isHidden = true;
    }
    
    @IBAction func btnAccountAction(_ sender: UIButton)
    {
        let user = CartHelper.getCurrentUser();
        if !CommonFunctions.isNull(s: self.viewController)
        {
            if !CommonFunctions.isNull(s: user.id)
            {
                let vc = self.viewController!.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController;
                self.viewController!.navigationController?.pushViewController(vc, animated: true);
            }
            else
            {
                for vC: UIViewController in (self.viewController!.navigationController?.viewControllers)!
                {
                    if (vC is LoginViewController)
                    {
                        self.viewController!.navigationController?.popToViewController(vC, animated: true);
                        return;
                    }
                }
                
                let vc = self.viewController!.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
                self.viewController!.navigationController?.pushViewController(vc, animated: true);
            }
        }
    }
    
    //MARK:- TABLE VIEW DATASOURCE AND DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.tableViewBasic
        {
            return self.basic.count;
        }
        else if tableView == self.tableViewDeals
        {
            return self.deals.count;
        }
        else if tableView == self.tableViewInformation
        {
            return self.information.count;
        }
        else if tableView == self.tableViewUserSettings
        {
            let user = CartHelper.getCurrentUser();
            
            if !CommonFunctions.isNull(s: user.id)
            {
                return self.userSettingsLoggedIn.count;
            }
            else
            {
                return self.userSettingsLoggedOut.count;
            }
        }
        else
        {
            return self.settingsOptions.count;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.tableViewInformation
        {
            return 60;
        }
        else
        {
            return 50;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.tableViewBasic
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
            cell.selectionStyle = .none;
            cell.imgArrow.isHidden = true;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            
            cell.lblMenuItemName.text = self.basic[indexPath.row];
            
            return cell;
        }
        else if tableView == self.tableViewUserSettings
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
            cell.selectionStyle = .none;
            cell.imgArrow.isHidden = true;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            
            if indexPath.row == 2
            {
                cell.imgLowerBorder.isHidden = false;
                cell.imgUpperBorder.isHidden = false;
            }
            
            cell.lblMenuItemName.text = self.userSettingsLoggedIn[indexPath.row];
            
            if indexPath.row == 3
            {
                let user = CartHelper.getCurrentUser();
                cell.lblMenuItemName.text = "Not " + user.first_name + self.userSettingsLoggedIn[indexPath.row];
            }
            
            return cell;
        }
        else if tableView == self.tableViewDeals
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
            cell.selectionStyle = .none;
            cell.imgArrow.isHidden = true;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            
            if indexPath.row == 5
            {
                cell.imgArrow.isHidden = false;
            }
            
            cell.lblMenuItemName.text = self.deals[indexPath.row];
            
            return cell;
        }
        else if tableView == self.tableViewInformation
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
            cell.selectionStyle = .none;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            cell.vwContainer.layer.borderColor = UIColor.lightGray.cgColor;
            cell.vwContainer.layer.borderWidth = 1;
            cell.lblMenuItemName.text = self.information[indexPath.row];
            
            return cell;
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
            cell.selectionStyle = .none;
            cell.imgArrow.isHidden = true;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            
            cell.lblMenuItemName.text = self.settingsOptions[indexPath.row];
            
            if indexPath.row == 0
            {
                cell.imgArrow.isHidden = false;
                cell.imgFlag.isHidden = false;
            }
            
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if !CommonFunctions.isNull(s: self.viewController)
        {
            if tableView == self.tableViewBasic
            {
                if indexPath.row == 0
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is HomeViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 1
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is AboutUsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 2
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is ContactUsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
            }
            if tableView == self.tableViewDeals
            {
                if indexPath.row == 0
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is AllDealsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "AllDealsViewController") as! AllDealsViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 1
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is UpcomingDealsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "UpcomingDealsViewController") as! UpcomingDealsViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 2
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is ShopByCategoryViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                    let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "ShopByCategoryViewController") as! ShopByCategoryViewController
                    viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 3
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is ShopByBrandViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                    let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "ShopByBrandViewController") as! ShopByBrandViewController
                    viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 4
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is GiftCardViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "GiftCardViewController") as! GiftCardViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 5
                {
                    self.btnMainMenu.isHidden = false;
                    self.vwInformationMenuContainer.isHidden = false;
                }
            }
            
            if tableView == self.tableViewInformation
            {
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5
                {
            
                    _info_screen = indexPath.row;
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is InformationViewController) && _info_screen == indexPath.row
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else if(currentController is InformationViewController) && _info_screen != indexPath.row
                    {
//                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "InformationViewController") as! InformationViewController
//                        vc.info_title = self.information[indexPath.row];
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "InformationViewController") as! InformationViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 3
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is FAQsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                        let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "FAQsViewController") as! FAQsViewController
                        viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                
            }
            
            if tableView == self.tableViewSettings
            {
                if indexPath.row == 0
                {
                    self.btnMainMenu.isHidden = false;
                    self.vwSettingsContainer.isHidden = false;
                }
                if indexPath.row == 1
                {
                    self.closeMenu();
                    
                    let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "CustomerServiceViewController") as! CustomerServiceViewController
                    viewController?.navigationController?.pushViewController(vc, animated: true);
                    
                }
            }
            if tableView == self.tableViewUserSettings
            {
                if indexPath.row == 0
                {
                    self.closeMenu();
                    
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is LanguageSettingsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                    let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "LanguageSettingsViewController") as! LanguageSettingsViewController
                    viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 1
                {
                    self.closeMenu();
                    let currentController = self.viewController?.navigationController?.topViewController;
                    if(currentController is NotificationSettingsViewController)
                    {
                        self.viewController?.refreshScreen();
                        return;
                    }
                    else
                    {
                    let vc = viewController?.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingsViewController") as! NotificationSettingsViewController
                    viewController?.navigationController?.pushViewController(vc, animated: true);
                    }
                }
                if indexPath.row == 2
                {
                    self.closeMenu();
                    
                }
                if indexPath.row == 3
                {
                    self.closeMenu();
                    BackGroundView.show(controller: viewController!, color: UIColor.black.withAlphaComponent(0.4));
                    //self.
                    CommonFunctions.showAlertWithMutipleActions(
                        message: "Do you want to signout?",
                        title: "Signout",
                        controller: viewController!,
                        firstBtnTitle: "No",
                        secondBtnTitle: "Yes",
                        completionHandler: { (ActionNo) in
                            if(ActionNo == 2)
                            {
                                //                            StateHelper.setIsUserLoggedIn(value: false);
                                //StateHelper.setAuthToken(value: "");
                                BackGroundView.hide(controller: self.viewController!);
//                                self.viewController?.navigationController?.popToRootViewController(animated: true);
                                let user = CartHelper.getCurrentUser();
                                self.viewController?.startLoader();
                                
                                ApiHelper.logout(user_id: user.id, lang: _lang[0], controller: self.viewController!, completionHandler: {
                                    self.viewController?.stopLoader();
                                    let appdelegate = UIApplication.shared.delegate as! AppDelegate;
                                    CartHelper.setCurrentUser(user: User());
                                    appdelegate.switchRootToIntroduction();
                                    
                                }, errorHandler: self.viewController!.apiCallErrorHandler)
                                
                            }
                            else
                            {
                                BackGroundView.hide(controller: self.viewController!);
                            }
                    });
                }
            }
        }
    }
    
    func closeMenu()
    {
        if(self.viewController != nil)
        {
            (self.viewController as! LeftMenuViewController).closeDrawer();
        }
        
    }
    
}
