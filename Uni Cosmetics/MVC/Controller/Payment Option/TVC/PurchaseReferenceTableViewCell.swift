//
//  PurchaseReferenceTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PurchaseReferenceTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDelivererName: LabelProperties!
    
    @IBOutlet weak var lblDeliveryAddress: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
