//
//  PaymentOptionTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PaymentOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCardHolderName: LabelProperties!
    @IBOutlet weak var lblCardNumber: LabelProperties!
    
    @IBOutlet weak var lblExpiryDate: LabelProperties!
    
    @IBOutlet weak var btnEditCard: ButtonProperties!
    
    @IBOutlet weak var btnDeleteCard: ButtonProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
