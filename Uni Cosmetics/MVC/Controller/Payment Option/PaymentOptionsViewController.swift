//
//  PaymentOptionsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PaymentOptionsViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewPaymentOption: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.tableViewPaymentOption.register(UINib(nibName: "PurchaseReferenceTableViewCell", bundle: nil), forCellReuseIdentifier: "PurchaseReferenceTableViewCell");
        self.tableViewPaymentOption.register(UINib(nibName: "PaymentOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentOptionTableViewCell");
        self.tableViewPaymentOption.register(UINib(nibName: "PaymentInformationTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentInformationTableViewCell");
        self.tableViewPaymentOption.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductHeaderTableViewCell");
        self.tableViewPaymentOption.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1;
        }
        else if section == 1
        {
            return 1;
        }
        else if section == 2
        {
            return 1;
        }
        else if section == 3
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 1
        {
            return 30;
        }
        else if section == 2
        {
            return 70;
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerSection = tableView.dequeueReusableCell(withIdentifier: "ProductHeaderTableViewCell") as! ProductHeaderTableViewCell
        
        headerSection.centralhorizontallyCostraint.isActive = true
        headerSection.bottomConstraint.isActive = false;
        
        if section == 1
        {
            headerSection.lblTitle.text = "";
        }
        if section == 2
        {
            headerSection.lblTitle.text = "Manage Payment Options";
        }
        
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
            
            cell.selectionStyle = .none;
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            
            cell.vwContainer.layer.borderColor = UIColor.lightGray.cgColor;
            cell.vwContainer.layer.borderWidth = 1.0;
            cell.vwContainer.layer.cornerRadius = 4;
            
            //Cell data
            cell.lblMenuItemName.text = "Add a payment method";
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseReferenceTableViewCell") as! PurchaseReferenceTableViewCell;
            cell.selectionStyle = .none;
            
            return cell
        }
        else if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentInformationTableViewCell") as! PaymentInformationTableViewCell;
            cell.selectionStyle = .none;
            
            cell.lblPaymentInfoHeading.text = "Default Payment Reference";
            cell.lblPaymentInfoDetails.text = "To improve your Uni purchasing experiences, set prefered payment method";
            cell.vwContainer.backgroundColor = UIColor.lightGray;
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentOptionTableViewCell") as! PaymentOptionTableViewCell;
            cell.selectionStyle = .none;
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

    }
}
