//
//  SignUpViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewControllerBase
{
    //MARK:- OUTLETS
    @IBOutlet weak var txtFirstName: UITextFieldProperties!
    @IBOutlet weak var txtLastName: UITextFieldProperties!
    @IBOutlet weak var txtEmail: UITextFieldProperties!
    @IBOutlet weak var txtPassword: UITextFieldProperties!
    @IBOutlet weak var txtConfirmPassword: UITextFieldProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnRegisterAction(_ sender: UIButton)
    {
        if CommonFunctions.isEmpty(s: self.txtFirstName.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter first name !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtLastName.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter last name !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtEmail.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter email !!", duration: 3);
            return
        }
        if !CommonFunctions.isValidEmail(email: self.txtEmail.text!)
        {
            Toast().showToast(message: "Please enter valid email !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter password !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtConfirmPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter confirm password !!", duration: 3);
            return
        }
        if self.txtPassword.text != self.txtConfirmPassword.text
        {
            Toast().showToast(message: "Passwords doesn't match !!", duration: 3);
            return
        }
        
        self.startLoader();
        ApiHelper.signUp(first_name: self.txtFirstName.text!, last_name: self.txtLastName.text!, email: self.txtEmail.text!, password: self.txtPassword.text!, lang: "en", controller: self, completionHandler: {
            self.stopLoader();
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController;
//            self.navigationController?.pushViewController(vc, animated: true);
            let appdelegate = UIApplication.shared.delegate as! AppDelegate;
            appdelegate.switchRootToHome();
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton)
    {
        for vC: UIViewController in (self.navigationController?.viewControllers)!
        {
            if (vC is LoginViewController)
            {
                self.navigationController?.popToViewController(vC, animated: true);
                return;
            }
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnSkipAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
}
