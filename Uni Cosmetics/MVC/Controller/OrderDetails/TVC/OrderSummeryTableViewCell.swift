//
//  OrderSummeryTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 28/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class OrderSummeryTableViewCell: UITableViewCell {
    @IBOutlet weak var lblItemCost: LabelProperties!
    @IBOutlet weak var lblShippingCost: LabelProperties!
    
    @IBOutlet weak var lblTotalBeforeTax: LabelProperties!
    
    @IBOutlet weak var lblEstimatedTax: LabelProperties!
    
    @IBOutlet weak var lblImportFee: LabelProperties!
    
    @IBOutlet weak var lblOrderTotal: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
