//
//  OrderDetailsTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblOrderDate: LabelProperties!
    
    @IBOutlet weak var lblOrderId: LabelProperties!
    
    @IBOutlet weak var lblOrderTotal: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
