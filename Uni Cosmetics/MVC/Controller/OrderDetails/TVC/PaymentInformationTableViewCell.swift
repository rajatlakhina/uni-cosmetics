//
//  PaymentInformationTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PaymentInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var lblPaymentInfoHeading: LabelProperties!
    @IBOutlet weak var lblPaymentInfoDetails: LabelProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
