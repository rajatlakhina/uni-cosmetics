//
//  ProductTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var lblStatus: LabelProperties!
    @IBOutlet weak var lblEstimate: LabelProperties!
    @IBOutlet weak var lblDate: LabelProperties!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: LabelProperties!
    @IBOutlet weak var lblPrice: LabelProperties!
    @IBOutlet weak var lblQuantity: LabelProperties!
    @IBOutlet weak var lblSoldBy: LabelProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
