//
//  OrderDetailsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewOrderDetails: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.tableViewOrderDetails.register(UINib(nibName: "OrderDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsTableViewCell");
        self.tableViewOrderDetails.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell");
        self.tableViewOrderDetails.register(UINib(nibName: "PaymentInformationTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentInformationTableViewCell");
        self.tableViewOrderDetails.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductHeaderTableViewCell");
        self.tableViewOrderDetails.register(UINib(nibName: "ShipphingAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "ShipphingAddressTableViewCell");
        self.tableViewOrderDetails.register(UINib(nibName: "OrderSummeryTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderSummeryTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1;
        }
        else if section == 1
        {
            return 1;
        }
        else if section == 2
        {
            return 2;
        }
        else if section == 3
        {
            return 1;
        }
        else if section == 4
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerSection = tableView.dequeueReusableCell(withIdentifier: "ProductHeaderTableViewCell") as! ProductHeaderTableViewCell
        
        headerSection.centralhorizontallyCostraint.isActive = true
        headerSection.bottomConstraint.isActive = false;
        
        if section == 0
        {
            headerSection.lblTitle.text = "View Order Details";
        }
        if section == 1
        {
            headerSection.lblTitle.text = "Shipment details";
        }
        if section == 2
        {
            headerSection.lblTitle.text = "Payment Information";
        }
        if section == 3
        {
            headerSection.lblTitle.text = "Shipping Address";
        }
        if section == 4
        {
            headerSection.lblTitle.text = "Order Summary";
        }
        
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsTableViewCell") as! OrderDetailsTableViewCell
            
            cell.selectionStyle = .none;
            
            //Cell data
            cell.lblOrderDate.text = "Jul 16, 2018";
            cell.lblOrderId.text = "11208080383654667"
            cell.lblOrderTotal.text = "106.51" + _currency_symbol + " (item)";
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell") as! ProductTableViewCell;
            cell.selectionStyle = .none;
            
            cell.imgProduct.image = UIImage(named: "perfume_image_four");
            cell.lblDate.text = "Wednesday, Jul 18, 2018 by 8pm";
            cell.lblProductName.text = "Ravlon Uniq \nOne Hair Tratment \n150ml";
            cell.lblPrice.text = "55.00" + _currency_symbol;
            cell.lblStatus.text = "Delivered";
            cell.lblEstimate.text = "Delivery Estimate";
            
            return cell
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShipphingAddressTableViewCell") as! ShipphingAddressTableViewCell;
            cell.selectionStyle = .none;
            return cell;
        }
        else if indexPath.section == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderSummeryTableViewCell") as! OrderSummeryTableViewCell;
            cell.selectionStyle = .none;
            return cell;
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentInformationTableViewCell") as! PaymentInformationTableViewCell;
            cell.selectionStyle = .none;

            
            if indexPath.row == 0
            {
                cell.lblPaymentInfoHeading.text = "Payment Method";
                cell.lblPaymentInfoDetails.text = "Visa ending in 0000";
            }
            else
            {
                cell.lblPaymentInfoHeading.text = "Billing Address";
                cell.lblPaymentInfoDetails.text = "1657, Riverside Drive Redding, CA 96001";
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
    }
    
}

