//
//  CategoryTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCatagoryName: LabelProperties!
    @IBOutlet weak var imgCategory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
