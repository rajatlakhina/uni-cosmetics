//
//  ShopByCategoryViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ShopByCategoryViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewCategory: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    var catagories = [Catagory]()
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
//        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
//        self.vwHeader.btnBack.isHidden = true;
        
        self.tableViewCategory.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getCategoriesData();
    }
    
    func getCategoriesData()
    {
        self.startLoader();
        ApiHelper.getCategoriesData(lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.catagories = data;
            self.tableViewCategory.reloadData();
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK: UITABLEVIEW DELEGATE AND DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.catagories.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell") as! CategoryTableViewCell;
        cell.selectionStyle = .none;
        cell.lblCatagoryName.text = String(htmlEncodedString: self.catagories[indexPath.row].name);
        
        let url = self.catagories[indexPath.row].banner;
        let fileUrl = (url)?.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
        
        cell.imgCategory.sd_setImage(with: URL(string: fileUrl ?? ""), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
        
        cell.imgCategory.backgroundColor = UIColor.black.withAlphaComponent(0.05);
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CatagoriesListViewController") as! CatagoriesListViewController
        vc.cat_id = self.catagories[indexPath.row].cat_id;
        self.navigationController?.pushViewController(vc, animated: true);
    }
}
