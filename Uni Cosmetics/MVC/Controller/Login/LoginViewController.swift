//
//  LoginViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class LoginViewController: UIViewControllerBase
{
    //MARK:- OUTLETS
    @IBOutlet weak var txtEmail: UITextFieldProperties!
    @IBOutlet weak var txtPassword: UITextFieldProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        
    }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnForgotPasswordAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnRegisterAction(_ sender: UIButton)
    {
        for vC: UIViewController in (self.navigationController?.viewControllers)!
        {
            if (vC is SignUpViewController)
            {
                self.navigationController?.popToViewController(vC, animated: true);
                return;
            }
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton)
    {
        if CommonFunctions.isEmpty(s: self.txtEmail.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter email !!", duration: 3);
            return
        }
        if !CommonFunctions.isValidEmail(email: self.txtEmail.text!)
        {
            Toast().showToast(message: "Please enter valid email !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter password !!", duration: 3);
            return
        }
        
        self.startLoader();
        ApiHelper.login(email: self.txtEmail.text!, password: self.txtPassword.text!, lang: _lang[0], controller: self, completionHandler: {
            self.stopLoader();
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController;
//            self.navigationController?.pushViewController(vc, animated: true);
            let appdelegate = UIApplication.shared.delegate as! AppDelegate;
            appdelegate.switchRootToHome();
            
        }, errorHandler: self.apiCallErrorHandler)
        
    }
    
    @IBAction func btnSkipAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
}
