//
//  OrderSummeryViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class OrderSummeryViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var lblDelivererName: LabelProperties!
    
    @IBOutlet weak var lblDeliveryAddress: LabelProperties!
    @IBOutlet weak var lblItemsCost: LabelProperties!
    
    @IBOutlet weak var lblShippingCost: LabelProperties!
    @IBOutlet weak var lblTotalBeforeTax: LabelProperties!
    @IBOutlet weak var lblEstimatedTaxCollected: LabelProperties!
    @IBOutlet weak var lblImportFeesDeposit: LabelProperties!
    
    @IBOutlet weak var lblOrderTotal: LabelProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }

}
