//
//  ProfileViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewControllerBase , LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var btnSave: ButtonProperties!
    
    @IBOutlet weak var txtName: UITextFieldProperties!
    
    @IBOutlet weak var txtLastname: UITextFieldProperties!
    
    @IBOutlet weak var txtEmail: UITextFieldProperties!
    
    @IBOutlet weak var txtPassword: UITextFieldProperties!
    
    //MARK:- VARIABLE DECLARATION
    var isEditProfile:Bool = false;
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    override func initialiseScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        
        self.setData();
        self.setViewForEditing();
    }
    
    func setData()
    {
        let user = CartHelper.getCurrentUser();
        
        self.txtName.text = user.first_name;
        self.txtLastname.text = user.last_name;
        self.txtEmail.text = user.email;
    }
    
    func setViewForEditing()
    {
        //self.txtPassword.isUserInteractionEnabled = false;
        self.txtEmail.isUserInteractionEnabled = false;
        
        if self.isEditProfile
        {
            let current_user = CartHelper.getCurrentUser();
            
            if CommonFunctions.isEmpty(s: self.txtName.text, checkWhitespace: true)
            {
                Toast().showToast(message: "Please enter first name !!", duration: 3);
                return
            }
            if CommonFunctions.isEmpty(s: self.txtLastname.text, checkWhitespace: true)
            {
                Toast().showToast(message: "Please enter last name !!", duration: 3);
                return
            }
            self.startLoader();
            ApiHelper.editProfile(user: current_user.id, first_name: self.txtName.text!, last_name: self.txtLastname.text!, lang: _lang[0], controller: self, completionHandler: {
                self.stopLoader();
                self.isEditProfile = !self.isEditProfile
                self.btnSave.setTitle("Edit", for: .normal);
                self.txtName.isUserInteractionEnabled = false;
                self.txtLastname.isUserInteractionEnabled = false;
                self.setData();
            }, errorHandler: self.apiCallErrorHandler)
            
        }
        else
        {
            self.btnSave.setTitle("Edit", for: .normal);
            
            self.txtName.isUserInteractionEnabled = false;
            self.txtLastname.isUserInteractionEnabled = false;
            //self.txtEmail.isUserInteractionEnabled = true;
        }
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    @IBAction func btnSaveAction(_ sender: UIButton)
    {
        if !self.isEditProfile
        {
            self.txtName.isUserInteractionEnabled = true;
            self.txtLastname.isUserInteractionEnabled = true;
            self.btnSave.setTitle("Save", for: .normal)
            self.isEditProfile = !self.isEditProfile
        }
        else
        {
            self.setViewForEditing();
        }
    }
    
    @IBAction func btnChangeAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(vc, animated: true);
    }
}
