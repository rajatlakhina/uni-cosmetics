//
//  UpcomingDealsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class AllDealsViewController: UIViewControllerBase, LeftMenuViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: HeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var collectionViewAllDeals: UICollectionView!
    
    //MARK:- VARIABLE DECLARATION
    
    var time:Double = 0;
    var homeData = Home();
    var tableSectionIndex:Int = 0;
    var productTimer: Timer!
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        
        self.collectionViewAllDeals.register(UINib(nibName: "WeeklyOfferCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WeeklyOfferCollectionViewCell");
        
        self.collectionViewAllDeals.register(UINib(nibName: "DealsHeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DealsHeaderCollectionViewCell")
        
        self.collectionViewAllDeals.register(UINib(nibName: "AllDealsFooterCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "AllDealsFooterCollectionViewCell")
        
        if let layout = self.collectionViewAllDeals.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getAllData();
    }
    
    override func postRefreshScreen()
    {
        productTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    override func screenDisappear()
    {
        time = 0;
        productTimer.invalidate();
    }
    
    @objc func runTimedCode(cell1: HomeTableViewCell)
    {
        self.time += 1
        self.collectionViewAllDeals.reloadData();
    }
    
    func getAllData()
    {
        self.startLoader();
        ApiHelper.getAllDeals(lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.homeData = data;
            
            self.collectionViewAllDeals.reloadData();
            
            if self.tableSectionIndex != 0
            {
                self.collectionViewAllDeals.scrollToItem(at: IndexPath(item: 0, section: self.tableSectionIndex), at: .top, animated: true)
            }
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- COLLECTION VIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        var section_rows:Int = 0;
        
        if !CommonFunctions.isNull(s: self.homeData.weekly)
        {
            section_rows += 1;
        }
        if !CommonFunctions.isNull(s: self.homeData.monthly)
        {
            section_rows += 1;
        }
        if !CommonFunctions.isNull(s: self.homeData.perfume)
        {
            section_rows += 1;
        }
        
        return section_rows;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0 // offers
        {
            if !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                return self.homeData.weekly.weekly_products.count
            }
            else
            {
                return (0);
            }
        }
        else if section == 1 // offers
        {
            if !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                return self.homeData.monthly.monthly_products.count
            }
            else
            {
                return (0);
            }
        }
        else if section == 2// offers
        {
            if !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                return self.homeData.perfume.perfume_products.count
            }
            else
            {
                return (0);
            }
        }
        else
        {
            return (0);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: 50);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: 21);
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                        withReuseIdentifier: "DealsHeaderCollectionViewCell",
                                                                                        for: indexPath) as! DealsHeaderCollectionViewCell
            if indexPath.section == 0 && !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                headerView.lblDealName.text = self.homeData.weekly.name;
            }
            if indexPath.section == 1 && !CommonFunctions.isNull(s: self.homeData.monthly)
            {
                
                headerView.lblDealName.text = self.homeData.monthly.name;
            }
            if indexPath.section == 2 && !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                
                headerView.lblDealName.text = self.homeData.perfume.name;
            }
            
            return headerView
        
        case UICollectionElementKindSectionFooter:
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "AllDealsFooterCollectionViewCell",
                                                                             for: indexPath) as! AllDealsFooterCollectionViewCell
            
            footerView.imgFooter.backgroundColor = primaryLightPinkColor;
            
            return footerView
            
        default:
            //4
            //assert(false, "Unexpected element kind")
            fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            let weekly = self.homeData.weekly!.weekly_products[indexPath.item];
            
            let url = weekly.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(weekly.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(weekly.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@ off)", weekly.sale_percentage);
            
            if weekly.dela_end_date > 0
            {
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: weekly.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            
            cell.lblProductName.text = weekly.item_name;
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            let monthly = self.homeData.monthly!.monthly_products[indexPath.item];
            
            let url = monthly.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(monthly.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(monthly.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@)", monthly.sale_percentage);
            
            if monthly.dela_end_date > 0
            {
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: monthly.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            
            cell.lblProductName.text = monthly.item_name;
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            let perfume = self.homeData.perfume!.perfume_products[indexPath.item];
            
            let url = perfume.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(perfume.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(perfume.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@ off)", perfume.sale_percentage);
            
            if perfume.dela_end_date > 0
            {
                cell.lblDealEndTime.isHidden = false;
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: perfume.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            else
            {
                cell.lblDealEndTime.isHidden = true;
            }
            
            cell.lblProductName.text = perfume.item_name;
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let perfume = self.homeData.weekly!.weekly_products[indexPath.item];
            vc.product_id = perfume.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        if indexPath.section == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let perfume = self.homeData.monthly!.monthly_products[indexPath.item];
            vc.product_id = perfume.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        if indexPath.section == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let perfume = self.homeData.perfume!.perfume_products[indexPath.item];
            vc.product_id = perfume.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width) / 3, height: 200);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
}
