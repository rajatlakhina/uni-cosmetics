//
//  ShopByBrandViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit
import SDWebImage

class ShopByBrandViewController: UIViewControllerBase, LeftMenuViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var collectionViewBrands: UICollectionView!
    
    //MARK:- VARIABLE DECLARATION
    
    //let brands = ["brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand","brand", "brand", "brand","brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand", "brand"];
    var brands = [Brand]();
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
//        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
//        self.vwHeader.btnBack.isHidden = true;
        
        self.collectionViewBrands.register(UINib(nibName: "BrandsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BrandsCollectionViewCell")
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getBrands();
    }
    
    func getBrands()
    {
        self.startLoader();
        ApiHelper.getBrands(lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.brands = data;
            self.collectionViewBrands.reloadData();
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- COLLECTION VIEW DELEGATE AND DATASOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if !CommonFunctions.isNull(s: self.brands)
        {
            return self.brands.count
        }
        else
        {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandsCollectionViewCell", for: indexPath) as! BrandsCollectionViewCell;
        
        let url = self.brands[indexPath.item].banner;
        let fileUrl = (url)!.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
        
        cell.imgBrand.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        vc.sub_cat_id = self.brands[indexPath.item].id;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width) / 3, height: (collectionView.frame.width) / 3);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
}
