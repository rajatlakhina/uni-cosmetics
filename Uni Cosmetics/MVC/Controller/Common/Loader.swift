//
//  Loader.swift
//  Bubble Car Wash
//
//  Created by Orem on 19/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit
import Lottie

class Loader: UIView
{
    // MARK:- ------- Singelton instance -------//
    static let sharedInstance = Loader();
    

    // MARK:- ------- Show Loader -------//
    static func showLoader(controller: UIViewController)
    {
        hideLoader(controller: controller)
        
        let loadingView = UIView()
        loadingView.tag = 9999
        loadingView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.4);
        
        let innerLoaderView = UIView()
        let innerLoaderViewWidth = CGFloat(100)
        let innerLoaderViewHeight = CGFloat(100)
        
        CommonFunctions.cornerRadius(view: innerLoaderView, cornerRadius: 10, maskToBounds: true)
        innerLoaderView.frame = CGRect(x: (UIScreen.main.bounds.size.width - innerLoaderViewWidth)/2, y: (UIScreen.main.bounds.size.height - innerLoaderViewHeight)/2, width: innerLoaderViewWidth, height: innerLoaderViewHeight)
        innerLoaderView.backgroundColor = UIColor.clear;
        loadingView.addSubview(innerLoaderView);
        
        let animationView = LOTAnimationView(name: "loader_ring");
        animationView.frame = CGRect(x: 0, y: 0, width: 100, height: 100);
        innerLoaderView.addSubview(animationView);
        animationView.play();
        animationView.loopAnimation = true;
        
//        let lblLoader = UILabel(frame: CGRect(x: (UIScreen.main.bounds.size.width - innerLoaderViewWidth)/2, y:( (UIScreen.main.bounds.size.height - innerLoaderViewHeight)/2) + 110, width: innerLoaderViewWidth, height: 25));
//        lblLoader.textColor = greyColorRGB;
//        var newFontSize: CGFloat
//
//        if IS_iPHONE_5
//        {
//            newFontSize = 12 * 1.1;
//        }
//        else if IS_STANDARD_IPHONE_6
//        {
//            newFontSize = 12 * 1.2;
//        }
//        else if (Is_iPad_9 || is_iPad_10 || is_iPad_12)
//        {
//            newFontSize = (12) * 1.5;
//        }
//        else
//        {
//            newFontSize = 12 * 1.3;
//        }
//
//        lblLoader.font = UIFont(name: "Poppins-Regular", size: newFontSize);
//        lblLoader.text = message;
//        lblLoader.numberOfLines = 0;
//        lblLoader.textColor = UIColor.white;
//        lblLoader.textAlignment = .center;
//        loadingView.addSubview(lblLoader);
//
        controller.view.addSubview(loadingView)
        controller.view.bringSubview(toFront: loadingView)
    }
    
    // MARK:- ------- Hide Loader -------//
    static func hideLoader(controller: UIViewController)
    {
        for view in controller.view.subviews
        {
            if view.tag == 9999
            {
                view.removeFromSuperview()
            }
        }
    }
}
