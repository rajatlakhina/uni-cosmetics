//
//  ProductSortOptionTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 03/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductSortOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var btnSortBy: ButtonProperties!
    @IBOutlet weak var btnFilter: ButtonProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
