//
//  backGroundView.swift
//  Crap Chronicles
//
//  Created by Orem on 05/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class BlurBackGroundView: UIView
{
    static let sharedInstance = BackGroundView()
    
    
    // MARK:- ------- Show Loader -------//
    static func show(controller: UIViewController, y: CGFloat, alpha:CGFloat, color:UIColor)
    {
        hide(controller: controller)
        
        let loadingView = UIView()
        loadingView.tag = 9999
        loadingView.frame = CGRect(x: 0, y: y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        //loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.4);
        loadingView.backgroundColor = color;
        loadingView.alpha = 0;
        
        controller.view.addSubview(loadingView)
        UIView.animate(withDuration: 0.9, animations: {
            loadingView.alpha = alpha;
        })
        
    }
    
    // MARK:- ------- Hide Loader -------//
    static func hide(controller: UIViewController)
    {
        for view in controller.view.subviews
        {
            if view.tag == 9999
            {
                UIView.animate(withDuration: 0.9, animations: {
                    view.alpha = 0;
                }, completion: { finised in
                    view.removeFromSuperview()
                })
            }
        }
    }
}
