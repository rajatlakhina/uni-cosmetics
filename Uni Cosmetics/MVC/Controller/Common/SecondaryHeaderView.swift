//
//  HeaderView.swift
//  Uni Cosmetics
//
//  Created by Rajat on 30/10/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class SecondaryHeaderView: UIView
{
    @IBOutlet var vwContainer: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
//    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    //    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.commonInit();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.commonInit();
    }
    
    private func commonInit()
    {
        Bundle.main.loadNibNamed("SecondaryHeaderView", owner: self, options: nil);
        self.addSubview(self.vwContainer);
        self.vwContainer.frame = self.bounds;
        self.vwContainer.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": self.vwContainer]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": self.vwContainer]))
        
    }
}
