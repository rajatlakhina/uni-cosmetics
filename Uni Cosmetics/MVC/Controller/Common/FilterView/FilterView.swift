//
//  FilterView.swift
//  Uni Cosmetics
//
//  Created by Rajat on 03/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class FilterView: UIView
{
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var vwContainer: UIViewProperties!
    
    @IBOutlet weak var btnClearAll: ButtonProperties!
    
    @IBOutlet weak var btnDepartment: ButtonProperties!
    
    @IBOutlet weak var btnDealType: ButtonProperties!
    
    @IBOutlet weak var btnPrice: ButtonProperties!
    
    @IBOutlet weak var btnDiscount: ButtonProperties!
    
    @IBOutlet weak var btnAvgCustomerReview: ButtonProperties!
    
    @IBOutlet weak var btnCancel: ButtonProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LIFECYCLE
    
    override func didMoveToWindow()
    {
        
    }
    
    //MARK:- CUSTOM METHODS
}
