//
//  UIViewControllerBase.swift
//  Assignment
//
//  Created by Rajat Lakhina on 05/08/18.
//  Copyright © 2018 Rajat Lakhina. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class UIViewControllerBase: UIViewController, UITabBarDelegate {
    
    //MARK:- VARIABLE DECLARATION
    var reusableViewsNib = UiHelper.getReusableViewsNibInstance();
    var reusableViews = UiHelper.getReusableViewsInstance();
    var customView: ErrorLoader! = nil;
    var networkError: NetworkError! = nil;
    
    var filterView: FilterView! = nil;
    var sortByView:SortByView! = nil;
    
    var isNetworkError:Bool = false;
    var index:Int = 0;

    override func viewDidLoad()
    {
        super.viewDidLoad()
//        self.customView = self.reusableViewsNib[0] as! ErrorLoader;
        
//        self.customView.frame = CGRect(x: 0, y: y, width: self.view.bounds.width, height: self.view.bounds.height - y);
//        self.view.addSubview(self.customView);
//        self.view.bringSubview(toFront: self.customView);
        print(self.index)
        self.filterView = self.reusableViews[0] as? FilterView;
        self.filterView.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300);//300
        self.view.addSubview(self.filterView);
        self.view.bringSubview(toFront: self.filterView);
        self.filterView.isHidden = true;
        self.filterView.alpha = 0;
        self.filterView.btnCancel.tag = 1102;
        self.filterView.btnCancel.addTarget(self, action: #selector(self.btnCancelAction(_:)), for: .touchUpInside);
        
        self.sortByView = self.reusableViews[1] as? SortByView;
        self.sortByView.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 350, width: UIScreen.main.bounds.size.width, height: 350);//350
        self.view.addSubview(self.sortByView);
        self.view.bringSubview(toFront: self.sortByView);
        self.sortByView.isHidden = true;
        self.sortByView.btnCancel.tag = 2202;
        self.sortByView.btnCancel.addTarget(self, action: #selector(self.btnCancelAction(_:)), for: .touchUpInside);
        
        self.networkError = self.reusableViewsNib[1] as! NetworkError;
        self.view.addSubview(self.networkError);
        self.view.bringSubview(toFront: self.networkError);
        
//        self.customView.isHidden = true;
//        self.customView.btnReload.addTarget(self, action: #selector(self.btnReloadAction(_:)), for: .touchUpInside);
        
        self.networkError.isHidden = true;
        self.networkError.btnReload.addTarget(self, action: #selector(self.btnReloadAction(_:)), for: .touchUpInside);
//        let current_comtroller = self.navigationController?.topViewController;
//        if current_comtroller is SignInViewController || current_comtroller is SignUpViewController || current_comtroller is UploadReportViewController || current_comtroller is SettingsViewController || current_comtroller is ChangePasswordViewController || current_comtroller is LaunchScreenViewController
//        {
//
//        }
//        else
//        {
//            self.startLoader();
//        }
        self.initialiseScreen();
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        
        self.refreshScreen();
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated);
        
        
        let currentVC = self.navigationController?.topViewController
        
        if currentVC is IntroductionViewController || currentVC is LoginViewController || currentVC is SignUpViewController || currentVC is ForgotPasswordViewController
        {
            
        }
        else
        {
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeRightGestureOpenMenu));
            swipeRight.direction = .right;
            self.view.addGestureRecognizer(swipeRight);
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeRightGestureOpenMenu));
            swipeLeft.direction = .left;
            self.view.addGestureRecognizer(swipeLeft);
        }
        
        self.postRefreshScreen();
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated);
        self.screenDisappear();
    }
    
    func initialiseScreen()
    {
        
    }
    
    func refreshScreen()
    {
        
    }
    
    func postRefreshScreen()
    {
        
    }
    
    func screenDisappear()
    {
        
    }
    
    func popToInitialViewController()
    {
//        let isLoggedIn = StateHelper.getIsUserLoggedIn()
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate;
//
//        if isLoggedIn!
//        {
//            appdelegate.switchRootToHome();
//        }
//        else
//        {
//            appdelegate.switchRootToLogin();
//        }
    }
    
    @IBAction func btnReloadAction(_ sender: UIButton)
    {
        self.networkError.isHidden = true;
        
        self.reload();
    }
    
    @objc func handleSwipeRightGestureOpenMenu(gesture: UISwipeGestureRecognizer) -> Void
    {
        if gesture.direction == UISwipeGestureRecognizerDirection.right
        {
            (self as! LeftMenuViewController).toggleLeftMenu();
        }
        
        if gesture.direction == UISwipeGestureRecognizerDirection.left
        {
            (self as! LeftMenuViewController).closeDrawer();
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload()
    {

    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) -> UIImage
    {
        print("Download Started")
        var img = UIImage();
        self.getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async()
                {
                    img = UIImage(data: data)!
            }
        }
        return img;
    }
    
    func startLoader()
    {
        //Loader.showLoader(controller: self);
        let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Please Wait...", messageFont: UIFont (name: "ApexNew-Medium", size: 15), messageSpacing: 15, type: .lineScalePulseOutRapid, color: primaryPinkColor, padding: 5, displayTimeThreshold: 1, minimumDisplayTime: 50, backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: primaryPinkColor);
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData);
//        let activityData = ActivityData(type: .ballGridPulse, color: splashBackGreen);
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData);
    }
    
    func stopLoader()
    {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating();
        //Loader.hideLoader(controller: self);
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle{
//        let current_controller = self.navigationController?.topViewController;
////        if current_controller is IntroductionViewController
////        {
////            return .default
////        }
////        else if current_controller is SignInViewController
////        {
////            return .default
////        }
////        else
////        {
////            return .lightContent
////        }
//    }
    
    //Setting navigation bar
    
    func setNavigationBar(heightHeaderVW:NSLayoutConstraint, lblCenterVertically:NSLayoutConstraint, btnCenterVertically:NSLayoutConstraint, topConstraint:NSLayoutConstraint)
    {
        if(IS_STANDARD_IPHONE_X || IS_IPAD)
        {
            heightHeaderVW.constant = 88
            btnCenterVertically.constant = 24
            lblCenterVertically.constant = 24
            topConstraint.constant = -24
        }
        else
        {
            heightHeaderVW.constant = 64
            btnCenterVertically.constant = 10
            lblCenterVertically.constant = 10
            topConstraint.constant = -20
        }
    }
    
    func setNavigationBar(heightHeaderVW:NSLayoutConstraint, lblCenterVertically:NSLayoutConstraint, btnCenterVertically:NSLayoutConstraint, btnEditCenterVertically:NSLayoutConstraint, topConstraint:NSLayoutConstraint)
    {
        if(IS_STANDARD_IPHONE_X || IS_IPAD)
        {
            heightHeaderVW.constant = 88
            btnCenterVertically.constant = 24
            lblCenterVertically.constant = 24
            btnEditCenterVertically.constant = 24
            topConstraint.constant = -24
        }
        else
        {
            heightHeaderVW.constant = 64
            btnCenterVertically.constant = 0
            lblCenterVertically.constant = 0
            btnEditCenterVertically.constant = 0
            topConstraint.constant = 0
        }
    }
    
    func initializeBanner(bannerScrollView: UIScrollView, width: CGFloat, height: CGFloat, bannerData: [UIImage])
    {
        for i in 0..<bannerData.count
        {
            let bannerImg = UIImageView(frame: CGRect(x:width * CGFloat(i), y:0,width:width, height:height));
            bannerImg.image = bannerData[i];
            //bannerImg.contentMode = .scaleAspectFit;
            
            bannerScrollView.addSubview(bannerImg);
        }
        
        bannerScrollView.contentSize = CGSize(width:self.view.frame.width * CGFloat(bannerData.count), height:height);
        
    }
    
    func moveToNextPageAutomatically(bannerScrollView: UIScrollView, bannerData: [UIImage])
    {
        let pageWidth:CGFloat = self.view.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(bannerData.count)
        let contentOffset:CGFloat = bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:80), animated: true);
    }
    
    @objc func tapToggleMenuAction(_ sender: UITapGestureRecognizer)
    {
        (self as! LeftMenuViewController).toggleLeftMenu();
    }
    
    @objc func handleSwipeGestureOpenMenu( _ gesture: UISwipeGestureRecognizer) -> Void
    {
        if gesture.direction == UISwipeGestureRecognizerDirection.left
        {
            (self as! LeftMenuViewController).closeDrawer();
        }
    }
    
    public func apiCallErrorHandler(dict: NSDictionary?, error: Error?, response: DataResponse<Any>?)
    {
        self.showApiCallError(dict: dict, error: error, response: response);
    }
    
    public func showApiCallError(dict: NSDictionary?, error: Error?, response: DataResponse<Any>?, completion: (() -> Void)? = nil)
    {
        self.stopLoader();
        if(dict != nil)
        {
            //let vc = self.topMostViewController();
            
            
            if !CommonFunctions.isNull(s: dict!.object(forKey: "status"))
            {
//                let status = dict!.object(forKey: "status") as! Int
//                if status == 401
//                {
//                    Toast().showToast(message: dict!.object(forKey: "msg") as! String, duration: 3);
////                    let appdelegate = UIApplication.shared.delegate as! AppDelegate;
////                    appdelegate.switchRootToLogin();
////                    Toast().showToast(message: dict!.object(forKey: "msg") as! String, duration: 3);
//                }
//                else
//                {
//                    if vc is MyOrdersViewController
//                    {
//
//                    }
//                    else
//                    {
                        Toast().showToast(message: dict!.object(forKey: "mesg") as! String, duration: 3);
                    //}
//                }
            }
            else
            {
//                if vc is MyOrdersViewController
//                {
//
//                }
//                else
//                {
                    Toast().showToast(message: dict!.object(forKey: "mesg") as! String, duration: 3);
//                }
            }
            
        }
        else if(error != nil)
        {
//            if StateHelper.getLang() == "1"
//            {
            Toast().showToast(message: "Something went wrong !!", duration: 3);
//                self.showError(msg: "Something went wrong !!");
                //self.networkError.lblMessage.text = "Your device does not have working internet connection !!";
//            }
//            else
//            {
//                self.showError(msg: "حدث خطأ");
//                //self.networkError.lblMessage.text = "جهازك غير متصل بالإنترنت";
//            }
            
        }
        
        if(completion != nil)
        {
            completion!();
        }
    }
    
    public func showError(msg:String)
    {
        var y:CGFloat = 0.0
        print(self.index)
        //let vc = self.topMostViewController();
//        if vc is MyOrdersViewController
//        {
//            if IS_STANDARD_IPHONE_X || IS_STANDARD_IPHONE_X_R
//            {
//                y = 128;
//            }
//            else
//            {
//                y = 106;
//            }
//        }
//        else if vc is LoginViewController
//        {
//            y = 0;
//        }
//        else
//        {
            if IS_STANDARD_IPHONE_X || IS_STANDARD_IPHONE_X_R
            {
                y = 88;
            }
            else
            {
                y = 64;
            }
//        }
        
        self.networkError.frame = CGRect(x: 0, y: y, width: self.view.bounds.width, height: self.view.bounds.height - y);
        self.networkError.lblMessage.text = msg;
        self.networkError.isHidden = false;
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnCancelAction(_ sender: UIButton)
    {
        UIView.animate(withDuration: 0.9, animations: {
            BlurBackGroundView.hide(controller: self);
            if sender.tag == 1102
            {
                self.filterView.alpha = 0;
            }
            else if sender.tag == 2202
            {
                self.sortByView.alpha = 0;
            }
            
        }, completion: {
            finished in
            if sender.tag == 1102
            {
                self.filterView.isHidden = true;
            }
            else if sender.tag == 2202
            {
                self.sortByView.isHidden = true;
            }
        })
    }

}
