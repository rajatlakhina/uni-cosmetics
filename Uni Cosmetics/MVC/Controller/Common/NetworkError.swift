//
//  NetworkError.swift
//  Bubble Car Wash
//
//  Created by Orem on 27/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit
import Lottie

class NetworkError: UIView
{

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var animationView: LOTAnimatedControl!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnReload: UIButton!

    // MARK:- --------- view lifecycle ----------//
    override func didMoveToWindow()
    {
        self.setupUI()
    }
    
    
    // MARK:- --------- view functinality ----------//
    
    func setupUI()
    {
        self.animationView.animationView.setAnimation(named: "cloud_disconnection");
        animationView.animationView.backgroundColor = UIColor.clear;
        animationView.animationView.loopAnimation = true;
        self.animationView.animationView.play();
    }
}
