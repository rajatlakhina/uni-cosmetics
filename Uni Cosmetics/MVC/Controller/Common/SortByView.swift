//
//  SortByView.swift
//  Uni Cosmetics
//
//  Created by Rajat on 03/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class SortByView: UIView
{

    //MARK:- OUTLETS
    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var btnRelevance: ButtonProperties!
    
    @IBOutlet weak var btnBestSelling: ButtonProperties!
    @IBOutlet weak var btnPriceLowToHigh: ButtonProperties!
    @IBOutlet weak var btnPriceHighToLow: ButtonProperties!
    @IBOutlet weak var btnDiscountLowToHigh: ButtonProperties!
    @IBOutlet weak var btnDiscountHighToLow: ButtonProperties!
    
    @IBOutlet weak var btnCancel: ButtonProperties!
    
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LIFECYCLE
    
    override func didMoveToWindow()
    {
        
    }
    
    //MARK:- CUSTOM METHODS
}
