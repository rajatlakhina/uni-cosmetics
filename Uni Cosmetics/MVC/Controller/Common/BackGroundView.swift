//
//  backGroundView.swift
//  Crap Chronicles
//
//  Created by Orem on 05/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class BackGroundView: UIView
{
    static let sharedInstance = BackGroundView()
    
    
    // MARK:- ------- Show Loader -------//
    static func show(controller: UIViewController, color:UIColor)
    {
        hide(controller: controller)
        
        let loadingView = UIView()
        loadingView.tag = 9999
        loadingView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        //loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.4);
        loadingView.backgroundColor = color;
            
        controller.view.addSubview(loadingView)
    }
    
    // MARK:- ------- Hide Loader -------//
    static func hide(controller: UIViewController)
    {
        for view in controller.view.subviews
        {
            if view.tag == 9999
            {
                view.removeFromSuperview()
            }
        }
    }
}
