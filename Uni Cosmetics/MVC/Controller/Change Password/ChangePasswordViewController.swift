//
//  ChangePasswordViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewControllerBase , LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    @IBOutlet weak var txtCurrentPassword: UITextFieldProperties!
    @IBOutlet weak var txtNewPassword: UITextFieldProperties!
    @IBOutlet weak var txtConfirmPassword: UITextFieldProperties!
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    override func initialiseScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    @IBAction func btnChangeAction(_ sender: UIButton)
    {
        if CommonFunctions.isEmpty(s: self.txtCurrentPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter current password !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtNewPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter new password !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtConfirmPassword.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter confirm password !!", duration: 3);
            return
        }
        if self.txtNewPassword.text != self.txtConfirmPassword.text
        {
            Toast().showToast(message: "Passwords doesn't match !!", duration: 3);
            return
        }
        let current_user = CartHelper.getCurrentUser();
        self.startLoader();
        ApiHelper.changePassword(user_id: current_user.id, oldPassword: self.txtCurrentPassword.text!, newPassword: self.txtNewPassword.text!, lang: _lang[0], controller: self, completionHandler: {
            self.stopLoader();
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                // Put your code which should be executed with a delay here
                self.navigationController?.popViewController(animated: true);
            })
            
        }, errorHandler: apiCallErrorHandler)
    }
}
