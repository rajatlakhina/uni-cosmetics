//
//  AccountViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class AccountViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var tableViewAccount: UITableView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    let account = ["Profile", "Wishlist", "Billing Address", "Shipping Address"];
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    override func initialiseScreen()
    {
        self.tableViewAccount.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell");
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- TABLE VIEW DELEGATE AND DATA SOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.account.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell;
        cell.selectionStyle = .none;
        cell.imgFlag.isHidden = true;
        cell.imgLowerBorder.isHidden = true;
        cell.imgUpperBorder.isHidden = true;
        cell.vwContainer.layer.borderColor = UIColor.lightGray.cgColor;
        cell.vwContainer.layer.borderWidth = 1;
        
        cell.lblMenuItemName.text = self.account[indexPath.row];
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(vc, animated: true);
        }
//        else if indexPath.row == 1
//        {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditAddressViewController") as! EditAddressViewController
//            self.navigationController?.pushViewController(vc, animated: true);
//        }
        else if indexPath.row == 1
        {
            
        }
        else if indexPath.row == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
            
            vc.type = 1;
            
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else if indexPath.row == 3
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
            
            vc.type = 2;
            
            self.navigationController?.pushViewController(vc, animated: true);
        }
    }
}
