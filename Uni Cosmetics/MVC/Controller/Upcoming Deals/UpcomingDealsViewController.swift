//
//  UpcomingDealsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class UpcomingDealsViewController: UIViewControllerBase, LeftMenuViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: HeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var collectionViewUpComingDeals: UICollectionView!
    
    //MARK:- VARIABLE DECLARATION
    
    var upcomingDeals: [Product]! = nil;
    var productTimer: Timer!
    var time:Double = 0;
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        
        self.collectionViewUpComingDeals.register(UINib(nibName: "WeeklyOfferCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WeeklyOfferCollectionViewCell");

        if let layout = self.collectionViewUpComingDeals.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getUpcomingDeals();
    }
    
    override func postRefreshScreen()
    {
        productTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    override func screenDisappear()
    {
        time = 0;
        productTimer.invalidate();
    }
    
    func getUpcomingDeals()
    {
        self.startLoader();
        ApiHelper.getUpcomingDeals(lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.upcomingDeals = data;
            self.collectionViewUpComingDeals.reloadData();
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    @objc func runTimedCode(cell1: HomeTableViewCell)
    {
        self.time += 1
        self.collectionViewUpComingDeals.reloadData();
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- COLLECTION VIEW DELEGATE AND DATASOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if !CommonFunctions.isNull(s: self.upcomingDeals)
        {
            return self.upcomingDeals.count;
        }
        else
        {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
        
        let weekly = self.upcomingDeals[indexPath.item];
        
        let url = weekly.images[0];
        let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
        
        cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
        
        let sale_price = Double(weekly.sale_price)!
        cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
        let price = Double(weekly.price)!
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
        
        attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
        
        cell.lblLastPrice.attributedText = attributeString;
        
        cell.lblDisCountPercent.text = String(format: "(%@ off)", weekly.sale_percentage);
        
        if weekly.dela_end_date > 0
        {
            let server_time = ConstantFunctions.getServerTime();
            
            let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
            
            let another_date = ConstantFunctions.getDate(unixTimestamp: weekly.dela_end_date);
            
            let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
            
            cell.lblDealEndTime.text = "Ends in \(remaning_time)";
        }
        
        cell.lblProductName.text = weekly.item_name;
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
        let perfume = self.upcomingDeals[indexPath.item];
        vc.product_id = perfume.item_id;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width) / 3, height: 200);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
}
