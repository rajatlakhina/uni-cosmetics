//
//  EditAddressViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class EditAddressViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var tableViewEditAddress: UITableView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!

    //MARK:- VARIABLE DECLARATION
    
    let editAccount = ["Edit Billing Address", "Edit Shipping Address"]
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    override func initialiseScreen()
    {
        self.tableViewEditAddress.register(UINib(nibName: "EditAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "EditAddressTableViewCell");
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    //MARK:- TABLE VIEW DELEGATE AND DATA SOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.editAccount.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditAddressTableViewCell") as! EditAddressTableViewCell
        cell.selectionStyle = .none
        
        cell.lblTitle.text = self.editAccount[indexPath.row];
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
        //vc.address_title = self.editAccount[indexPath.row];
        
        vc.type = indexPath.row + 1;
        
        self.navigationController?.pushViewController(vc, animated: true);
    }
}


