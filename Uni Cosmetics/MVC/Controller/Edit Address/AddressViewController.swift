//
//  AddressViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class AddressViewController: UIViewControllerBase, LeftMenuViewController, UITextViewDelegate
{
    //MARK:- OUTLETS
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    @IBOutlet weak var lblTitle: LabelProperties!
    
    @IBOutlet weak var txtLastName: UITextFieldProperties!
    @IBOutlet weak var txtName: UITextFieldProperties!
    
    @IBOutlet weak var txtPhoneNumber: UITextFieldProperties!
    @IBOutlet weak var txtEmail: UITextFieldProperties!
    
    @IBOutlet weak var txtFieldAddress: UITextViewProperties!
    
    @IBOutlet weak var txtLandmark: UITextField!
    
    @IBOutlet weak var txtCity: UITextFieldProperties!
    
    @IBOutlet weak var txtState: UITextFieldProperties!
    
    @IBOutlet weak var txtCountry: UITextFieldProperties!
    
    @IBOutlet weak var txtZipCode: UITextFieldProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    var type:Int = 0;
    var isEditAddress:Bool = false;
    var address = Address();
    static let addressPlaceholderText:String = "Address";
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    override func initialiseScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        
        if self.type == 1 && self.isEditAddress
        {
            self.lblTitle.text = "Edit Shipping Address";
        }
        else if self.type == 2 && self.isEditAddress
        {
            self.lblTitle.text = "Edit Billing Address";
        }
        else if self.type == 1 && !self.isEditAddress
        {
            self.lblTitle.text = "Add Shipping Address";
        }
        else if self.type == 2 && !self.isEditAddress
        {
            self.lblTitle.text = "Add Billing Address";
        }
        else
        {
            self.lblTitle.text = "Saved Addresses";
        }
        
        self.setData();
        self.txtFieldAddress.setPlaceholderText(AddressViewController.addressPlaceholderText);
        self.txtFieldAddress.delegate = self;
        self.txtFieldAddress.syncwithPlaceholderText();
        self.updateViewConstraints();
    }
    
    func setData()
    {
        self.txtName.text = self.address.first_name;
        self.txtLastName.text = self.address.last_name;
        self.txtEmail.text = self.address.email;
        self.txtPhoneNumber.text = self.address.phone;
        self.txtFieldAddress.text = self.address.address_1;
        self.txtLandmark.text = self.address.address_2;
        self.txtCity.text = self.address.city;
        self.txtState.text = self.address.state;
        self.txtCountry.text = self.address.country;
        self.txtZipCode.text = self.address.postcode;
    }
    
    //MARK:- -------Text View Delegates-----------
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if(textView == self.txtFieldAddress)
        {
            self.txtFieldAddress.textFieldDidBeginEditing();
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if(textView == self.txtFieldAddress)
        {
            self.txtFieldAddress.textFieldDidEndEditing();
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(textView == self.txtFieldAddress)
        {
            self.txtFieldAddress.textFieldDidChange();
        }
        
        return true
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }

    @IBAction func btnSaveAction(_ sender: UIButton)
    {
        if CommonFunctions.isEmpty(s: self.txtName.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter name !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtLastName.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter surname !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtEmail.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter email !!", duration: 3);
            return
        }
        if !CommonFunctions.isValidEmail(email: self.txtEmail.text!)
        {
            Toast().showToast(message: "Please enter valid email !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtPhoneNumber.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter phone number !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtFieldAddress.getEffectiveTextIgnoringPlaceholder(""), checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter address !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtCity.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter city !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtState.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter state !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtCountry.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter country !!", duration: 3);
            return
        }
        if CommonFunctions.isEmpty(s: self.txtZipCode.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter zip code !!", duration: 3);
            return
        }
        
        if CommonFunctions.isEmpty(s: self.address.user_id, checkWhitespace: true)
        {
            let current_user = CartHelper.getCurrentUser();
            self.address.user_id = current_user.id;
        }
        
        self.address.address_1 = self.txtFieldAddress.getEffectiveTextIgnoringPlaceholder("");
        self.address.address_2 = self.txtLandmark.text ?? "";
        self.address.first_name = self.txtName.text!;
        self.address.last_name = self.txtLastName.text!;
        self.address.phone = self.txtPhoneNumber.text!
        self.address.email = self.txtEmail.text!;
        self.address.city = self.txtCity.text!
        self.address.state = self.txtState.text!;
        self.address.country = self.txtCountry.text!;
        self.address.postcode = self.txtZipCode.text!;
        self.address.type = String(format: "%ld", self.type);
        
        self.startLoader()
        
        ApiHelper.saveAddress(lang: _lang[0], address: self.address, delete: "", controller: self, completionHandler: {
            self.stopLoader();
            
            self.navigationController?.popViewController(animated: true);
        }, errorHandler: self.apiCallErrorHandler)
    }
}
