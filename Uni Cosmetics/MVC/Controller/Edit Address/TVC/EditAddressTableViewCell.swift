//
//  EditAddressTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 17/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class EditAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
