//
//  CustomerServiceViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 23/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class CustomerServiceViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var tableViewCustomerService: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
        self.tableViewCustomerService.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell");
        self.tableViewCustomerService.register(UINib(nibName: "CustomerServiceTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomerServiceTableViewCell");
        self.tableViewCustomerService.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductHeaderTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnViewOrdersAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController;
        vc.isBackBtnHidden = false;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnViewOrderDetailsAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnAccoutSettingsAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController;
//        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnReturnAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController;
//        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    @IBAction func btnPaymentOptionsAction(_ sender: UIButton)
    {
        let section = (sender.tag / 100);
        let row = (sender.tag % 100);
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionsViewController") as! PaymentOptionsViewController;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 80
        }
        else
        {
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 330;
        }
        else
        {
            return 50;
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerSection = tableView.dequeueReusableCell(withIdentifier: "ProductHeaderTableViewCell") as! ProductHeaderTableViewCell
        
        headerSection.centralhorizontallyCostraint.isActive = false
        headerSection.bottomConstraint.constant = 5;
        headerSection.bottomConstraint.isActive = true;
        
        if section == 0
        {
            headerSection.lblTitle.text = "Manage your orders";
        }
        if section == 1
        {
            headerSection.lblTitle.text = "Need more help?";
        }
        
        return headerSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
            
            cell.selectionStyle = .none;
            
            // Cell UI
            cell.imgFlag.isHidden = true;
            cell.imgLowerBorder.isHidden = true;
            cell.imgUpperBorder.isHidden = true;
            cell.vwContainer.layer.borderColor = UIColor.lightGray.cgColor;
            cell.vwContainer.layer.borderWidth = 1;
            
            //Cell data
            cell.lblMenuItemName.text = "Contact Us";
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerServiceTableViewCell") as! CustomerServiceTableViewCell;
            
            cell.selectionStyle = .none;
            
            cell.imgProduct.image = UIImage(named: "perfume_image_four");
            cell.lblDate.text = "Delivered Jul 18, 2018";
            
            cell.btnViewOrders.tag = (indexPath.section * 100) + indexPath.row;
            cell.btnRefunds.tag = (indexPath.section * 100) + indexPath.row;
            cell.btnPaymentOptions.tag = (indexPath.section * 100) + indexPath.row;
            cell.btnAccountSettings.tag = (indexPath.section * 100) + indexPath.row;
            cell.btnViewOrderDetails.tag = (indexPath.section * 100) + indexPath.row;
            
            cell.btnViewOrders.addTarget(self, action: #selector(self.btnViewOrdersAction(_:)), for: .touchUpInside);
            cell.btnRefunds.addTarget(self, action: #selector(self.btnReturnAction(_:)), for: .touchUpInside);
            cell.btnPaymentOptions.addTarget(self, action: #selector(self.btnPaymentOptionsAction(_:)), for: .touchUpInside);
            cell.btnAccountSettings.addTarget(self, action: #selector(self.btnAccoutSettingsAction(_:)), for: .touchUpInside);
            cell.btnViewOrderDetails.addTarget(self, action: #selector(self.btnViewOrderDetailsAction(_:)), for: .touchUpInside);
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(vc, animated: true);
        }
    }

}
