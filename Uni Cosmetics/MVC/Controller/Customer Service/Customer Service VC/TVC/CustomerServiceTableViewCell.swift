//
//  CustomerServiceTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 23/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class CustomerServiceTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblDate: LabelProperties!
    @IBOutlet weak var btnViewOrderDetails: UIButton!
    
    @IBOutlet weak var btnPaymentOptions: UIButton!
    @IBOutlet weak var btnAccountSettings: UIButton!
    @IBOutlet weak var btnRefunds: UIButton!
    @IBOutlet weak var btnViewOrders: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
