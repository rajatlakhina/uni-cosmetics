//
//  AddressTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 18/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: LabelProperties!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var lblEmail: LabelProperties!
    //@IBOutlet weak var lblLandmark: LabelProperties!
    
    @IBOutlet weak var lblAddress: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
