//
//  AddressListViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 18/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class AddressListViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    
    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var tableViewAddressList: UITableView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    var type:Int = 0;
    var address = [Address]();
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.tableViewAddressList.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell");
        
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        
        if self.type == 1
        {
            self.lblTitle.text = "Billing Addresses";
        }
        else if type == 2
        {
            self.lblTitle.text = "Shipping Addresses";
        }
        else
        {
            self.lblTitle.text = "Saved Addresses";
        }
    }
    
    override func refreshScreen()
    {
        self.startLoader()
        let current_user = CartHelper.getCurrentUser();
        ApiHelper.getAddress(user_id: current_user.id, lang: _lang[0], type: String(format: "%ld", self.type), controller: self, completionHandler:
            {
                (data) in
                self.stopLoader();
                self.address = data;
                
                if self.address.count > 0
                {
                    self.tableViewAddressList.reloadData();
                }
                else
                {
                    Toast().showToast(message: "You still do no have setup any address. Setup Now !!", duration: 3);
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController;
//                    vc.type = self.type;
//                    self.navigationController?.pushViewController(vc, animated: true);
                }
                
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnAddAddressAction(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController;
        vc.type = self.type;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    //MARK:- TABLE VIEW DELEGATES AND DATA SOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.address.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as! AddressTableViewCell
        cell.selectionStyle = .none;
        
        
        let lastAddress:String = String(format: "%@, %@, %@", self.address[indexPath.row].city, self.address[indexPath.row].state, self.address[indexPath.row].country);
        
        var beforeAddres:String = "";
        
        if !CommonFunctions.isEmpty(s: self.address[indexPath.row].address_2, checkWhitespace: true)
        {
            beforeAddres = String(format: "%@ %@ %@", self.address[indexPath.row].address_1 , "near", self.address[indexPath.row].address_2);
        }
        else
        {
            beforeAddres = self.address[indexPath.row].address_1;
        }
        
        cell.lblName.text = String(format: "%@ %@", self.address[indexPath.row].first_name, self.address[indexPath.row].last_name);
        
        cell.lblPhoneNumber.text = self.address[indexPath.row].phone;
        cell.lblEmail.text = self.address[indexPath.row].email;
        
        cell.lblAddress.text = String(format: "%@, %@", beforeAddres, lastAddress);
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController;
        vc.address = self.address[indexPath.row];
        vc.isEditAddress = true;
        vc.type = self.type;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete)
        {
            self.startLoader()
            ApiHelper.saveAddress(lang: _lang[0], address: self.address[indexPath.row], delete: "1", controller: self, completionHandler: {
                self.address.removeAll();
                self.tableViewAddressList.reloadData();
                self.stopLoader();
                self.refreshScreen();
            }, errorHandler: self.apiCallErrorHandler)
        }
    }
}
