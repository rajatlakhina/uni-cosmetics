//
//  LanguageTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblLanguageName: LabelProperties!
    @IBOutlet weak var lblWebsiteName: LabelProperties!
    
    @IBOutlet weak var imgRadio: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
