//
//  LanguageSettingsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class LanguageSettingsViewController: UIViewControllerBase, UITableViewDelegate, UITableViewDataSource, LeftMenuViewController
{
    
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    
    @IBOutlet weak var lblLanguageName: LabelProperties!
    
    @IBOutlet weak var tableViewLanguages: UITableView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    var previousLanguage = [PreviouslyVisitedLanguage]();
    var selectedLanguageIndex:Int = 0;
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
        let previous_language = PreviouslyVisitedLanguage();
        previous_language.language_id = 1;
        previous_language.language_image = "flag";
        previous_language.language_name = "United States - English";
        previous_language.language_website = "unicosmetics.net";
        
        self.previousLanguage.append(previous_language);
        
        self.tableViewLanguages.register(UINib(nibName: "LanguageTableViewCell", bundle: nil), forCellReuseIdentifier: "LanguageTableViewCell");
        
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    @IBAction func btnLanguageAction(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- TABLEVIEW DELEGATES AND DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.previousLanguage.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as! LanguageTableViewCell;
        
        cell.lblLanguageName.text = self.previousLanguage[indexPath.row].language_name;
        cell.lblWebsiteName.text = self.previousLanguage[indexPath.row].language_website;
        cell.imgCountryFlag.image = UIImage(named: self.previousLanguage[indexPath.row].language_image);
        
        if self.selectedLanguageIndex == self.previousLanguage[indexPath.row].language_id
        {
            cell.imgRadio.image = UIImage(named: "ic_radio_selected");
        }
        else
        {
            cell.imgRadio.image = UIImage(named: "ic_radio_unselected");
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.selectedLanguageIndex = self.previousLanguage[indexPath.row].language_id;
        self.tableViewLanguages.reloadData();
    }
}
