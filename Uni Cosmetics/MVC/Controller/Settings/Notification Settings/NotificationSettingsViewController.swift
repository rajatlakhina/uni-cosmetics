//
//  NotificationSettingsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var btnShowAccountNotification: UIButton!
    @IBOutlet weak var vwAccountNotificationDetailsContainer: UIView!
    @IBOutlet weak var btnAccountNotification: UIButton!
    @IBOutlet weak var btnShowShipmentNotification: UIButton!
    @IBOutlet weak var vwShipmentNotificationDetailsContainer: UIView!
    @IBOutlet weak var btnShipmentNotification: UIButton!
    @IBOutlet weak var btnShowRecommendationNotification: UIButton!
    @IBOutlet weak var vwRecommendationNotificationDetailsContainer: UIView!
    @IBOutlet weak var btnRecommendationNotification: UIButton!
    @IBOutlet weak var btnShowUpcomingDealsNotification: UIButton!
    @IBOutlet weak var vwUpcomingDealsNotificationDetailsContainer: UIView!
    @IBOutlet weak var btnUpcomingDealsNotification: UIButton!
    @IBOutlet weak var btnShowSaveListNotification: UIButton!
    @IBOutlet weak var vwSaveListNotificationDetailsContainer: UIView!
    @IBOutlet weak var btnSaveListNotification: UIButton!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
        self.setupUI();
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    func setupUI()
    {
        self.vwAccountNotificationDetailsContainer.isHidden = true;
        self.vwSaveListNotificationDetailsContainer.isHidden = true;
        self.vwShipmentNotificationDetailsContainer.isHidden = true;
        self.vwUpcomingDealsNotificationDetailsContainer.isHidden = true;
        self.vwRecommendationNotificationDetailsContainer.isHidden = true;
        
        self.btnAccountNotification.isSelected = false;
        self.btnSaveListNotification.isSelected = false;
        self.btnShipmentNotification.isSelected = false;
        self.btnUpcomingDealsNotification.isSelected = false;
        self.btnRecommendationNotification.isSelected = false;
        
        self.btnShowAccountNotification.isSelected = false;
        self.btnShowSaveListNotification.isSelected = false;
        self.btnShowShipmentNotification.isSelected = false;
        self.btnShowUpcomingDealsNotification.isSelected = false;
        self.btnShowRecommendationNotification.isSelected = false;
    }
    
    //MAKR:- TOGGLE ON BUTTON TAP
    
    func toggleToShowDetailsAction (tag:Int)
    {
        let btn = self.view.viewWithTag(tag) as? UIButton;
        
        let imgTag:Int = ((tag * 10) + 1);
        let imgArrow = self.view.viewWithTag(imgTag) as? UIImageView;
        
        let viewTag:Int = (tag * 100);
        let view = self.view.viewWithTag(viewTag);
        
        if (btn?.isSelected)!
        {
            btn?.isSelected = false;
            imgArrow!.image = UIImage(named: "arrow_down");
            view?.isHidden = true;
        }
        else
        {
            btn?.isSelected = true;
            imgArrow!.image = UIImage(named: "arrow_up");
            view?.isHidden = false;
        }
    }
    
    func toggleButtonAction(tag:Int)
    {
        let btn = self.view.viewWithTag(tag) as? UIButton;
        
        if (btn?.isSelected)!
        {
            btn?.isSelected = false;
        }
        else
        {
            btn?.isSelected = true;
        }
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnShowAccountNotificationAction(_ sender: UIButton)
    {
        self.toggleToShowDetailsAction(tag: sender.tag);
    }
    
    @IBAction func btnAccountNotificationAction(_ sender: UIButton)
    {
        self.toggleButtonAction(tag: sender.tag);
    }
    
    @IBAction func btnShowShipmentNotificationAction(_ sender: UIButton)
    {
        self.toggleToShowDetailsAction(tag: sender.tag);
    }
    
    @IBAction func btnShipmentNotificationAction(_ sender: UIButton)
    {
        self.toggleButtonAction(tag: sender.tag);
    }
    
    @IBAction func btnShowRecommendationNotificationAction(_ sender: UIButton)
    {
        self.toggleToShowDetailsAction(tag: sender.tag);
    }
    
    @IBAction func btnRecommendationNotificationAction(_ sender: UIButton)
    {
        self.toggleButtonAction(tag: sender.tag);
    }
    
    @IBAction func btnShowUpcomingDealsNotificationAction(_ sender: UIButton)
    {
        self.toggleToShowDetailsAction(tag: sender.tag);
    }
    
    @IBAction func btnUpcomingDealsNotificationAction(_ sender: UIButton)
    {
        self.toggleButtonAction(tag: sender.tag);
    }
    
    @IBAction func btnShowSaveListNotificationAction(_ sender: UIButton)
    {
        self.toggleToShowDetailsAction(tag: sender.tag);
    }
    
    @IBAction func btnSaveListNotificationAction(_ sender: UIButton)
    {
        self.toggleButtonAction(tag: sender.tag);
    }
}
