//
//  FAQsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 23/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class FAQsViewController: UIViewControllerBase, LeftMenuViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var tableViewFAQs: UITableView!
    @IBOutlet weak var webViewFAQ: UIWebView!
    
    //MARK:- VARIABLE DECLARATION
    
    var expandedRows = Set<Int>()
    let answer:String = "Just as Uni Cosmatics Simple Storage Service (Uni Cosmatics S3) enables storage in the cloud, Uni Cosmatics EC2 enables “compute” in the cloud. Uni Cosmatics EC2’s simple web service interface allows you to obtain and configure capacity with minimal friction. It provides you with complete control of your computing resources and lets you run on Uni Cosmatics’s proven computing environment. Uni Cosmatics EC2 reduces the time required to obtain and boot new server instances to minutes, allowing you to quickly scale capacity, both up and down, as your computing requirements change. Uni Cosmatics EC2 changes the economics of computing by allowing you to pay only for capacity that you actually use.";
//    var detailExpansion = [Int:Bool]();
//    var itemSelected = [Int:Bool]();
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.startLoader();
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.tableViewFAQs.register(UINib(nibName: "FAQsTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQsTableViewCell");
        self.vwHeader.btnBack.isHidden = true;
        self.webViewFAQ.scrollView.bounces = false;
        self.webViewFAQ.scrollView.showsVerticalScrollIndicator = false;
        self.webViewFAQ.scrollView.showsHorizontalScrollIndicator = false;
        
        self.startLoader()
        ApiHelper.faq(lang: "en", controller: self, completionHandler: {
            (data) -> () in
            self.stopLoader()
            //            self.txtViewAboutUs.text = data.content.htmlToString;
            
            self.webViewFAQ.loadHTMLString(data.content, baseURL: nil);
            self.lblTitle.text = data.title;
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQsTableViewCell") as! FAQsTableViewCell
        
        cell.selectionStyle = .none;
        
        cell.lblQuestion.text = "What can I do with Uni Cosmatics EC2?"
        
        cell.isExpanded = self.expandedRows.contains(indexPath.row)
        
        cell.lblAnswer.text = self.answer;
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        print(indexPath.row)
        
        guard let cell = tableView.cellForRow(at: indexPath) as? FAQsTableViewCell
            
            else { return }
        cell.answerText = self.answer;
        
        
        switch cell.isExpanded
            
        {
            
        case true:
            
            self.expandedRows.remove(indexPath.row)
            
        case false:
            
            self.expandedRows.insert(indexPath.row)
            
        }
        
        
        
        cell.isExpanded = !cell.isExpanded
        
        
        
        self.tableViewFAQs.beginUpdates()
        
        self.tableViewFAQs.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? FAQsTableViewCell
            
            else { return }
        
        self.expandedRows.remove(indexPath.row)
        
        
        cell.isExpanded = false
        
        self.tableViewFAQs.beginUpdates()
        
        self.tableViewFAQs.endUpdates()
        
    }
    
    
}
