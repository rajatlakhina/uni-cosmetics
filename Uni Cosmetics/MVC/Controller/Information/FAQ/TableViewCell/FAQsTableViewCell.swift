//
//  FAQsTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 23/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class FAQsTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var lblQuestion: LabelProperties!
    @IBOutlet weak var lblAnswer: LabelProperties!
    @IBOutlet weak var heightLblAnswer: NSLayoutConstraint!
    @IBOutlet weak var lblExpand: LabelProperties!
    
    var answerText = String();
    
    var isExpanded:Bool = false
    {
        didSet
        {
            if !isExpanded {
                self.heightLblAnswer.constant = 0.0
                self.lblExpand.text = "+";
                
            }
            else
            {
                self.lblExpand.text = "-";

                //self.lblAnswer.text = self.answerText;
                self.heightLblAnswer.constant = CommonFunctions.heightForView(text: self.answerText, font: self.lblAnswer.font, width: self.lblAnswer.frame.width)
                    //CommonFunctions.getLabelHeight(label: self.lblAnswer) + 10;
            }
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
