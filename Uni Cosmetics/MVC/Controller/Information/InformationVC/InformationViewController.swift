//
//  InformationViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 22/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class InformationViewController: UIViewControllerBase, LeftMenuViewController
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var lblInformatioTitle: LabelProperties!
    @IBOutlet weak var txtViewInformation: UITextView!
    @IBOutlet weak var webViewInformation: UIWebView!
    //MARK:- VARIABLE DECLARATION
    
    var info_title = String();
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = true;
        self.webViewInformation.scrollView.bounces = false;
        self.webViewInformation.scrollView.showsVerticalScrollIndicator = false;
        self.webViewInformation.scrollView.showsHorizontalScrollIndicator = false;
    }
    
    override func refreshScreen()
    {
        let information = ["How to buy", "Payment Methods", "Payments & Shipping", "FAQs", "Customer Protection", "Terms & Conditions" ];
        
        self.info_title = information[_info_screen];
        
        var url:String = "";
        switch self.info_title
        {
        case "How to buy":
            url = _howToBuyInfomation;
            break
        case "Payment Methods":
            url = _paymentmethodsInformation;
            break
        case "Payments & Shipping":
            url = _productShippingInformation;
            break
        case "Customer Protection":
            url = _customerProtection;
            break
        case "Terms & Conditions":
            url = _terms;
            break
        default:
            url = _terms;
            break
        }
        self.startLoader();
        ApiHelper.information(url: url, lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            switch self.info_title
            {
            case "How to buy":
                let content = HowToBuyInformation(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            case "Payment Methods":
                let content = PaymentMethodsInformation(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            case "Payments & Shipping":
                let content = ProductShipping(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            case "Customer Protection":
                let content = CustomerProtection(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            case "Terms & Conditions":
                let content = Terms(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            default:
                let content = Terms(dictionary: data);
                self.webViewInformation.loadHTMLString(content.content, baseURL: nil);
                break
            }
            
        }, errorHandler: self.apiCallErrorHandler)
        
        
        self.lblInformatioTitle.text = info_title;
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
}
