//
//  TabDetailsTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat Lakhina on 22/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class TabDetailsTableViewCell: UITableViewCell, UIWebViewDelegate {
    @IBOutlet weak var lblDescription: LabelProperties!
    @IBOutlet weak var webViewDetails: UIWebView!
    
    //@IBOutlet weak var txtDetails: LabelProperties!
    @IBOutlet weak var txtDetails: UILabel!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(CGSize.zero)
    }
    
}
