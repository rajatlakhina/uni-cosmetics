//
//  ProductDetailsTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat Lakhina on 22/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblProductName: LabelProperties!
    @IBOutlet weak var lblCurrentPrice: LabelProperties!
    
    @IBOutlet weak var lblNewPrice: LabelProperties!
    
    @IBOutlet weak var lblCode: LabelProperties!
    
    @IBOutlet weak var lblStatus: LabelProperties!
    
    @IBOutlet weak var lblDescription: LabelProperties!
    
    @IBOutlet weak var lblQuantity: LabelProperties!
    
    @IBOutlet weak var btnMinus: UIButton!
    
    @IBOutlet weak var lblTimer: LabelProperties!
    @IBOutlet weak var btnPlus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
