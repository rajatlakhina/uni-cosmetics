//
//  RatingsTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class RatingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblComment: LabelProperties!
    @IBOutlet weak var lblUserRatings: LabelProperties!
    @IBOutlet weak var lblUser: LabelProperties!
    @IBOutlet weak var imgUser: ImageViewProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
