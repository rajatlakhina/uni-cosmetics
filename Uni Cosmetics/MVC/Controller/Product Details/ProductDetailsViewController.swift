//
//  ProductDetailsViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit
import WebKit

class ProductDetailsViewController: UIViewControllerBase, LeftMenuViewController , UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIWebViewDelegate
{
    //MARK:- OUTLET
    
    //@IBOutlet weak var stackViewButtonContainer: UIStackView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionviewImage: UICollectionView!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var tableViewProductDetails: UITableView!
    @IBOutlet weak var vwHeader: SecondaryHeaderView!
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    @IBOutlet weak var lblProductName: LabelProperties!
    
    @IBOutlet weak var lblCurrentPrice: LabelProperties!
    
    @IBOutlet weak var lblEarlierPrice: LabelProperties!
    
    @IBOutlet weak var lblQuantity: LabelProperties!
    
    @IBOutlet weak var btnReduceQuantity: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var vwDiscount: UIView!
    
    @IBOutlet weak var lblDiscount: LabelProperties!
    
    @IBOutlet weak var heightTableViewProductDetails: NSLayoutConstraint!
    
    @IBOutlet weak var heightImageCollectionView: NSLayoutConstraint!
    //    @IBOutlet weak var lblWorldWideWeb: LabelProperties!
//
//    @IBOutlet weak var webViewWorldWideWeb: UIWebView!
//
//    @IBOutlet weak var heightWebViewWorldWideShopping: NSLayoutConstraint!
//    @IBOutlet weak var lblDescription: LabelProperties!
//
//    @IBOutlet weak var webViewDescription: UIWebView!
//
//    @IBOutlet weak var heightWebViewDescription: NSLayoutConstraint!
//    @IBOutlet weak var lblTab2: LabelProperties!
//    @IBOutlet weak var webViewTab2: UIWebView!
//    @IBOutlet weak var heightWebViewTab2: NSLayoutConstraint!
    
    //MARK:- VARIABLE DECLARATION
    
    var product_id = String();
    var productData:Product! = nil;
    var tabdata:Tabsdata! = nil;
    var world_wide_shipping: TabData! = nil;
    var time:Double = 0;
    var productTimer: Timer!
    var heightWebView:CGFloat = 0;
    
    //var productDetails:ProductDetails! = nil;
    var quantity:Int = 1;
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        self.tableViewProductDetails.register(UINib(nibName: "ProductDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductDetailsTableViewCell");
        self.tableViewProductDetails.register(UINib(nibName: "TabDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "TabDetailsTableViewCell");
        self.collectionviewImage.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell");
        
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        self.vwHeader.btnBack.isHidden = false;
        self.vwHeader.btnBack.addTarget(self, action: #selector(self.btnBackAction(_:)), for: .touchUpInside);
        self.vwHeader.btnSearch.isHidden = true;
        //self.stackViewButtonContainer.isHidden = true;

        self.pageControl.isHidden = true;        self.pageControl.pageIndicatorTintColor = .lightGray;
        self.pageControl.currentPageIndicatorTintColor = primaryPinkColor;
        
        self.heightImageCollectionView.constant = UIScreen.main.bounds.size.height / 2;
        self.getProductDetails();
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
    }
    
    override func postRefreshScreen()
    {
        self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
        productTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    func getProductDetails()
    {
        self.startLoader();
        ApiHelper.getProductDetails(lang: "en", id: self.product_id, controller: self, completionHandler: {
            (content) in
            self.stopLoader();
            
            self.productData = Product(dictionary: (content["productData"] as! NSDictionary));
            
            
            self.tabdata = Tabsdata(dictionary: (content["tabdata"] as! NSDictionary));
            self.world_wide_shipping = TabData(dictionary: (content["world_wide_shipping"] as! NSDictionary));
                //ApiObject.deserializeArray(array: (content["tabdata"] as! NSArray)) as [TabData]
            
            self.collectionviewImage.reloadData();
            self.pageControl.numberOfPages = self.productData.images.count;
                self.pageControl.isHidden = false

            self.tableViewProductDetails.reloadData();
            //self.stackViewButtonContainer.isHidden = false;
            self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
//            self.assignValues()
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION HANDLER
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func btnAddQuantityAction(_ sender: UIButton)
    {
        self.quantity += 1
        let indexPath = IndexPath(row: 0, section: 0);
        self.tableViewProductDetails.beginUpdates()
        self.tableViewProductDetails.reloadRows(at: [indexPath], with: .none);
        self.tableViewProductDetails.endUpdates();
    }
    
    @IBAction func btnReduceQuantityAction(_ sender: UIButton)
    {
        if self.quantity > 1
        {
            self.quantity -= 1
            let indexPath = IndexPath(row: 0, section: 0);
            self.tableViewProductDetails.beginUpdates()
            self.tableViewProductDetails.reloadRows(at: [indexPath], with: .none);
            self.tableViewProductDetails.endUpdates();
        }
    }
    
    @IBAction func btnAddToBasketAction(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnAddToWishListAction(_ sender: UIButton)
    {
    }
    
    //MARK:- TIMER HANDLER METHOD
    
    @objc func runTimedCode(cell1: ProductDetailsTableViewCell)
    {
        self.time += 1
        print(time)
        let indexPath = IndexPath(row: 0, section: 0);
        
        if !CommonFunctions.isNull(s: self.productData)
        {
            if self.productData.dela_end_date != 0
            {
                self.tableViewProductDetails.beginUpdates()
                self.tableViewProductDetails.reloadRows(at: [indexPath], with: .none);
                self.tableViewProductDetails.endUpdates();
                
            }
        }
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var section_count:Int = 0;
        if !CommonFunctions.isNull(s: self.productData)
        {
            section_count += 1;
        }
        if !CommonFunctions.isNull(s: self.world_wide_shipping)
        {
            section_count += 1
        }
        if !CommonFunctions.isNull(s: self.tabdata) && !CommonFunctions.isNull(s: self.tabdata.tab1)
        {
            section_count += 1
        }
        if !CommonFunctions.isNull(s: self.tabdata) && !CommonFunctions.isNull(s: self.tabdata.tab2)
        {
            section_count += 1
        }

        return section_count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            if !CommonFunctions.isNull(s: self.productData)
            {
                self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else if section == 1
        {
            if !CommonFunctions.isNull(s: self.world_wide_shipping)
            {
                self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
                return 1;
                //return self.tabdata.count;
            }
            else
            {
                return 0;
            }
        }
        else if section == 2
        {
            if !CommonFunctions.isNull(s: self.tabdata)
            {
                self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
                return 1;
                //return self.tabdata.count;
            }
            else
            {
                return 0;
            }
        }
        else if section == 3
        {
            if !CommonFunctions.isNull(s: self.tabdata)
            {
                self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
                return 1;
                //return self.tabdata.count;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell") as! ProductDetailsTableViewCell;
            cell.selectionStyle = .none;
            
            cell.lblProductName.text = String(htmlEncodedString: self.productData.item_name);
            
            if self.productData.sale_price == "0"
            {
                cell.lblNewPrice.isHidden = true;
                let price:Double = Double(self.productData.price)!
                cell.lblCurrentPrice.text = String(format: "%.2f%@", price, _currency_symbol);
            }
            else
            {
                cell.lblNewPrice.isHidden = false;
                let price:Double = Double(self.productData.sale_price)!
                cell.lblCurrentPrice.text = String(format: "%.2f%@", price, _currency_symbol);
                let price1:Double = Double(self.productData.price)!
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f%@", price1, _currency_symbol));
                
                attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
                
                cell.lblNewPrice.attributedText = attributeString;
            }
            
            cell.lblCode.text = self.productData.sku;
            
            if self.productData.status == "1"
            {
                cell.lblStatus.textColor = greenColorRGB;
                cell.lblStatus.text = "In stocks";
            }
            else
            {
                cell.lblStatus.textColor = .red;
                cell.lblStatus.text = "Out of stocks";
            }
            
            if self.productData.dela_end_date != 0
            {
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: productData.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblTimer.text = "\(remaning_time)";
            }
            else
            {
                cell.lblTimer.text = "";
            }
            
            cell.lblDescription.text = String(htmlEncodedString: self.productData._description);
            cell.lblQuantity.text = String(format: "%ld", quantity);
            
            if self.quantity == 1
            {
                cell.btnMinus.isEnabled = false;
                cell.btnMinus.setImage(UIImage(named: "ic_minus_gray"), for: .normal);
            }
            else if self.quantity > 1
            {
                cell.btnMinus.isEnabled = true;
                cell.btnMinus.setImage(UIImage(named: "ic_minus"), for: .normal);
            }
            
            cell.btnPlus.addTarget(self, action: #selector(self.btnAddQuantityAction(_:)), for: .touchUpInside);
            
            cell.btnMinus.addTarget(self, action: #selector(self.btnReduceQuantityAction(_:)), for: .touchUpInside);
            
            return cell;
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TabDetailsTableViewCell") as! TabDetailsTableViewCell;
            cell.selectionStyle = .none;

            cell.lblDescription.text = String(htmlEncodedString: self.world_wide_shipping.title);
            cell.lblDescription.textColor = primaryPinkColor;
            cell.txtDetails.attributedText = try! NSAttributedString(
                data: self.world_wide_shipping.desc.replacingOccurrences(of: "\r\n", with: "<br>").data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                //self.world_wide_shipping.desc.replacingOccurrences(of: "\r\n", with: "<br>").htmlToAttributedString
//            cell.webViewDetails.loadHTMLString(self.world_wide_shipping.desc, baseURL: nil);
//            cell.webViewDetails.delegate = self;
            
//            cell.updateConstraints()
//
            self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
            return cell;
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TabDetailsTableViewCell") as! TabDetailsTableViewCell;
            cell.selectionStyle = .none;
            
            cell.lblDescription.text = String(htmlEncodedString: self.tabdata.tab1.title);
            cell.lblDescription.textColor = .black;
            cell.txtDetails.attributedText = try! NSAttributedString(
                data: self.tabdata.tab1.desc.replacingOccurrences(of: "\r\n", with: "<br>").data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//            let string:String = self.tabdata.tab1.desc.replacingOccurrences(of: "\r\n", with: "<br>").htmlToString
//
//            let font = cell.txtDetails.font
//            let attributes = [NSAttributedStringKey.font: font]
//            let attributedQuote = NSAttributedString(string: string, attributes: attributes as [NSAttributedStringKey : Any])
//
//            cell.txtDetails.attributedText = attributedQuote
            self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
            return cell;
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TabDetailsTableViewCell") as! TabDetailsTableViewCell;
            cell.selectionStyle = .none;
            
            cell.lblDescription.textColor = .black;
            cell.lblDescription.text = String(htmlEncodedString: self.tabdata.tab2.title);
            
            cell.txtDetails.adjustsFontSizeToFitWidth = true
            cell.txtDetails.minimumScaleFactor = 0.5
            cell.txtDetails.attributedText = try! NSAttributedString(
                data: self.tabdata.tab2.desc.replacingOccurrences(of: "\r\n", with: "<br>").data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
//            let string:String = self.tabdata.tab2.desc.replacingOccurrences(of: "\r\n", with: "<br>").htmlToString
//
//            let font = cell.txtDetails.font
//            let attributes = [NSAttributedStringKey.font: font]
//            let attributedQuote = NSAttributedString(string: string, attributes: attributes as [NSAttributedStringKey : Any])
//
//            cell.txtDetails.attributedText = attributedQuote
            
//            cell.webViewDetails.loadHTMLString(self.tabdata.tab2.desc, baseURL: nil);
//
//            cell.webViewDetails.delegate = self;
//
//            cell.updateConstraints()
//
            self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height;
//
            return cell;
        }
    }
    
    //MARK:- COLLECTIONVIEW DELEGATES AND DATASOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if !CommonFunctions.isNull(s: self.productData) && !CommonFunctions.isNull(s: self.productData.images)
        {
            return self.productData.images.count
        }
        else
        {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell;
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        
        let url = self.productData.images[indexPath.item];
        let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);

        cell.imgBanner.sd_setImage(with: URL(string: fileUrl ), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
        cell.imgBanner.backgroundColor = .clear;
        cell.imgBanner.contentMode = .scaleAspectFit;
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.width), height: (collectionView.frame.height));
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currentIndex = self.collectionviewImage.contentOffset.x / self.collectionviewImage.frame.size.width;
        self.pageControl.currentPage = Int(currentIndex);
    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//
//        if webView == self.webViewDescription
//        {
//            webView.frame.size.height = 1
//            print(webView.frame.height)
//            print(webView.scrollView.contentSize.height)
//            webView.frame.size = webView.scrollView.contentSize
//            self.heightWebViewDescription.constant = webView.scrollView.contentSize.height
//            self.webViewDescription.updateConstraints()
//        }
//        if webView == self.webViewTab2
//        {
//            webView.frame.size.height = 1
//            print(webView.frame.height)
//            print(webView.scrollView.contentSize.height)
//            webView.frame.size = webView.scrollView.contentSize
//            self.heightWebViewTab2.constant = webView.scrollView.contentSize.height
//            self.webViewTab2.updateConstraints()
//        }
//        if webView == self.webViewWorldWideWeb
//        {
//            webView.frame.size.height = 1
//            print(webView.frame.height)
//            print(webView.scrollView.contentSize.height)
//            webView.frame.size = webView.scrollView.contentSize
//            self.heightWebViewWorldWideShopping.constant = webView.scrollView.contentSize.height
//            self.webViewWorldWideWeb.updateConstraints()
//        }
//        self.containerScrollView.layoutSubviews();
//        self.containerScrollView.updateConstraints();
//        self.updateViewConstraints();
//
////        self.heightWebView += webView.scrollView.contentSize.height;
//        //self.setTableHeight();
//    }
    
//    func setTableHeight()
//    {
//        self.heightTableViewProductDetails.constant = self.tableViewProductDetails.contentSize.height + self.heightWebView;
//        self.tableViewProductDetails.updateConstraints();
//        self.view.updateConstraints();
//    }
    
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        webView.frame.size.height = 1
//        webView.frame.size = webView.scrollView.contentSize
//    }
}
