//
//  ForgotPasswordViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewControllerBase
{
    //MARK:- OUTLETS
    @IBOutlet weak var txtEmail: UITextFieldProperties!
    
    //MARK:- VARIABLE DECLARATION
    
    override func initialiseScreen()
    {
        
    }
    
    //MARK:- LIFE CYCLE
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnContinueAction(_ sender: UIButton)
    {
        if CommonFunctions.isEmpty(s:self.txtEmail.text, checkWhitespace: true)
        {
            Toast().showToast(message: "Please enter email !!", duration: 3);
            return
        }
        if !CommonFunctions.isValidEmail(email: self.txtEmail.text!)
        {
            Toast().showToast(message: "Please enter valid email !!", duration: 3);
            return
        }
        
        self.startLoader();
        
        ApiHelper.forgotPassword(email: self.txtEmail.text!, lang: _lang[0], controller: self, completionHandler: {
            self.stopLoader();
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                // Put your code which should be executed with a delay here
                self.navigationController?.popViewController(animated: true);
            })
            
        }, errorHandler: self.apiCallErrorHandler)
    }
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true);
    }
}
