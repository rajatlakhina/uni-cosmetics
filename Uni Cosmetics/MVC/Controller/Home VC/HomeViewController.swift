//
//  HomeViewController.swift
//  Uni Cosmetics
//
//  Created by Rajat on 30/10/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewControllerBase, LeftMenuViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource
{
    //MARK:- OUTLETS
    @IBOutlet var vwBaseContainer: UIView!
    @IBOutlet weak var vwHeader: HeaderView!
    @IBOutlet weak var tableViewHome: UITableView!
    @IBOutlet weak var leftDrawerMenuBackground: UIVisualEffectView!
    
    //MARK:- VARIABLE DECLARATION
    
    let banner = ["home_banner"];
    var time:Double = 0;
    var homeData = Home();
    var productTimer: Timer!
    
    //MARK:- LEFT DRAWER VARIABLES
    
    var baseView: UIView
    {
        get
        {
            return (self.vwBaseContainer);
        }
    }
    
    var leftMenuBackground: UIView
    {
        get
        {
            return (self.leftDrawerMenuBackground);
        }
    }
    
    //MARK:- LIFE CYCLE
    
    override func initialiseScreen()
    {
        StateHelper.setIsUserIn(value: true);
        //        self.heightProductTableView.constant = self.productsTableView.contentSize.height;
        self.vwHeader.btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)), for: .touchUpInside);
        
        self.tableViewHome.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell");
        
        self.tableViewHome.register(UINib(nibName: "HomeFooterTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeFooterTableViewCell");
        
        self.tableViewHome.isHidden = true;
//        self.tableViewHome.register(UINib(nibName: "HomeSectionFooterTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeSectionFooterTableViewCell");
//        self.tableViewHome.register(UINib(nibName: "ProductSortOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductSortOptionTableViewCell");
    }
    
    override func refreshScreen()
    {
        self.initialiseLeftMenu(viewControllerBase: self);
        self.getHomeData();
    }
    
    @objc func runTimedCode(cell1: HomeTableViewCell)
    {
        self.time += 1
        print(time)
        let indexPath = IndexPath(row: 0, section: 2);
        let indexPath1 = IndexPath(row: 0, section: 3);
        let indexPath2 = IndexPath(row: 0, section: 4);
        let indexPath3 = IndexPath(row: 0, section: 1);
        
        let cell4 = self.tableViewHome.cellForRow(at: indexPath3)
        let cell1 = self.tableViewHome.cellForRow(at: indexPath)
        let cell2 = self.tableViewHome.cellForRow(at: indexPath1)
        let cell3 = self.tableViewHome.cellForRow(at: indexPath2)
        
        if !CommonFunctions.isNull(s: cell1) && !CommonFunctions.isNull(s: self.homeData.weekly)
        {
            self.ButtonTappedWeekly(cell1: cell1 as! HomeTableViewCell)
        }
        if !CommonFunctions.isNull(s: cell2) && !CommonFunctions.isNull(s: self.homeData.monthly)
        {
            self.ButtonTappedWeekly(cell1: cell2 as! HomeTableViewCell)
        }
        if !CommonFunctions.isNull(s: cell3) && !CommonFunctions.isNull(s: self.homeData.perfume)
        {
            self.ButtonTappedWeekly(cell1: cell3 as! HomeTableViewCell)
        }
        
        if !CommonFunctions.isNull(s: cell4) && !CommonFunctions.isNull(s: self.homeData.banner)
        {
            self.ButtonTappedWeekly(cell1: cell4 as! HomeTableViewCell)
        }
    }
    override func postRefreshScreen()
    {
        
        productTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
        
    }
    
    override func screenDisappear()
    {
        self.time = 0;
        productTimer.invalidate();
    }
    
    func ButtonTappedWeekly(cell1: HomeTableViewCell)
    {
        
        cell1.collectionViewCatagories.reloadData();
    }
    
    func getHomeData()
    {
        self.startLoader();
        ApiHelper.getHomeData(lang: "en", controller: self, completionHandler: {
            (data) in
            self.stopLoader();
            
            self.homeData = data;
            self.tableViewHome.reloadData();
            self.tableViewHome.isHidden = false;
        }, errorHandler: self.apiCallErrorHandler)
    }
    
    //MARK:- BUTTON ACTION HANDLER
    
    @IBAction func btnMenuAction(_ sender: UIButton)
    {
        self.toggleLeftMenu();
    }
    
    @IBAction func btnSeeMoreAction(_ sender: UIButton)
    {
        if sender.tag > 5000
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CatagoriesListViewController") as! CatagoriesListViewController
            vc.cat_id = self.homeData.category[(sender.tag - 1) % 5000].cat_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllDealsViewController") as! AllDealsViewController
            vc.tableSectionIndex = sender.tag - 3;
            self.navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 7;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0 || section == 1 || section == 6 // banner, Image , Bottom
        {
            return 1;
        }
        else if section == 2 // offers
        {
            var section_rows:Int = 0;
            
            if !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                section_rows += 1;
            }
            
            return section_rows;
        }
        else if section == 3 // offers
        {
            var section_rows:Int = 0;
            
            if !CommonFunctions.isNull(s: self.homeData.monthly)
            {
                section_rows += 1;
            }
            
            return section_rows;
        }
        else if section == 4 // offers
        {
            var section_rows:Int = 0;
            
            if !CommonFunctions.isNull(s: self.homeData.perfume)
            {
                section_rows += 1;
            }
            
            return section_rows;
        }
        else if section == 5
        {
            if !CommonFunctions.isNull(s: self.homeData.category)
            {
                return self.homeData.category.count
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 // banner
        {
            if !CommonFunctions.isNull(s: self.homeData.banner)
            {
                return tableView.frame.size.width / 1.5;
            }
            else
            {
                return 0;
            }
        }
        else if indexPath.section == 1 // static image
        {
            return 85
        }
        else if indexPath.section == 2 // super weekly offer
        {
            if !CommonFunctions.isNull(s: self.homeData.weekly)
            {
                let count:Double = Double(self.homeData.weekly.weekly_products.count / 3)
                let row = ceil(count);
                
                return (CGFloat(row * 200) + 110);
            }
            else
            {
                return 0
            }
        }
        else if indexPath.section == 3
        {
            if !CommonFunctions.isNull(s: self.homeData.monthly)
            {
                let count:Double = Double(self.homeData.monthly.monthly_products.count / 3)
                let row = ceil(count);
                
                return (CGFloat(row * 200) + 110);
            }
            else
            {
                return 0
            }
        }
        else if indexPath.section == 4 // monthly
        {
            if !CommonFunctions.isNull(s: self.homeData.perfume)
            {
                let count:Double = Double(self.homeData.perfume.perfume_products.count / 3)
                let row = ceil(count);
                
                return (CGFloat(row * 200) + 110);
            }
            else
            {
                return 0
            }
        }
        else if indexPath.section == 5 // Catagories
        {

            if !CommonFunctions.isNull(s: self.homeData.category)
            {
                if self.homeData.category[indexPath.row].cat_id == "909" || self.homeData.category[indexPath.row].cat_id == "898"
                {
                    var count:Double = 0;
                    if self.homeData.category[indexPath.row].subCatData.count % 2 == 0
                    {
                        count = Double(self.homeData.category[indexPath.row].subCatData.count / 2)
                    }
                    else
                    {
                        count = Double((self.homeData.category[indexPath.row].subCatData.count / 2) + 1);
                    }
                    
                    let row = ceil(count);

                    let height = ((tableView.frame.size.width - 20) / 2);
                    
                    return CGFloat((CGFloat(row) * height) + 110); //515
                }
                else
                {
                    var count:Double = 0;
                    if self.homeData.category[indexPath.row].subCatData.count % 3 == 0
                    {
                        count = Double(self.homeData.category[indexPath.row].subCatData.count / 3)
                    }
                    else
                    {
                        count = Double((self.homeData.category[indexPath.row].subCatData.count / 3) + 1);
                    }
                    
                    let row = ceil(count);

                    let height = ((tableView.frame.size.width) / 3);
                    
                    let space:CGFloat  = CGFloat((row - 1) * 7);

                    if self.homeData.category[indexPath.row].cat_id == "632" || self.homeData.category[indexPath.row].cat_id == "255"
                    {
                        return CGFloat(((CGFloat(row) * height) + space) + 75);
                    }
                    else
                    {
                        return CGFloat(((CGFloat(row) * height) + space) + 115);
                    }
                }
            }
            else
            {
                return 0
            }
        }
        else if indexPath.section == 6
        {
            return 100
        }
        else
        {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFooterTableViewCell") as! HomeFooterTableViewCell
            cell.selectionStyle = .none;
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
            cell.selectionStyle = .none;
            
            cell.vwHeader.isHidden = true
            cell.collectionViewCatagories.isHidden = true;
            cell.vwBotton.isHidden = true;
            cell.imgBottom.isHidden = true;

            if indexPath.section == 0 || indexPath.section == 1
            {
                cell.imgBottom.isHidden = true;
                cell.vwBotton.isHidden = true;
                cell.vwHeader.isHidden = true;
                cell.containerStackView.spacing = 0;
                cell.collectionViewCatagories.isHidden = false;
                cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section + 1);
                if indexPath.section == 0
                {
                    cell.collectionViewCatagories.isScrollEnabled = true;
                }
                else
                {
                   cell.collectionViewCatagories.isScrollEnabled = false;
                }
                
            }
            else
            {
                cell.containerStackView.spacing = 5;
                cell.imgBottom.isHidden = false;
                cell.collectionViewCatagories.isScrollEnabled = false;
                
                if indexPath.section == 2
                {
                    cell.vwHeader.isHidden = false;
                    cell.vwBotton.isHidden = false;
                    cell.lblTitle.text = self.homeData.weekly.name;
                    cell.imgTitleBottom.isHidden = true;
                    cell.lblBtnTitle.text = "See More";
                    cell.collectionViewCatagories.isHidden = false;
                    cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section + 1);
                    cell.btnSeeMore.tag = indexPath.section + 1;
                }
                if indexPath.section == 3
                {
                    cell.vwHeader.isHidden = false;
                    cell.vwBotton.isHidden = false;
                    cell.lblTitle.text = self.homeData.monthly.name;
                    cell.imgTitleBottom.isHidden = true;
                    cell.lblBtnTitle.text = "See More";
                    cell.collectionViewCatagories.isHidden = false;
                    cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section + 1);
                    cell.btnSeeMore.tag = indexPath.section + 1;
                }
                if indexPath.section == 4
                {
                    cell.vwHeader.isHidden = false;
                    cell.vwBotton.isHidden = false;
                    cell.lblTitle.text = self.homeData.perfume.name;
                    cell.lblBtnTitle.text = "View shopping History";
                    cell.imgTitleBottom.isHidden = true;
                    cell.collectionViewCatagories.isHidden = false;
                    cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section + 1);
                    cell.btnSeeMore.tag = indexPath.section + 1;
                }
                if indexPath.section == 5
                {
                    cell.vwHeader.isHidden = false;
                    
                    cell.lblTitle.text = String(htmlEncodedString:  self.homeData.category[indexPath.row].name);
                    cell.imgTitleBottom.isHidden = true;
                    cell.lblBtnTitle.text = "See More";
                    cell.collectionViewCatagories.isHidden = false;
                    
                    if self.homeData.category[indexPath.row].cat_id == "632"
                    {
                        cell.vwBotton.isHidden = true;
                    }
                    else
                    {
                        cell.vwBotton.isHidden = false;
                    }
                    
                    cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: ((indexPath.section * 1000) + indexPath.row) + 1);
                    cell.btnSeeMore.tag = ((indexPath.section * 1000) + indexPath.row) + 1;
                    cell.collectionViewCatagories.reloadData()
                    
                }
                
                cell.btnSeeMore.addTarget(self, action: #selector(self.btnSeeMoreAction(_:)), for: .touchUpInside);
            }
            return cell
        }
    }
    
    //MARK:- COLLECTION VIEW DELEGATE AND DATA SOURXE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == 1 && !CommonFunctions.isNull(s: self.homeData.banner)
        {
            return self.homeData.banner.images.count;
        }
        else if collectionView.tag == 2
        {
            return 1;
        }
        else if collectionView.tag == 3 && !CommonFunctions.isNull(s: self.homeData.weekly)
        {
            return self.homeData.weekly.weekly_products.count;
        }
        else if collectionView.tag == 4 && !CommonFunctions.isNull(s: self.homeData.monthly)
        {
            return self.homeData.monthly.monthly_products.count;
        }
        else if collectionView.tag == 5
        {
            return self.homeData.perfume.perfume_products.count;
        }
        else if collectionView.tag > 5000
        {
            return self.homeData.category[(collectionView.tag - 1) % 5000].subCatData.count
        }
        else
        {
            return 0;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView.tag == 1 && !CommonFunctions.isNull(s: self.homeData.banner)// Banner
        {
            collectionView.isPagingEnabled = true;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            
            let img = self.homeData.banner.images[indexPath.item];
            let url = img.img;
            let fileUrl = (url)?.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgBanner.sd_setImage(with: URL(string: fileUrl!), placeholderImage: UIImage(named: placeholderImage), options: [.highPriority], completed: nil);
            cell.imgBanner.backgroundColor = .clear;
            cell.imgBanner.contentMode = .scaleToFill;
            
            return cell;
        }
        else if collectionView.tag == 2 // static Image
        {
            collectionView.isPagingEnabled = false;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
            
            cell.backgroundColor = UIColor.clear;
            cell.contentView.backgroundColor = UIColor.clear;
            
            cell.imgBanner.image = UIImage(named: "ic_home_image");
            cell.imgBanner.backgroundColor = UIColor.white;
            cell.imgBanner.contentMode = .scaleAspectFill;
            
            return cell;
        }
        else if collectionView.tag == 3 && !CommonFunctions.isNull(s: self.homeData.weekly)// weekly
        {
            collectionView.isPagingEnabled = false;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            let weekly = self.homeData.weekly!.weekly_products[indexPath.item];
            
            let url = weekly.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(weekly.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(weekly.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@ off)", weekly.sale_percentage);
            
            if weekly.dela_end_date > 0
            {
                cell.lblDealEndTime.isHidden = false;
                let server_time = ConstantFunctions.getServerTime();

                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: weekly.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            else
            {
                cell.lblDealEndTime.isHidden = true;
            }
            
            cell.lblProductName.text = String(htmlEncodedString: weekly.item_name);
            
            return cell
        }
        else if collectionView.tag == 4 && !CommonFunctions.isNull(s: self.homeData.monthly)// monthly
        {
            collectionView.isPagingEnabled = false;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            let monthly = self.homeData.monthly!.monthly_products[indexPath.item];
            
            let url = monthly.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(monthly.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(monthly.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@ off)", monthly.sale_percentage);
            
            if monthly.dela_end_date > 0
            {
                cell.lblDealEndTime.isHidden = false;
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: monthly.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            else
            {
                cell.lblDealEndTime.isHidden = true;
            }
            
            cell.lblProductName.text = String(htmlEncodedString: monthly.item_name);
            
            return cell
        }
        else if collectionView.tag == 5 && !CommonFunctions.isNull(s: self.homeData.perfume) // perfumes offers
        {
            collectionView.isPagingEnabled = false;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyOfferCollectionViewCell", for: indexPath) as! WeeklyOfferCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            let perfume = self.homeData.perfume!.perfume_products[indexPath.item];
            
            let url = perfume.images[0];
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            let sale_price = Double(perfume.sale_price)!
            cell.lblDealPrice.text = String(format: "%.2f%@", sale_price, _currency_symbol);
            let price = Double(perfume.price)!
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format: "%.2f %@", price, _currency_symbol));
            
            attributeString.addAttribute( NSAttributedStringKey.strikethroughStyle , value: 1, range: NSMakeRange(0, attributeString.length))
            
            cell.lblLastPrice.attributedText = attributeString;
            
            cell.lblDisCountPercent.text = String(format: "(%@ off)", perfume.sale_percentage);
            
            if perfume.dela_end_date > 0
            {
                cell.lblDealEndTime.isHidden = false;
                let server_time = ConstantFunctions.getServerTime();
                
                let temp = Calendar.current.date(byAdding: .second, value: Int(time), to: server_time!)
                
                let another_date = ConstantFunctions.getDate(unixTimestamp: perfume.dela_end_date);
                
                let remaning_time = ConstantFunctions.getTimeDifference(dateA: another_date!, dateB: temp!)
                
                cell.lblDealEndTime.text = "Ends in \(remaning_time)";
            }
            else
            {
                cell.lblDealEndTime.isHidden = true;
            }
            
            cell.lblProductName.text = String(htmlEncodedString: perfume.item_name);
            
            return cell
        }
        else if collectionView.tag > 5000 && !CommonFunctions.isNull(s: self.homeData.category) //perfume category
        {
            collectionView.isPagingEnabled = false;
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatagoryCollectionViewCell", for: indexPath) as! ProductCatagoryCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
            
            let perfume = self.homeData.category[(collectionView.tag - 1) % 5000].subCatData[indexPath.item];
            
            let url = perfume.banner;
            let fileUrl = (url).replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil);
            
            cell.imgProduct.sd_setImage(with: URL(string: fileUrl), placeholderImage: UIImage(named: placeholderImage), options: [.scaleDownLargeImages], completed: nil);
            
            cell.imgProduct.backgroundColor = UIColor.black.withAlphaComponent(0.05);
            
            cell.lblProduct.text = String(htmlEncodedString: perfume.name);
            
            return cell;
        }
        else
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell;
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
            
            return cell;
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag == 1 && !CommonFunctions.isNull(s: self.homeData.banner)// Banner
        {
            
        }
        if collectionView.tag == 3 && !CommonFunctions.isNull(s: self.homeData.weekly)// weekly
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let weekly = self.homeData.weekly!.weekly_products[indexPath.item];
            vc.product_id = weekly.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        if collectionView.tag == 4 && !CommonFunctions.isNull(s: self.homeData.monthly)// monthly
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let weekly = self.homeData.monthly!.monthly_products[indexPath.item];
            vc.product_id = weekly.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        if collectionView.tag == 5 && !CommonFunctions.isNull(s: self.homeData.perfume) // perfume
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController;
            let weekly = self.homeData.perfume!.perfume_products[indexPath.item];
            vc.product_id = weekly.item_id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        if collectionView.tag > 5000 && !CommonFunctions.isNull(s: self.homeData.category) //perfume category
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
            let perfume = self.homeData.category[(collectionView.tag - 1) % 5000].subCatData[indexPath.item];
            vc.sub_cat_id = perfume.id;
            self.navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView.tag == 1
        {
            return CGSize(width: self.view.frame.size.width, height: (self.tableViewHome.frame.size.width / 1.5));
        }
        else if collectionView.tag == 2
        {
            return CGSize(width: self.view.frame.size.width, height: 85);
        }
        else if collectionView.tag == 3
        {
            return CGSize(width: (collectionView.frame.width) / 3, height: 200);
        }
        else if collectionView.tag == 4
        {
            return CGSize(width: (collectionView.frame.width) / 3, height: 200);
        }
        else if collectionView.tag == 5
        {
            return CGSize(width: (collectionView.frame.width) / 3, height: 200);
        }
        else if collectionView.tag > 5000
        {
            if self.homeData.category[(collectionView.tag - 1) % 5000].cat_id == "909" || self.homeData.category[(collectionView.tag - 1) % 5000].cat_id == "898"
            {
                 return CGSize(width: (collectionView.frame.width - 20) / 2, height: (collectionView.frame.width - 30) / 2);
            }
            
            else
            {
                return CGSize(width: (collectionView.frame.width - 50) / 3, height: (collectionView.frame.width - 10) / 3);
            }
        }
        else
        {
            return CGSize(width: (collectionView.frame.width - 20) / 3, height: 135);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView.tag == 1 || collectionView.tag == 2 || collectionView.tag == 3 || collectionView.tag == 4 || collectionView.tag == 5
        {
            return UIEdgeInsets.zero;
        }
        else
        {
            return UIEdgeInsetsMake(5, 7, 5, 7);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView.tag == 2 || collectionView.tag == 3 || collectionView.tag == 4 || collectionView.tag == 5
        {
            return 0
        }
        else if collectionView.tag > 5000
        {
            if self.homeData.category[((collectionView.tag - 1) % 5000)].cat_id == "909" || self.homeData.category[((collectionView.tag - 1) % 5000)].cat_id == "898"
            {
                return 0
            }
            else
            {
                return 7
            }
        }
        else
        {
            return 10;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView.tag > 5000
        {
            if self.homeData.category[((collectionView.tag - 1) % 5000)].cat_id == "909" || self.homeData.category[((collectionView.tag - 1) % 5000)].cat_id == "898"
            {
                return 0
            }
            else
            {
                return 7
            }
        }
        else
        {
            return 0
        }
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint
    {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant);
    }
}

