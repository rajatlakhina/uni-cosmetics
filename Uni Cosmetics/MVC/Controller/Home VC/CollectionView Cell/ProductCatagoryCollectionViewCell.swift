//
//  ProductCatagoryCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 29/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductCatagoryCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProduct: LabelProperties!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
