//
//  RelatedItemCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 27/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class RelatedItemCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblCost: LabelProperties!
    
    @IBOutlet weak var lblProduct: LabelProperties!
    @IBOutlet weak var imgProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
