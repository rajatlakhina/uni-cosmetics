//
//  PickUpCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PickUpCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: LabelProperties!
    
    @IBOutlet weak var lblCost: LabelProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
