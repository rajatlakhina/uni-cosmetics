//
//  BannerCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 30/10/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgBanner: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
