//
//  ItemCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 02/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProduct: LabelProperties!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
