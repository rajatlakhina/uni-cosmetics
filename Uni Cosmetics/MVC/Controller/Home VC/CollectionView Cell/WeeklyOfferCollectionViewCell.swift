//
//  WeeklyOfferCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 30/10/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class WeeklyOfferCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: LabelProperties!
    
    @IBOutlet weak var lblDealPrice: LabelProperties!
    
    @IBOutlet weak var lblLastPrice: LabelProperties!
    
    @IBOutlet weak var lblDisCountPercent: LabelProperties!
    
    @IBOutlet weak var lblDealEndTime: LabelProperties!
    
    private var timer: Timer?
    private var timeCounter: TimeInterval = 0
    
    var expiryTimeInterval: TimeInterval? {
        didSet {
            startTimer()
        }
    }
    
    private func startTimer() {
        if let interval = expiryTimeInterval {
            timeCounter = interval
            
           timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(onComplete), userInfo: nil, repeats: true)
//            if #available(iOS 10.0, *) {
//                timer = Timer(timeInterval: 1.0,
//                              repeats: true,
//                              block: { [weak self] _ in
//                                guard let strongSelf = self else {
//                                    return
//                                }
//                                strongSelf.onComplete()
//                })
//
//            } else {
//                timer = Timer(timeInterval: 1.0,
//                              target: self,
//                              selector: #selector(onComplete),
//                              userInfo: nil,
//                              repeats: true)
//            }
        }
    }
    
    @objc func onComplete() {
        guard timeCounter >= 0 else {
            timer?.invalidate()
            timer = nil
            return
        }
        
        
        self.lblDealEndTime.text = "Ends in \(timeCounter.stringTime)"
        timeCounter -= 1
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        timer?.invalidate()
        timer = nil
    }
}
