//
//  ProductCollectionViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 02/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var vwContainer: UIViewProperties!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProduct: LabelProperties!
    
    @IBOutlet weak var heightImgProduct: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
