//
//  HomeTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 26/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewCatagories: UICollectionView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var imgTitleBottom: UIImageView!
    
    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var imgBottom: UIImageView!
    @IBOutlet weak var vwBotton: UIView!
    @IBOutlet weak var lblBtnTitle: LabelProperties!
    @IBOutlet weak var containerStackView: UIStackView!
    
    @IBOutlet weak var btnSeeMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionViewCatagories.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "WeeklyOfferCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WeeklyOfferCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell");
        self.collectionViewCatagories.register(UINib(nibName: "RelatedItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RelatedItemCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "PickUpCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PickUpCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "ProductCatagoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCatagoryCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "DealsHeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DealsHeaderCollectionViewCell");
        
        self.collectionViewCatagories.register(UINib(nibName: "HomeFooterCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "HomeFooterCollectionViewCell");
}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate & UICollectionViewDelegateFlowLayout>
        (dataSourceDelegate: D, forSection section: Int) {
        
        self.collectionViewCatagories.delegate = dataSourceDelegate
        self.collectionViewCatagories.dataSource = dataSourceDelegate
        self.collectionViewCatagories.isScrollEnabled = false;
        self.collectionViewCatagories.tag = section
        self.collectionViewCatagories.reloadData()
    }
}
