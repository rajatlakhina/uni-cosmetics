//
//  PickUpTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 02/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class PickUpTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProbuctName: LabelProperties!
    @IBOutlet weak var lblProductCost: LabelProperties!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
