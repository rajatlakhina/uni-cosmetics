//
//  ProductHeaderTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 03/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class ProductHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: LabelProperties!
    @IBOutlet weak var centralhorizontallyCostraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
