//
//  HomeSectionFooterTableViewCell.swift
//  Uni Cosmetics
//
//  Created by Rajat on 27/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import UIKit

class HomeSectionFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBtnTitle: LabelProperties!
    
    @IBOutlet weak var btnMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
