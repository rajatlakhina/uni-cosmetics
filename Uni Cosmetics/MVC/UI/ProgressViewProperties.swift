//
//  ProgressViewProperties.swift
//  Bubble Car Wash
//
//  Created by Rajat on 22/10/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class ProgressViewProperties: UIProgressView
{

    @IBInspectable var rotation: Int
        {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }

}
