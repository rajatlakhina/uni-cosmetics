//
//  UIApplicationProperties.swift
//  Bubble Car Wash
//
//  Created by Orem on 02/10/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication
{
    func topMostViewController() -> UIViewController?
    {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
