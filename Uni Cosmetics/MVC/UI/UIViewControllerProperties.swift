//
//  UIViewControllerProperties.swift
//  Bubble Car Wash
//
//  Created by Orem on 02/10/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    func topMostViewController() -> UIViewController
    {
        if self.presentedViewController == nil
        {
            return self
        }
        if let navigation = self as? UINavigationController
        {
            return navigation.visibleViewController!.topMostViewController()
        }
        
        if let tab = self as? UITabBarController
        {
            if let selectedTab = tab.selectedViewController
            {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        
        if let navigation = self.presentedViewController as? UINavigationController
        {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController
        {
            if let selectedTab = tab.selectedViewController
            {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}
