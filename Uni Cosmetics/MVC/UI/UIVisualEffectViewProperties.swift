//
//  UIVisualEffectViewProperties.swift
//  Bubble Car Wash
//
//  Created by Orem on 12/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class UIVisualEffectViewProperties: UIVisualEffectView
{    
    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable var cornerRadius: Double{
        get
        {
            return Double(self.layer.cornerRadius);
        }
        set
        {
            self.layer.cornerRadius = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderWidth: Double {
        get
        {
            return Double(self.layer.borderWidth);
        }
        set
        {
            self.layer.borderWidth = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.borderColor!);
        }
        set {
            self.layer.borderColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.shadowColor!);
        }
        set
        {
            self.layer.shadowColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowRadius: CGFloat {
        get
        {
            return layer.shadowRadius;
        }
        set
        {
            layer.shadowRadius = newValue
        }
    };
    
    @IBInspectable var shadowOffset: CGSize {
        get
        {
            return layer.shadowOffset;
        }
        set
        {
            layer.shadowOffset = newValue;
        }
    };
    
    @IBInspectable var shadowOpacity: Float {
        get
        {
            return self.layer.shadowOpacity;
        }
        set
        {
            self.layer.shadowOpacity = newValue;
        }
    };
}
