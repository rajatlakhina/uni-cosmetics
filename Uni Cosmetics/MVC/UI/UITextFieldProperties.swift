//
//  UITextFieldProperties.swift
//  Crap Chronicles
//
//  Created by Orem on 04/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class UITextFieldProperties: UITextField
{
    private var placeholderText1: String! = nil;
    private var __maxLengths = [UITextField: Int]()
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        layoutIfNeeded()
        configurefont()
        
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        layoutIfNeeded()
        configurefont()
        
    }
    
    func configurefont()
    {
        self.text = self.text?.localized;
        var newFontSize: CGFloat
        if IS_iPHONE_5
        {
            newFontSize = (font?.pointSize)! * 1.1
        }
        else if IS_STANDARD_IPHONE_6
        {
            newFontSize = (font?.pointSize)! * 1.2
        }
        else if (Is_iPad_9 || is_iPad_10 || is_iPad_12)
        {
            newFontSize = (font?.pointSize)! * 1.5
        }
        else
        {
            newFontSize = (font?.pointSize)! * 1.3
        }
        
        font = UIFont(name: (font?.fontName)!, size: newFontSize)
    }
    
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    @IBInspectable var cornerRadius: Double{
        get
        {
            return Double(self.layer.cornerRadius);
        }
        set
        {
            self.layer.cornerRadius = CGFloat(newValue);
        }
    };
    
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 550 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
    
    @IBInspectable var borderWidth: Double {
        get
        {
            return Double(self.layer.borderWidth);
        }
        set
        {
            self.layer.borderWidth = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.borderColor!);
        }
        set {
            self.layer.borderColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.shadowColor!);
        }
        set
        {
            self.layer.shadowColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowRadius: CGFloat {
        get
        {
            return layer.shadowRadius;
        }
        set
        {
            layer.shadowRadius = newValue
        }
    };
    
    @IBInspectable var shadowOffset: CGSize {
        get
        {
            return layer.shadowOffset;
        }
        set
        {
            layer.shadowOffset = newValue;
        }
    };
    
    @IBInspectable var shadowOpacity: Float {
        get
        {
            return self.layer.shadowOpacity;
        }
        set
        {
            self.layer.shadowOpacity = newValue;
        }
    };
}
extension UITextFieldProperties
{
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }
    
    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right }
    }
    
    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }
    
    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
}
