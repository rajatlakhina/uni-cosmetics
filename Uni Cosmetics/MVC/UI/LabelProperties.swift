//
//  LabelProperties.swift
//  Crap Chronicles
//
//  Created by Orem on 04/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class LabelProperties: UILabel
{
    func lblRotate(){
        var rotation: Int {
            get {
                return 0
            } set {
                let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
                self.transform = CGAffineTransform(rotationAngle: radians)
            }
        }
    }
    
    public override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        layoutIfNeeded();
        self.configurefont();
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        layoutIfNeeded()
        self.configurefont();
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text.localized
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func configurefont()
    {
        var newFontSize: CGFloat
        if IS_iPHONE_5
        {
            newFontSize = font.pointSize * 1.1;
        }
        else if IS_STANDARD_IPHONE_6
        {
            newFontSize = (font.pointSize) * 1.2;
        }
        else if (Is_iPad_9 || is_iPad_10 || is_iPad_12)
        {
            newFontSize = (font.pointSize) * 1.5;
        }
        else
        {
            newFontSize = (font.pointSize) * 1.3;
        }
        
        font = UIFont(name: (font.fontName), size: newFontSize)
    }
    
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                                          left: -textInsets.left,
                                          bottom: -textInsets.bottom,
                                          right: -textInsets.right)
        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
    }
    
     @IBInspectable var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
    
    @IBInspectable var cornerRadius: Double{
        get
        {
            return Double(self.layer.cornerRadius);
        }
        set
        {
            self.layer.cornerRadius = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderWidth: Double {
        get
        {
            return Double(self.layer.borderWidth);
        }
        set
        {
            self.layer.borderWidth = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.borderColor!);
        }
        set {
            self.layer.borderColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable override var shadowColor: UIColor? {
        get
        {
            if CommonFunctions.isNull(s: self.layer.shadowColor)
            {
                return UIColor(cgColor: UIColor.clear.cgColor);
            }
            else
            {
                return UIColor(cgColor: self.layer.shadowColor!);
            }
        }
        set
        {
            self.layer.shadowColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowRadius: CGFloat {
        get
        {
            return layer.shadowRadius;
        }
        set
        {
            layer.shadowRadius = newValue;
        }
    };
    
    @IBInspectable override var shadowOffset: CGSize {
        get
        {
            if CommonFunctions.isNull(s: layer.shadowOffset)
            {
                return CGSize(width: 0.0, height: 0.0);
            }
            else
            {
                return layer.shadowOffset;
            }
        }
        set
        {
            layer.shadowOffset = newValue;
        }
    };
    
    @IBInspectable var shadowOpacity: Float {
        get
        {
            return self.layer.shadowOpacity;
        }
        set
        {
            self.layer.shadowOpacity = newValue;
        }
    };
}

extension LabelProperties
{
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }
    
    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right }
    }
    
    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }
    
    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
}
