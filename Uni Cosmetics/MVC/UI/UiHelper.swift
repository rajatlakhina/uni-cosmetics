//
//  UiHelper.swift
//  Mart
//
//  Created by Rahul Singla on 26/03/18.
//  Copyright © 2018 Imbibe User. All rights reserved.
//

import Foundation
import UIKit

class UiHelper
{
    static var nibCache = [String : UINib]();
    
    static func loadNib(name: String) -> UINib
    {
        if let nib = self.nibCache[name]
        {
            return (nib);
        }
        
        let nib = UINib(nibName: name, bundle: Bundle.main);
        self.nibCache[name] = nib;
        
        return (nib);
    }
    
    static func getNibInstance(name: String) -> [Any]
    {
        let nib = UiHelper.loadNib(name: name);
        //let nib2 = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?[6] as! DealsView
        
        return (nib.instantiate(withOwner: nil, options: nil));
    }
    
    static func getReusableViewsNibInstance() -> [Any]
    {
        return (self.getNibInstance(name: "ErrorLoader"));
    }
    
    static func getReusableViewsInstance() -> [Any]
    {
        return (self.getNibInstance(name: "ReuseableViews"));
    }
    
    static func getNetworkViewsNibInstance() -> [Any]
    {
        return (self.getNibInstance(name: "AddEditItem"));
    }
    
    static func getResolvedUiController(uiViewController: UIViewController) -> UIViewController
    {
        var currentController = uiViewController;
//        if (currentController is MFSideMenuContainerViewController)
//        {
//            let mfSideMenuContainerViewController = currentController as! MFSideMenuContainerViewController;
//            currentController = mfSideMenuContainerViewController.centerViewController as! UIViewController;
//        }
//        if (currentController is UINavigationController)
//        {
//            let uiNavigationController = currentController as! UINavigationController;
//            currentController = uiNavigationController.visibleViewController!;
//        }
        
        return (currentController);
    }
    
    static func getResolvedUiController(navigationController: UINavigationController) -> UIViewController
    {
        return (self.getResolvedUiController(uiViewController: navigationController.visibleViewController!))
    }

    static func popCurrentViewControllerAndPush(navigationController: UINavigationController, vc: UIViewController, animated: Bool = true)
    {
        var vcArray = navigationController.viewControllers;
        vcArray.removeLast();
        vcArray.append(vc);
        navigationController.setViewControllers(vcArray, animated: animated);
    }
    
//    static func getOrderStatusColor(status: OrderStatus) -> UIColor
//    {
//        switch(status)
//        {
//        case .Submitted:
//            return UIColor(red: 204.0/255.0, green: 153.0/255.0, blue: 51.0/255.0, alpha: 1.0);
//
//        case .Confirmed:
//            return UIColor(red: 119/255, green: 166/255, blue: 24/255, alpha: 1);
//
//        case .Completed:
//            return UIColor(red: 0/255, green: 128/255, blue: 0/255, alpha: 1);
//
//        default:
//            return UIColor(red: 204/255, green: 102/255, blue: 102/255, alpha: 1.0);
//        }
//    }
//
//    static func getOrderStatusText(status: OrderStatus) -> String
//    {
//        switch(status)
//        {
//        case .Submitted:
//            return "Pending";
//
//        case .Confirmed:
//            return "Confirmed";
//
//        case .Completed:
//            return "Completed";
//
//        default:
//            return "Cancelled";
//        }
//    }
//
//    static func getOrderStatusImage(status: OrderStatus) -> String
//    {
//        switch(status)
//        {
//        case .Submitted:
//            return "orderPending";
//
//        case .Confirmed:
//            return "orderConfirm";
//
//        case .Completed:
//            return "orderConfirm";
//
//        default:
//            return "orderCancel";
//        }
//    }
}
