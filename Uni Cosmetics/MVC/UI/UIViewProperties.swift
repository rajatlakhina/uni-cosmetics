//
//  UIViewProperties.swift
//  Crap Chronicles
//
//  Created by Orem on 03/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

@IBDesignable
class UIViewProperties: UIView
{
    @IBInspectable var startColor:   UIColor = .clear { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .clear { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    public override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        layoutIfNeeded();
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        layoutIfNeeded();
    }
    
    @IBInspectable var cornerRadius: Double{
        get
        {
            return Double(self.layer.cornerRadius);
        }
        set
        {
            self.layer.cornerRadius = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderWidth: Double {
        get
        {
            return Double(self.layer.borderWidth);
        }
        set
        {
            self.layer.borderWidth = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.borderColor!);
        }
        set {
            self.layer.borderColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.shadowColor!);
        }
        set
        {
            self.layer.shadowColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowRadius: CGFloat {
        get
        {
            return layer.shadowRadius;
        }
        set
        {
            layer.shadowRadius = newValue
        }
    };
    
    @IBInspectable var shadowOffset: CGSize {
        get
        {
            return layer.shadowOffset;
        }
        set
        {
            layer.shadowOffset = newValue;
        }
    };
    
    @IBInspectable var shadowOpacity: Float {
        get
        {
            return self.layer.shadowOpacity;
        }
        set
        {
            self.layer.shadowOpacity = newValue;
        }
    };
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints()
    {
        if horizontalMode
        {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        }
        else
        {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations()
    {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors()
    {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}
