//
//  UITestViewProperties.swift
//  Crap Chronicles
//
//  Created by Orem on 04/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import UIKit

class UITextViewProperties: UITextView
{
    private var placeholderText1: String! = nil;
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        layoutIfNeeded()
        configurefont()
        
    }
    
    func configurefont()
    {
        self.text = self.text?.localized;
        var newFontSize: CGFloat
        if IS_iPHONE_5
        {
            newFontSize = (font?.pointSize)! * 1.1
        }
        else if IS_STANDARD_IPHONE_6
        {
            newFontSize = (font?.pointSize)! * 1.2
        }
        else if (Is_iPad_9 || is_iPad_10 || is_iPad_12)
        {
            newFontSize = (font?.pointSize)! * 1.5
        }
        else
        {
            newFontSize = (font?.pointSize)! * 1.3
        }
        
        font = UIFont(name: (font?.fontName)!, size: newFontSize)
    }
    
    @IBInspectable var cornerRadius: Double{
        get
        {
            return Double(self.layer.cornerRadius);
        }
        set
        {
            self.layer.cornerRadius = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderWidth: Double {
        get
        {
            return Double(self.layer.borderWidth);
        }
        set
        {
            self.layer.borderWidth = CGFloat(newValue);
        }
    };
    
    @IBInspectable var borderColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.borderColor!);
        }
        set {
            self.layer.borderColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowColor: UIColor? {
        get
        {
            return UIColor(cgColor: self.layer.shadowColor!);
        }
        set
        {
            self.layer.shadowColor = newValue?.cgColor;
        }
    };
    
    @IBInspectable var shadowRadius: CGFloat {
        get
        {
            return layer.shadowRadius;
        }
        set
        {
            layer.shadowRadius = newValue
        }
    };
    
    @IBInspectable var shadowOffset: CGSize {
        get
        {
            return layer.shadowOffset;
        }
        set
        {
            layer.shadowOffset = newValue;
        }
    };
    
    @IBInspectable var shadowOpacity: Float {
        get
        {
            return self.layer.shadowOpacity;
        }
        set
        {
            self.layer.shadowOpacity = newValue;
        }
    };
    
    func textFieldDidBeginEditing()
    {
        if(self.text == self.placeholderText1)
        {
            self.text = "";
            self.textColor = UIColor.lightGray;
        }
    }
    
    func textFieldDidChange()
    {
        self.syncwithPlaceholderText(autoAddPlaceHolderText: false);
    }
    
    func textFieldDidEndEditing()
    {
        self.syncwithPlaceholderText();
    }
    
    func syncwithPlaceholderText(autoAddPlaceHolderText: Bool = true)
    {
        if(self.text == self.placeholderText1)
        {
            self.textColor = UIColor.gray;
        }
        else if((self.text?.isEmpty)! && autoAddPlaceHolderText)
        {
            self.text = self.placeholderText1;
            self.textColor = UIColor.gray;
        }
        else
        {
            self.textColor = UIColor.black;
        }
    }
    
    func getEffectiveTextIgnoringPlaceholder(_ emptyValue: String! = nil) -> String!
    {
        if(self.text == self.placeholderText1)
        {
            return ("");
        }
        else
        {
            return (self.text);
        }
    }
    
    func setPlaceholderText(_ text: String!)
    {
        self.placeholderText1 = text;
        
        self.syncwithPlaceholderText();
    }
}
