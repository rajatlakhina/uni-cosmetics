//
//  Address.swift
//  Uni Cosmetics
//
//  Created by Rajat on 18/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class Address:ApiObject
{
    var id = String();
    var user_id = String();
    var first_name = String();
    var last_name = String();
    var company = String();
    var address_1 = String();
    var address_2 = String();
    var city = String();
    var postcode = String();
    var state = String();
    var country = String();
    var email = String();
    var type = String();
    var phone = String();
}
