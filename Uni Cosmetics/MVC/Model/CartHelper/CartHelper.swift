//
//  CartHelper.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation

class CartHelper
{
    private static var currentUser: User? = nil;
    
    public static func setCurrentUser(user: User)
    {
        self.currentUser = user;
        UserDefaults.standard.set(user.toJsonString(), forKey: "user");
    }
    
    public static func getCurrentUser() -> User
    {
        var user: User? = self.currentUser;
        
        if(user == nil)
        {
            let currentUserStr = UserDefaults.standard.object(forKey: "user") as! String?
            if(currentUserStr == nil)
            {
                user = User();
            }
            else
            {
                user = User(json: currentUserStr);
            }
        }
        
        self.currentUser = user;
        return(user!);
    }
}
