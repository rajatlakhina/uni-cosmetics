//
//  HomeData.swift
//  Uni Cosmetics
//
//  Created by Rajat on 20/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import Foundation
import EVReflection

class HomeData: ApiObject
{
    var banner:Banner! = nil;
    var monthly:MonthlyOffers! = nil;
    var weekly:WeeklyOffers! = nil;
    var perfume:PerfumeOffers! = nil;
    var perfumes: Catagory! = nil;
    var hair:Catagory! = nil;
    var makeup:Catagory! = nil;
    var coloring: Catagory! = nil;
    var equipment:Catagory! = nil;
    var nails:Catagory! = nil;
    var body:Catagory! = nil;
}
