//
//  Home.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class Home: ApiObject
{
    var banner:Banner! = nil;
    var monthly:MonthlyOffers! = nil;
    var weekly:WeeklyOffers! = nil;
    var perfume:PerfumeOffers! = nil;
    var category: [Catagory]! = nil;
}
