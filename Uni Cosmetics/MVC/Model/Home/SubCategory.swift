//
//  SubCategory.swift
//  Uni Cosmetics
//
//  Created by Rajat on 20/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class SubCategory:ApiObject
{
    var id: String! = nil;
    var name: String! = nil;
    var banner: String! = nil;
}
