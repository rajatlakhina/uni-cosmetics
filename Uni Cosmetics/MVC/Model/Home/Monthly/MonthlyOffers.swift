//
//  MonthlyOffers.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class MonthlyOffers:ApiObject
{
    var id = String();
    var name = String();
    var banner = String()
    var monthly_products = [Product]();
}
