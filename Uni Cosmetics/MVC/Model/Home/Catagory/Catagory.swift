//
//  File.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class Catagory:ApiObject
{
    var cat_id: String! = nil;
    var name: String! = nil;
    var banner: String! = nil;
    var subCatData : [SubCatagory]! = nil;
}
