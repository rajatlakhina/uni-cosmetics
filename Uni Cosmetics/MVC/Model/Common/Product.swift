//
//  Product.swift
//  Uni Cosmetics
//
//  Created by Rajat on 19/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class Product:ApiObject
{
    var item_id = String();
    var item_name = String();
    var _description = String();
    var price = String();
    var sale_price = String();
    var sku = String();
    var quantity = String();
    var status = String();
    var images = [String]();
    var dela_end_date = TimeInterval();
    var sale_percentage = String();
    var catname = String();
    
    override public func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)]
    {
        return [(keyInObject: "_description",keyInResource: "description")];
    }
}
