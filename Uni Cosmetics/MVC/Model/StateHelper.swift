//
//  StateHelper.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation

class StateHelper
{
    public static func getDeviceToken () -> String!
    {
        if let callback_token = UserDefaults.standard.string(forKey:"device_token")
        {
            return (callback_token);
        }
        else
        {
            return ("");
        }
    }
    
    public static func setDeviceToken (value: String)
    {
        UserDefaults.standard.set(value, forKey: "device_token");
    }
    
    public static func getIsUserIn () -> Bool!
    {
        let callback_token = UserDefaults.standard.bool(forKey:"isUserLoggedIn")
        if CommonFunctions.isNull(s: callback_token)
        {
            return false;
        }
        else
        {
            return callback_token;
        }
    }
    
    public static func setIsUserIn (value: Bool)
    {
        UserDefaults.standard.set(value, forKey: "isUserLoggedIn")
    }
}
