//
//  Brand.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class Brand:ApiObject
{
    var id:String! = nil;
    var name:String! = nil;
    var banner:String! = nil;
}
