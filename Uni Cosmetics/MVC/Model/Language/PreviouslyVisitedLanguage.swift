//
//  PreviouslyVisitedLanguage.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/11/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class PreviouslyVisitedLanguage:ApiObject
{
    var language_id:Int = 0;
    var language_image = String();
    var language_name = String();
    var language_website = String();
}
