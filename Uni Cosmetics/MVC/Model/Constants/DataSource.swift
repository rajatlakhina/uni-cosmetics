//
//  DataSource.swift
//  Crap Chronicles
//
//  Created by Orem on 04/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import Foundation
import UIKit

//MARK:- ------Root variables ----------//
let appDelegate = UIApplication.shared.delegate as! AppDelegate

//MARK:- CURRENCY SYMBOL
let locale = Locale(identifier: "fr_FR");
//let _currency_symbol = locale.currencySymbol ?? "€";
let _currency_symbol = locale.currencySymbol ?? "€"
let placeholderImage = "ic_place_holder"
//let _currency_symbol = "€";
//"SAR ";

//MARK:- INFORMATION SCREENS
var _info_screen:Int = -1

//MARK:- DEVICE TOKEN
//var device_tokken = String();

//MARK:- User Details

var otp_ = "";

let _lang = ["en", "gr", "al"]

var _lat = String();
var _lng = String();

var refreshScreenData:Bool = false;

//MARK:- Tab switch

var isOrderRefresh:Bool = false;
var orderType:String = "";

//MARK:- SERVER TIME
var server_time = String();
var _serverTimeInterval = TimeInterval();

//MARK:- ------iPhone Size Defined------//
let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == .phone)
let IS_STANDARD_IPHONE_X = (IS_IPHONE && UIScreen.main.bounds.size.height == 812.0)
let IS_STANDARD_IPHONE_X_R = (IS_IPHONE && UIScreen.main.bounds.size.height == 896.0)
let IS_OS_8_OR_LATER = (Float(UIDevice.current.systemVersion) ?? 0.0 >= 8.0)
let IS_IPAD = (UI_USER_INTERFACE_IDIOM() == .pad)
let IS_iPHONE_5 = (IS_IPHONE && (UIScreen.main.bounds.size.height == 568.0) && ((IS_OS_8_OR_LATER && UIScreen.main.nativeScale == UIScreen.main.scale) || !IS_OS_8_OR_LATER))
let IS_STANDARD_IPHONE_6 = (IS_IPHONE && UIScreen.main.bounds.size.height == 667.0 && IS_OS_8_OR_LATER && UIScreen.main.nativeScale == UIScreen.main.scale)
let IS_ZOOMED_IPHONE_6 = (IS_IPHONE && UIScreen.main.bounds.size.height == 568.0 && IS_OS_8_OR_LATER && UIScreen.main.nativeScale > UIScreen.main.scale)
let IS_STANDARD_IPHONE_6_PLUS = (IS_IPHONE && UIScreen.main.bounds.size.height == 736.0)
let IS_ZOOMED_IPHONE_6_PLUS = (IS_IPHONE && UIScreen.main.bounds.size.height == 667.0 && IS_OS_8_OR_LATER && UIScreen.main.nativeScale < UIScreen.main.scale)
let Is_iPad_9 = (IS_IPAD && (UIScreen.main.bounds.size.height == 1024.0) && ((IS_OS_8_OR_LATER && UIScreen.main.nativeScale == UIScreen.main.scale) || !IS_OS_8_OR_LATER))
let is_iPad_12 = (IS_IPAD && (UIScreen.main.bounds.size.height == 1366.0) && ((IS_OS_8_OR_LATER && UIScreen.main.nativeScale == UIScreen.main.scale) || !IS_OS_8_OR_LATER))
let is_iPad_10 = (IS_IPAD && (UIScreen.main.bounds.size.height == 1112.0) && ((IS_OS_8_OR_LATER && UIScreen.main.nativeScale == UIScreen.main.scale) || !IS_OS_8_OR_LATER))

//MARK:- ------ Color code ---------//
let blueColorRGB:UIColor = UIColor(red: 0.0/255, green: 173.0/255, blue: 240.0/255, alpha: 1.0);
let greenColorRGB:UIColor = UIColor(red: 82.0/255, green: 170.0/255, blue: 94.0/255, alpha: 1.0);
let offWhiteRGB:UIColor = UIColor(red: 216.0/255, green: 216.0/255, blue: 216.0/255, alpha: 1.0);
let greyColorRGB:UIColor = UIColor(red: 205.0/255, green: 205.0/255, blue: 205.0/255, alpha: 1.0);
let splashBackGreen:UIColor = UIColor(red: 18.0/255, green: 45.0/255, blue: 48.0/255, alpha: 1.0);
let primaryPinkColor:UIColor = UIColor(red: 217.0/255, green: 17.0/255, blue: 140.0/255, alpha: 1.0); //hex code: D90B8C
let greyColor:UIColor = UIColor(red: 142.0/255, green: 142.0/255, blue: 146.0/255, alpha: 1.0); //hex code: 8E8E92
let primaryLightPinkColor:UIColor = UIColor(red: 245.0/255, green: 207.0/255, blue: 233.0/255, alpha: 1.0); //hex code: D90B8C

let fontPrimaryBlack:UIColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0); //hex code: 000000
let drawerBackgroundBlackColor:UIColor = UIColor(red: 63.0/255, green: 63.0/255, blue: 65.0/255, alpha: 1.0); //hex code: 000000

//MARK:- ------- Other Constants -------//
let _errorTitle = "Error !!"
let _AlertTitle = "Alert !!"
let _cancelBtnTitle = "Cancel"
let _loginBtnTitle = "LogIn"
let _okBtnTitle = "Ok"
let _yesButton = "Yes"
let _noButton = "No"
let _somethingWrongError = "Oops something went wrong."
let _connectionError = "No internet connection. Either turn on wifi or cellular data to proceed further."
