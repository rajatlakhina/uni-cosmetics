//
//  Constants.swift
//  FODDelivery
//
//  Created by Imbibe User on 22/08/17.
//  Copyright © 2017 Imbibe User. All rights reserved.
//

import Foundation

//MARK:- Google Api

let _googleAPIlink = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
let googleApiKey = "AIzaSyCxSx7RPXxUrGq73DVNTLpTTa-zFY1ygLs";
//MARK:- TIME ZONE

//let _riyadTimeZone = TimeZone(identifier: "Asia/Riyadh");

let _currentTimeZone = TimeZone(identifier: TimeZone.current.identifier);
//let _currentTimeZone = TimeZone(identifier: "Europe/Tirane");
//let _currentTimeZone = TimeZone(abbreviation: "UTC");
//let _currentTimeZone = TimeZone(identifier: "Asia/Riyadh");

// MARK:- ------ API Service URL ------//

//base urls
let _BaseURL = "https://www.unicosmetics.net/test/api/"

//let user_base_url = "http://18.224.14.248/img/users/"
//
//let cars_base_url = "http://18.224.14.248/img/cars/"
//
//let catagories_base_url = "http://18.224.14.248/img/categories/"

//api names

let _aboutUs = "about.php?"
//params = lang

let _home = "home.php?lang=en"

let _productDetails = "product_details.php?lang=en&product_id="

let _allDeals = "all_deals.php?lang=en"

let _getCategories = "category.php?lang=en"

let _getUpcomingDeals = "upcomming_deals.php?lang=en"

let _getCatagoryData = "category_data.php?lang=en&cat_id="

let _getProductList = "sub_category_data.php?lang=en&sub_cat_id="

let _getBrands = "brand.php?lang=en"

let _faqs = "faq.php?"
//params = lang

let _productShippingInformation = "payment_shipping.php?"
//params = lang

let _paymentmethodsInformation = "payment_method.php?"
//params = lang

let _customerProtection = "cust_protection.php?"
//params = lang

let _terms = "term.php?"
//params = lang

let _howToBuyInfomation = "how_to_buy.php?"
//params = lang

let _signUp = "register.php?"
//params = email, first_name, last_name, password

let _login = "login.php?"
//params = email, password, lang

let _forgotPassword = "forget.php?"
//params = email, lang

let _logout = "logout.php?"
//params = userid, lang

let _editProfile = "edit_profile.php?"
//params = userid, first_name, last_name

let _changePassword = "changepassword.php?"
//params = userid, oldpassword, newpassword

let _getAddress = "get_address.php?"
//params = userid, lang

let _saveAddress = "add_address.php?"
//params = userid, address
