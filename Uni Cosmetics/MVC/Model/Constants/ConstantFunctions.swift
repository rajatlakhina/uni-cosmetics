//
//  ConstantFunctions.swift
//  Uni Cosmetics
//
//  Created by Rajat on 24/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation

class ConstantFunctions
{
    public static func setServerTime(unixTimestamp: TimeInterval)
    {
        let date = Date(timeIntervalSince1970: unixTimestamp)
        let dateFormatter = DateFormatter()
        let timeZone = _currentTimeZone;
        dateFormatter.timeZone = timeZone;
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        server_time = dateFormatter.string(from: date);
    }
    
    public static func getDate(unixTimestamp: TimeInterval) -> Date!
    {
        let date = Date(timeIntervalSince1970: unixTimestamp)
        let dateFormatter = DateFormatter()
        let timeZone = _currentTimeZone;
        dateFormatter.timeZone = timeZone;
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date_to_get = dateFormatter.string(from: date);
        if !CommonFunctions.isEmpty(s: date_to_get, checkWhitespace: true)
        {
            return dateFormatter.date(from: date_to_get)!;
        }
        else
        {
            return nil
        }
    }
    
    public static func getServerTime() -> Date!
    {
        let dateFormatter = DateFormatter()
        let timeZone = _currentTimeZone;
        dateFormatter.timeZone = timeZone;
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        //server_time = dateFormatter.string(from: date);
        if !CommonFunctions.isEmpty(s: server_time, checkWhitespace: true)
        {
            return dateFormatter.date(from: server_time)!;
        }
        else
        {
            return nil
        }
    }
    
    static func getTimeDifference(dateA:Date, dateB:Date) -> String
    {
        let days = Calendar.current.dateComponents([.day,.hour,.minute,.second], from: dateB, to: dateA)
        
        print(dateA)
        print(dateB)
        print(Date())
        return (" \(days.day ?? 0)d \(days.hour ?? 0) h \(days.minute ?? 0) m \(days.second ?? 0) s")
    }
    
    static func getTimeDifference1(dateA:Date, dateB:Date) -> String
    {
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.maximumUnitCount = 1
        dateComponentsFormatter.unitsStyle = .full
        
        dateComponentsFormatter.allowedUnits = [.day,.hour,.minute,.second]
        //dateComponentsFormatter.allowedUnits = [.day]
        let days = dateComponentsFormatter.string(from: dateB, to: dateA);
//        let days = Calendar.current.dateComponents([.day,.hour,.minute,.second], from: dateB, to: dateA)
        return (days ?? "")
//
//        return days.second ?? 0
    }
}
