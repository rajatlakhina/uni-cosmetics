//
//  AboutUs.swift
//  Uni Cosmetics
//
//  Created by Rajat on 13/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class AboutUs:ApiObject
{
    var title = String()
    var content = String()
    var image = String()
    
    public func toDictionary(_ conversionOptions: ConversionOptions = .DefaultSerialize) -> NSDictionary
    {
        let dict = NSMutableDictionary();
        
        dict.setObject(self.title, forKey: "title" as NSCopying);
        dict.setObject(self.content, forKey: "title" as NSCopying);
        dict.setObject(self.image, forKey: "title" as NSCopying);
        
        return dict
    }
    
    public func toJsonString(_ conversionOptions: ConversionOptions = .DefaultSerialize, prettyPrinted: Bool = false) -> String
    {
        let dict = self.toDictionary();
        return (JsonSerialization.getJsonString(obj: dict));
    }
}
