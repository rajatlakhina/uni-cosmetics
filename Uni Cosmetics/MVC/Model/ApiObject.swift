//
//  ApiObject.swift
//  Mart
//
//  Created by Rahul Singla on 06/11/17.
//  Copyright © 2017 Imbibe User. All rights reserved.
//

import Foundation
import EVReflection

class ApiObject : EVObject
{
    public required init() {
        super.init();
    }
    
    public required convenience init(dictionary: NSDictionary, conversionOptions: ConversionOptions = .DefaultDeserialize) {
        self.init()
        ApiObject.deserializeDictionary(dictionary: dictionary, anyObject: self, conversionOptions: conversionOptions);
    }
    
    public required convenience init(json: String?, conversionOptions: ConversionOptions = .DefaultDeserialize) {
        self.init()
        ApiObject.deserializeDictionary(json: json, anyObject: self, conversionOptions: conversionOptions);
    }
    
    public static func deserializeDictionary(json: String?, anyObject: NSObject, conversionOptions: ConversionOptions = .DefaultDeserialize)
    {
        let dictionary = EVReflection.dictionaryFromJson(json)
        ApiObject.deserializeDictionary(dictionary: dictionary, anyObject: anyObject, conversionOptions: conversionOptions);
    }
    
    public static func deserializeDictionary(dictionary: NSDictionary, anyObject: NSObject, conversionOptions: ConversionOptions = .DefaultDeserialize)
    {
        EVReflection.setPropertiesfromDictionary(dictionary, anyObject: anyObject, conversionOptions: conversionOptions)
    }
    
    public static func deserializeArray<T: ApiObject>(json: String, conversionOptions: ConversionOptions = .DefaultDeserialize) -> [T]
    {
        let array = EVReflection.arrayFromJson(type: T(), json: json, conversionOptions: conversionOptions) as [T];
        return (array);
    }
    
    public static func deserializeArray<T: ApiObject>(array: NSArray, conversionOptions: ConversionOptions = .DefaultDeserialize) -> [T]
    {
        var result = [T]();
        for i in 0..<array.count
        {
            result.append(T(dictionary: array[i] as! NSDictionary));
        }
        return (result);
    }
    
    override func setValue(_ value: Any!, forUndefinedKey key: String)
    {
        //Derived classes can override as needed. This prevents needless warnings from EVReflection making concentrating on real warnings/messages difficult.
    }

}

extension NSDictionary {
    public func asInt(forKey: String, defaultValue: Int! = nil) -> Int!
    {
        if let value = self[forKey]
        {
            if CommonFunctions.isNull(s: value)
            {
                return (defaultValue);
            }
            
            if (value is UnsafePointer<Int>)
            {
                return ((value as! UnsafePointer<Int>).pointee);
            }
            else
            {
                return (value as! Int);
            }
        }
        
        return (defaultValue);
    }

    public func asBoolean(forKey: String, defaultValue: Bool! = nil) -> Bool!
    {
        if let value = self[forKey]
        {
            if CommonFunctions.isNull(s: value)
            {
                return (defaultValue);
            }
            
            return (value as! Bool);
        }
        
        return (defaultValue);
    }
}
