//
//  FAQ.swift
//  Uni Cosmetics
//
//  Created by Rajat on 13/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class FAQ:ApiObject
{
    var title = String()
    var content = String()
    var image = String()
}
