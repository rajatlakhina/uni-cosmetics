//
//  TabData.swift
//  Uni Cosmetics
//
//  Created by Rajat Lakhina on 22/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class TabData:ApiObject
{
    var title = String();
    var desc = String();
}
