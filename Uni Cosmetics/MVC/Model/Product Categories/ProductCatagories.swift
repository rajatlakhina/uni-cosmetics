//
//  ProductCatagories.swift
//  Uni Cosmetics
//
//  Created by Rajat on 21/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class ProductCatagories:ApiObject
{
    var id = String();
    var name = String();
    var banner = String();
    var product = [Product]();
}
