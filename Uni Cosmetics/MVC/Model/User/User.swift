//
//  SignUp.swift
//  Uni Cosmetics
//
//  Created by Rajat on 14/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import EVReflection

class User:ApiObject
{
    var id:String! = nil;
    var email = String();
    var registered = String();
    var status = String();
    var role = String();
    var first_name = String();
    var last_name = String();
    var profile_picture = String();
    var deviceType = String();
    var deviceToken = String();
    
    public func toDictionary(_ conversionOptions: ConversionOptions = .DefaultSerialize) -> NSDictionary
    {
        let dict = NSMutableDictionary();
        
        dict.setObject(self.id, forKey: "id" as NSCopying);
        dict.setObject(self.email, forKey: "email" as NSCopying);
        dict.setObject(self.registered, forKey: "registered" as NSCopying);
        dict.setObject(self.status, forKey: "status" as NSCopying);
        dict.setObject(self.role, forKey: "role" as NSCopying);
        dict.setObject(self.first_name, forKey: "first_name" as NSCopying);
        dict.setObject(self.last_name, forKey: "last_name" as NSCopying);
        dict.setObject(self.profile_picture, forKey: "profile_picture" as NSCopying);
        dict.setObject(self.deviceType, forKey: "deviceType" as NSCopying);
        dict.setObject(self.deviceToken, forKey: "deviceToken" as NSCopying);
        
        return dict
    }
    
    public func toJsonString(_ conversionOptions: ConversionOptions = .DefaultSerialize, prettyPrinted: Bool = false) -> String
    {
        let dict = self.toDictionary();
        return (JsonSerialization.getJsonString(obj: dict));
    }
}

