//
//  String.swift
//  Mart
//
//  Created by Rahul Singla on 12/02/18.
//  Copyright © 2018 Imbibe User. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.documentType.rawValue): NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.characterEncoding.rawValue): String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString.string)
    }
    
    public func capitalizingFirstLetter() -> String
    {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    public func lowercasedFirstLetter() -> String
    {
        let first = String(characters.prefix(1)).lowercased()
        let other = String(characters.dropFirst())
        return first + other
    }
    
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n)
        {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
    
    var htmlToAttributedString: NSAttributedString?
    {
        return Data(utf8).htmlToAttributedString
    }
    
    var htmlToString: String
    {
        return htmlToAttributedString?.string ?? ""
    }
    
    private func unescapeAsNumber() -> String? {
        
        let isHexadecimal = hasPrefix("#X") || hasPrefix("#x")
        let radix = isHexadecimal ? 16 : 10
        
        let numberStartIndex = index(startIndex, offsetBy: isHexadecimal ? 2 : 1)
        let numberString = self[numberStartIndex ..< endIndex]
        
        guard let codePoint = UInt32(numberString, radix: radix),
            let scalar = UnicodeScalar(codePoint) else {
                return nil
        }
        
        return String(scalar)
        
    }

    
    public var removingHTMLEntities: String {
        
        guard self.contains("&") else {
            return self
        }
        
        var result = String()
        var cursorPosition = startIndex
        
        while let delimiterRange = range(of: "&", range: cursorPosition ..< endIndex) {
            
            // Avoid unnecessary operations
            let head = self[cursorPosition ..< delimiterRange.lowerBound]
            result += head
            
            guard let semicolonRange = range(of: ";", range: delimiterRange.upperBound ..< endIndex) else {
                result += "&"
                cursorPosition = delimiterRange.upperBound
                break
            }
            
            let escapableContent = self[delimiterRange.upperBound ..< semicolonRange.lowerBound]
            let escapableContentString = String(escapableContent)
            let replacementString: String
            
            if escapableContentString.hasPrefix("#") {
                
                guard let unescapedNumber = escapableContentString.unescapeAsNumber() else {
                    result += self[delimiterRange.lowerBound ..< semicolonRange.upperBound]
                    cursorPosition = semicolonRange.upperBound
                    continue
                }
                
                replacementString = unescapedNumber
                
            } else {
                
                guard let unescapedCharacter = HTMLUnescapingTable[escapableContentString] else {
                    result += self[delimiterRange.lowerBound ..< semicolonRange.upperBound]
                    cursorPosition = semicolonRange.upperBound
                    continue
                }
                
                replacementString = unescapedCharacter
                
            }
            
            result += replacementString
            cursorPosition = semicolonRange.upperBound
            
        }
        
        // Append unprocessed data, if unprocessed data there is
        let tail = self[cursorPosition ..< endIndex]
        result += tail
        
        return result
        
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    var localized: String
    {
        return NSLocalizedString(self,comment: "");
    }
    
    /*mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }*/
    
    /*public func capitalizingWordFirstLetters() -> String {
        var array = self.components(separatedBy: " ");
        for i in 0..<array.count
        {
            array[i] = array[i].capitalizingFirstLetter();
        }
        
        return (array.joined(separator: " "));
    }*/
}
