//
//  JsonHandleClass.swift
//  Locus
//
//  Created by Imbibe-Desk42 on 29/06/17.
//  Copyright © 2017 Imbibe-Desk42. All rights reserved.
//

import Foundation

class JsonHandleClass: NSObject
{
    static func jsonObjToString(jsonObject: Any)->String
    {
        var JSONString = ""
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONStringTemp = String(data: jsonData, encoding: String.Encoding.utf8)
            {
                JSONString = JSONStringTemp
            }
        }
        catch{}
        return JSONString.replacingOccurrences(of: "\n", with: "")
    }
    
    static func stringToJsonObj(jsonString: String)->Any
    {
        var JSONObject: Any?
        do
        {
            let jsonData = jsonString.data(using: .utf8)
            if let JSONObjectTemp = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments) as? Any
            {
                JSONObject = JSONObjectTemp
            }
        }
        catch {}
        return JSONObject!
    }
    
}
