//
//  Common.swift
//  Umbria
//
//  Created by Imbibe Desk16 on 29/06/15.
//  Copyright (c) 2015 Imbibe Desk16. All rights reserved.
//

import UIKit

class JsonSerialization: NSObject
{
    static func getDictionaryFromJsonString(rowData:NSMutableData)-> Dictionary<String,AnyObject>
    {
        do
        {
            return try JSONSerialization.jsonObject(with: rowData as Data, options:  JSONSerialization.ReadingOptions.allowFragments) as! Dictionary
        }
        catch
        {
            return [:]
        }
    }
    
    static func getDictionaryFromJsonString(dictString:String)-> Dictionary<String,AnyObject>
    {
        do
        {
            return try  JSONSerialization.jsonObject(with: dictString.data(using: String.Encoding.utf8, allowLossyConversion: true)!, options:  JSONSerialization.ReadingOptions.allowFragments) as! Dictionary
        }
        catch
        {
            return [:]
        }
    }
    
    static func getArrayFromJsonString(arrayString:String)-> [[String : AnyObject]]
    {
        do
        {
            return try  JSONSerialization.jsonObject(with: arrayString.data(using: String.Encoding.utf8, allowLossyConversion: true)!, options:  JSONSerialization.ReadingOptions.allowFragments) as! [[String : AnyObject]]
        }
        catch
        {
            return [[:]]
        }
    }
    
    static func getJsonString(rowData:Data)-> String
    {
        return NSString(data: rowData, encoding: String.Encoding.utf8.rawValue)! as String
    }
    
    static func getJsonString(array : [[String : AnyObject]]) -> String
    {
        do
        {
            return try getJsonString(rowData: JSONSerialization.data(withJSONObject: array, options: JSONSerialization.WritingOptions()))
        }
        catch
        {
            return ""
        }
    }
    
    static func getJsonString(dictionary : Dictionary<String,AnyObject>) -> String
    {
        do
        {
            return try getJsonString(rowData: JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions()))
        }
        catch
        {
            return ""
        }
    }
    
    static func getJsonString(obj : Any) -> String
    {
        do
        {
            return try getJsonString(rowData: JSONSerialization.data(withJSONObject: obj, options: JSONSerialization.WritingOptions()))
        }
        catch
        {
            return ""
        }
    }
}
