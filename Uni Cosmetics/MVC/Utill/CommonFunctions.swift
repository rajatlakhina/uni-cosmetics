//
//  CommonFunctions.swift
//  FODDelivery
//
//  Created by Imbibe User on 22/08/17.
//  Copyright © 2017 Imbibe User. All rights reserved.
//

import UIKit
import SystemConfiguration
import Foundation

class CommonFunctions: NSObject
{
    // MARK:- ------- Singelton instance -------//
    static let sharedInstance = CommonFunctions()
    
    func CheckStoryboard() -> String
    {
            return "Main"
    }
    
    func secondsToHoursMinutesSeconds (seconds : Double) -> (Double, Double, Double, Double) {
        let days = (seconds / (3600 * 24))
        let (hr,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        return (days ,hr, min, 60 * secf)
    }
    
    static func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
//    static func stringFromTimeInterval(interval: TimeInterval) -> NSString {
//        
//        let ti = NSInteger(interval)
//        
//        let ms = Int((interval % 1) * 1000)
//        
//        let seconds = ti % 60
//        let minutes = (ti / 60) % 60
//        let hours = (ti / 3600)
//        
//        return NSString(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
//    }
    
    static func getCurrentDay() -> String
    {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString.lowercased()
    }
    
    static func roundToPlaces(value: Double, places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places));
        return round(value * divisor) / divisor;
    }
    
    static func date_converter(date:String) ->String
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy";
        
        let _date = dateFormat.date(from: date);
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd";
        
        return dateFormatter.string(from: _date!);
    }

    static func CheckNull(value : AnyObject?) -> Bool
    {
        if value is NSNull
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    static func isNull(s: Any?) -> Bool
    {
        if (s == nil || s is NSNull)
        {
            return (true);
        }
        if let sStr = s as? String
        {
            return (sStr == "<null>")
        }
        
        return (false);
    }
    
    static func isEmpty(s: String?, checkWhitespace: Bool = false) -> Bool
    {
        if (self.isNull(s: s))
        {
            return (true);
        }
        if s == ""
        {
            return (true)
        }
        if(checkWhitespace && s?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count == 0)
        {
            return (true);
        }
        
        return (false);
    }
    
    
    public static func getNestedValue(obj: [String:Any], props: [String],
                                    defaultValue: Any! = nil) -> Any!
    {
        var obj1 = obj;
        
        var i = 0;
        for prop in props
        {
            if let value1 = obj1[prop]
            {
                if CommonFunctions.isNull(s: value1)
                {
                    return (defaultValue);
                }
                else if i == props.count - 1
                {
                    return (value1);
                }
                else
                {
                    obj1 = (value1 as? [String : Any])!;
                }
            }
            else
            {
                return (defaultValue);
            }
            
            i += 1;
        }
        
        return (defaultValue);
    }
    
    public static func getNestedValueInt(obj: [String:Any], props: [String], defaultValue: Int = 0) -> Int
    {
        let value = self.getNestedValue(obj: obj, props: props, defaultValue: defaultValue);
        return (value as! Int);
    }
    
    static func setTextValueAndHandleHidden(value: String?, control: UILabel, controlWrapper: UIView? = nil, affixCurrencySymbol: Bool = true) -> Bool
    {
        var hide = false;
        if(self.isEmpty(s: value))
        {
            hide = true;
        }
        else
        {
            control.text = value;
        }
        
        if(controlWrapper == nil)
        {
            control.isHidden = hide;
        }
        else
        {
            controlWrapper?.isHidden = hide;
        }
        
        if(hide)
        {
            return (false);
        }
        else
        {
            return (true);
        }
    }
    
    static func trim(passingString:String) -> String
    {
        return passingString.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    static func showAlert(_ message:String,title:String,controller:UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWithMutipleActions(message:String, title:String, controller:UIViewController, firstBtnTitle:String, secondBtnTitle:String, completionHandler: @escaping (_ ActionNo:Int) -> ())
    {
        var _btnActionNo : Int = 0
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let Action_1 = UIAlertAction(title: firstBtnTitle, style: .default, handler:
            {(alert: UIAlertAction!) in
                _btnActionNo = 1
                completionHandler(_btnActionNo)
        })
        alertController.addAction(Action_1)
        
        if(secondBtnTitle != "")
        {
            let Action_2 = UIAlertAction(title: secondBtnTitle, style: .default, handler:
                {(alert: UIAlertAction!) in
                    _btnActionNo = 2
                    completionHandler(_btnActionNo)
            })
            alertController.addAction(Action_2)
        }
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func blurEffect(view: UIView)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
    static func setButtonTitleForState(button: UIButton, title: String, font: UIFont?, state: UIControlState)
    {
        button.setTitle(title, for: state)
        if font != nil
        {
            button.titleLabel?.font = font!
        }
    }
    
    static func shadow(view: UIView, shadowOpacity: Float, shadowOffset: CGSize, shadowRadius: CGFloat, shadowPath: CGPath?, shadowColor: UIColor)
    {
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowRadius = shadowRadius
        if shadowPath != nil
        {
            view.layer.shadowPath = shadowPath
        }
        view.layer.shadowColor = shadowColor.cgColor
    }
    
    static func shadow(view: UITableView, shadowOpacity: Float, shadowOffset: CGSize, shadowRadius: CGFloat, shadowPath: CGPath?, shadowColor: UIColor)
    {
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowRadius = shadowRadius
        if shadowPath != nil
        {
            view.layer.shadowPath = shadowPath
        }
        view.layer.shadowColor = shadowColor.cgColor
    }
    
    static func drawBorder(view:UITableView, width:CGFloat, color:UIColor)
    {
        view.layer.borderWidth = width;
        view.layer.borderColor = color.cgColor;
    }
    
    static func CheckNumberString(value:AnyObject) -> String
    {
        var valueString:String = ""
        
        if let result_number = value as? NSNumber
        {
            valueString = result_number.stringValue
        }
        else
        {
            valueString = "\(value)"
        }
        return valueString
    }
    
    static func CheckDoubleString(value:AnyObject) -> String
    {
        var valueString:String = ""
        
        if let result_number = value as? Double
        {
            valueString = "\(result_number)"
        }
        if let result_number = value as? String
        {
            valueString = "\(result_number)"
        }
        else
        {
            if value is NSNull || ("\(value)").isEmpty
            {
                valueString = "0"
            }
        }
        return valueString
    }

    static func parseDuration(_ timeString:String) -> TimeInterval
    {
        guard !timeString.isEmpty else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated()
        {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    // MARK:- ------ check internet connectivity via... LAN, WLAN, WIFI, HotSpot ------
    static func isConnected() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
            else
        {
            return false
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func FetchTimeDifference(startDate:Date, endDate:Date) -> Int
    {
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.minute], from: startDate, to: endDate)
        return components.minute!
    }
    
    func FetchFormattedDate(startTime:String, endTime:String) -> Double
    {
//        let startTime = "22:30:00"
//        let endTime = "23:00:00"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startFormattedTime = dateFormatter.date(from: startTime)
        let endFormattedTime = dateFormatter.date(from: endTime)
        //        dateFormatter.dateFormat = "hh:mm:ss a"
        //        var time2 = dateFormatter.string(from: fullDate!)
        
        let minutes = self.FetchTimeDifference(startDate:startFormattedTime!, endDate:endFormattedTime! )
        let timeValue:Double = (Double(minutes) * 60)
        
        return timeValue
    }
    
    func CheckDateFormat() -> String
    {
//        let currentDate = Date()
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.locale = NSLocale.current
        
        let locale = NSLocale.current
        let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: locale)!
        
        
        //------- example for use ------//
//        var time1: String = ""
//        if (dateFormat == "h a")
//        {
//            dateFormatter.dateFormat = "h:mm:ss a"
//            time1 = "11:30:00 am"
//        }
//        else
//        {
//            dateFormatter.dateFormat = "HH:mm:ss"
//            time1 = "11:30:00"
//        }
//        
//        let date1: Date? = dateFormatter.date(from: time1)
//        let current: Date? = dateFormatter.date(from: dateFormatter.string(from: currentDate))
//        
//        let result1: ComparisonResult? = current?.compare(date1!)
//        if result1 == .orderedAscending && result1 != .orderedSame
//        {}
        //-----------------------------//
        
        return dateFormat
    }
    
    func CheckRangeOfString(passedValue:String, stringToFind:String) -> Bool
    {
        let range = passedValue.range(of:stringToFind, options:.regularExpression)
        if range != nil
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func CreateImageFromViewArea(_view:UIView) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(_view.bounds.size, false, 0.0)
        _view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func requiredHeight(_label:UILabel) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:_label.frame.width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = _label.font
        label.text = _label.text
        label.sizeToFit()
        return label.frame.height
    }
    
    func RemoveControllersFromNavigationStack(controller:UIViewController) -> NSArray
    {
        //----- checking for navigation stack ------//
        var ArrayOfNav:Array = (controller.navigationController?.viewControllers)!
        
        repeat
        {
            ArrayOfNav.remove(at: ArrayOfNav.count-1)
        } while ArrayOfNav.count > 1
        
        controller.navigationController?.viewControllers = ArrayOfNav
        return ArrayOfNav as NSArray
    }
    
    
    // MARK:- --------- variable declaration ----------//
    
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x:0, y:0, width:size.width, height:size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    static func getLabelHeight(label: UILabel)->CGFloat
    {
        return label.sizeThatFits(label.bounds.size).height
    }
    
    static func removeTextViewPadding(textView: UITextView)
    {
        textView.textContainerInset = UIEdgeInsets.zero
        textView.textContainer.lineFragmentPadding = 0
    }
    
    static func setAndAdjustTextForLabel(label: UILabel, labelText: String)
    {
        label.text = labelText
        label.adjustsFontSizeToFitWidth = true
    }
    
    static func setFontTextLanguage(text:String) -> String
    {
        return NSLocalizedString(text, comment: "");
    }
    
    static func setPlaceHolderTextColor(labelPlaceholderText:String) -> NSAttributedString
    {
        return NSAttributedString(string: labelPlaceholderText,
                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.white]);
    }
    
    static func setPlaceHolderTextColor(labelPlaceholderText:String, color:UIColor) -> NSAttributedString
    {
        return NSAttributedString(string: labelPlaceholderText,
                                  attributes: [NSAttributedStringKey.foregroundColor: color]);
    }
    static func setTextAndAdjustHeightForLabel(label: UILabel, labelText: String)
    {
        label.text = labelText
        label.frame.size.height = label.sizeThatFits(label.bounds.size).height
    }
    
    static func setTextAndAdjustHeightForTextView(textView: UITextView, textViewText: String)
    {
        textView.text = textViewText
        textView.frame.size.height = textView.sizeThatFits(textView.bounds.size).height
    }
    
    // MARK:- ------- screen layout functions -------
    static func cornerRadius(view: UIView, cornerRadius: CGFloat, maskToBounds: Bool)
    {
        view.layer.masksToBounds = maskToBounds
        view.layer.cornerRadius = cornerRadius
    }
    
    static func setLayerProperties(view: UIView, maskToBounds: Bool?, cornerRadius: CGFloat?, borderColor: UIColor?, borderWidth: CGFloat?)
    {
        if maskToBounds != nil
        {
            view.layer.masksToBounds = maskToBounds!
        }
        else
        {
            view.layer.masksToBounds = false
        }
        if cornerRadius != nil
        {
            view.layer.cornerRadius = cornerRadius!
        }
        if borderColor != nil
        {
            view.layer.borderColor = borderColor!.cgColor
        }
        if borderWidth != nil
        {
            view.layer.borderWidth = borderWidth!
        }
    }
    
    static func addBorder(element: UIView, edge: UIRectEdge, color: UIColor, thickness: CGFloat)
    {
        let border = CALayer()
        switch edge
        {
        case UIRectEdge.top:
            border.frame = CGRect(x:0, y:0, width:element.frame.width, height:thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:element.frame.height - thickness, width:element.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width:thickness, height:element.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:element.frame.width - thickness, y:0, width:thickness, height:element.frame.height)
            break
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        element.layer.addSublayer(border)
    }
    
    static func addLayerWithFrameAndColor(element: UIView, frame: CGRect, color: UIColor)
    {
        let layer = CALayer()
        layer.frame = frame
        layer.backgroundColor = color.cgColor;
        element.layer.addSublayer(layer)
    }
    
    static func isPhoneNumberValid(value: String) -> Bool
    {
        let PHONE_REGEX = "^[0-9]{10}$"
        let phoneNumberTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneNumberTest.evaluate(with: value)
        return result
    }
    
    static func isValidEmail(email:String) -> Bool
    {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,64}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func isPasswordValid(password:String) -> Bool
    {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{6,}")
        return passwordTest.evaluate(with: password)
    }
    
}
