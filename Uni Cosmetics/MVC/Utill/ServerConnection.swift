//
//  ServerConnection.swift
//  FODDelivery
//
//  Created by Imbibe User on 22/08/17.
//  Copyright © 2017 Imbibe User. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ServerConection: NSObject
{
    var _obj_appDelegate: AppDelegate?
    
    // MARK:- ------- Singelton instance -------//
    static let sharedInstance = ServerConection()
    
    func initializeObjects()
    {
        _obj_appDelegate = UIApplication.shared.delegate as? AppDelegate
    }
    
    static func toQueryString(dict: [String: AnyObject]) -> String
    {
        var keyValuePairObjArray = [String]();
        for (type, value) in dict
        {
            keyValuePairObjArray.append("\(type)=\("\(value)".replacingOccurrences(of: "&", with: "%26").replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "+", with: "+"))");
        }
        
        let paramString = keyValuePairObjArray.joined(separator: "&");
        
        return (paramString);
    }
    
    static func makeApiCall(strURL: String,
                            parameters: [String: AnyObject],
                            header_parameters: [String: String],
                            controller: UIViewControllerBase,
                            completionHandler: @escaping ((_ dict: NSDictionary) -> ()),
                            errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        let url = _BaseURL + strURL;
        
        
        print("--------------------------------------------")
        print("==========   " + url)
        print(parameters)
        print("--------------------------------------------")
        print(header_parameters)
        print("--------------------------------------------")
        //------------------ Request using Alamofire --------------------//
        var request = URLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.timeoutInterval = 30.0
        
        let paramString = self.toQueryString(dict: parameters);
        request.allHTTPHeaderFields = header_parameters;
        request.httpBody = paramString.data(using: String.Encoding.utf8)!
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        if CommonFunctions.isConnected()
        {
            Alamofire.request(request as URLRequestConvertible).responseJSON
                { response in
                    switch (response.result)
                    {
                    case .success:
                        if let dict:NSDictionary = response.result.value as? NSDictionary
                        {
//                            if !CommonFunctions.isNull(s: dict.object(forKey: "status"))
//                            {
                            
                                let isSuccess = response.response?.statusCode
                                if(isSuccess == 200)
                                {
                                    completionHandler(dict)
                                }
                                else if controller is AddressListViewController && (isSuccess == 404)
                                {
                                    completionHandler(dict)
                                }
                                else
                                {
                                    errorHandler(dict, nil, nil);
                                }
//                            }
//                            else
//                            {
//                                controller.stopLoader();
//                                Toast().showToast(message: _somethingWrongError, duration: 3);
//                            }
                        }
                        break
                        
                    case .failure(let error):
                        
                        errorHandler(nil, error, nil);
                        controller.stopLoader();
                        Toast().showToast(message: error.localizedDescription as! String, duration: 3);
                        //                    //NVActivityIndicatorPresenter.sharedInstance.stopAnimating();
                        //                    Toast().showToast(message: "The network connection is lost.", duration: 3);
                        //                    print(error);
                        // errorHandler(nil, error, response);
                        break
                    }
            }
        }
        else
        {
            controller.stopLoader();
//            if StateHelper.getLang() == "1"
//            {
            Toast().showToast(message: "Your device does not have working internet connection !!", duration: 3)
//                controller.showError(msg: );
//            }
//            else
//            {
//                controller.showError(msg: "جهازك غير متصل بالإنترنت");
//            }
        }
    }
    
    static func makeGetApiCall(strURL: String,
                            parameters: [String: AnyObject],
                            header_parameters: [String: String],
                            controller: UIViewControllerBase,
                            completionHandler: @escaping ((_ dict: NSDictionary) -> ()),
                            errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        let url = _BaseURL + strURL;
        
        
        print("--------------------------------------------")
        print("==========   " + url)
        print(parameters)
        print("--------------------------------------------")
        print(header_parameters)
        print("--------------------------------------------")
        //------------------ Request using Alamofire --------------------//
        var request = URLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.timeoutInterval = 30.0
        
        let paramString = self.toQueryString(dict: parameters);
        request.allHTTPHeaderFields = header_parameters;
        request.httpBody = paramString.data(using: String.Encoding.utf8)!
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        if CommonFunctions.isConnected()
        {
            Alamofire.request(request as URLRequestConvertible).responseJSON
                { response in
                    switch (response.result)
                    {
                    case .success:
                        if let dict:NSDictionary = response.result.value as? NSDictionary
                        {
                            //                            if !CommonFunctions.isNull(s: dict.object(forKey: "status"))
                            //                            {
                            
                            let isSuccess = response.response?.statusCode
                            if(isSuccess == 200)
                            {
                                completionHandler(dict)
                            }
                            else if controller is AddressListViewController && (isSuccess == 404)
                            {
                                completionHandler(dict)
                            }
                            else
                            {
                                errorHandler(dict, nil, nil);
                            }
                            //                            }
                            //                            else
                            //                            {
                            //                                controller.stopLoader();
                            //                                Toast().showToast(message: _somethingWrongError, duration: 3);
                            //                            }
                        }
                        break
                        
                    case .failure(let error):
                        
                        errorHandler(nil, error, nil);
                        controller.stopLoader();
                        Toast().showToast(message: error.localizedDescription as! String, duration: 3);
                        //                    //NVActivityIndicatorPresenter.sharedInstance.stopAnimating();
                        //                    Toast().showToast(message: "The network connection is lost.", duration: 3);
                        //                    print(error);
                        // errorHandler(nil, error, response);
                        break
                    }
            }
        }
        else
        {
            controller.stopLoader();
            //            if StateHelper.getLang() == "1"
            //            {
             Toast().showToast(message: "Your device does not have working internet connection !!", duration: 3)
            //            }
            //            else
            //            {
            //                controller.showError(msg: "جهازك غير متصل بالإنترنت");
            //            }
        }
    }
    
    static func registerAndUploadImage(_ strURL:String,method:String,header_parameters: [String: String],parameters:[String: AnyObject], image:UIImage,controller:UIViewControllerBase, completionHandler: @escaping (_ response: NSDictionary) -> (),errorHandler: @escaping (_ error: NSError) -> ())
    {
        let parameters1 = parameters;
        
        print("--------------------------------------------")
        print(">>>>>>>" + strURL)
        print(parameters1)
        print("--------------------------------------------")
        
        if CommonFunctions.isConnected()
        {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                //------- change "User.sharedInstance.image!" with required value ------//
                //            if  let imageData = UIImageJPEGRepresentation(User.sharedInstance.image!, 0.6)
                
                let date = Date();
                
                let file_Name = String(format: "iOSid%@(%ld)img.jpeg", "StateHelper.getUserId()", Int(date.timeIntervalSince1970))
                
                if  let imageData = UIImageJPEGRepresentation(image, 0.6)
                {
                    multipartFormData.append(imageData, withName: "image", fileName: file_Name.replacingOccurrences(of: ".", with: "_").replacingOccurrences(of: "_jpeg", with: ".jpeg").replacingOccurrences(of: "_", with: ""), mimeType: "image/jpeg")
                }
                for (key, value) in parameters1
                {
                    multipartFormData.append(String(describing: value).data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to: strURL, headers: header_parameters, encodingCompletion: { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON
                        { response in
                            if let JSON:NSDictionary = response.result.value as? NSDictionary
                            {
                                
                                completionHandler(JSON)
                            }
                            else
                            {
                                
                                errorHandler(response.result.error! as NSError)
                            }
                            
                    }
                    
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Loader.hideLoader(controller: controller);
                    CommonFunctions.showAlert("\n\nAuth request failed with error:\n The network connection is lost", title: _errorTitle, controller: controller)
                }
                
            })
        }
        else
        {
            controller.stopLoader();
//            if StateHelper.getLang() == "1"
//            {
                 Toast().showToast(message: "Your device does not have working internet connection !!", duration: 3)
//            }
//            else
//            {
//                controller.showError(msg: "جهازك غير متصل بالإنترنت");
//            }
        }
    }
    
    static func registerAndUploadImage(_ strURL:String,method:String,parameters:[String: AnyObject],header_parameters: [String: String], image:UIImage,id_image:UIImage,licence_image:UIImage,registeration_image:UIImage,controller:UIViewControllerBase, completionHandler: @escaping (_ response: NSDictionary) -> (),errorHandler: @escaping (_ error: NSError) -> ())
    {
        let parameters1 = parameters;
        
        print("--------------------------------------------")
        print(">>>>>>>" + strURL)
        print(parameters1)
        print("--------------------------------------------")
        if CommonFunctions.isConnected()
        {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                //------- change "User.sharedInstance.image!" with required value ------//
                //            if  let imageData = UIImageJPEGRepresentation(User.sharedInstance.image!, 0.6)
                
                let date = Date();
                
                let file_Name = String(format: "iOSid%@(%ld)img.jpeg", "StateHelper.getUserId()", Int(date.timeIntervalSince1970))
                
                if  let imageData = UIImageJPEGRepresentation(image, 0.6)
                {
                    multipartFormData.append(imageData, withName: "image", fileName: file_Name, mimeType: "image/jpeg")
                }
                
                let date1 = Date();
                
                let id_image_name = String(format: "iOSid%@(%ld)id_image.jpeg", "StateHelper.getUserId()", Int(date1.timeIntervalSince1970))
                
                if  let imageData = UIImageJPEGRepresentation(id_image, 0.6)
                {
                    multipartFormData.append(imageData, withName: "id_image", fileName: id_image_name, mimeType: "image/jpeg")
                }
                
                let date2 = Date();
                
                let licence_image_name = String(format: "iOSid%@(%ld)licence_image.jpeg", "StateHelper.getUserId()", Int(date2.timeIntervalSince1970))
                
                if  let imageData = UIImageJPEGRepresentation(licence_image, 0.6)
                {
                    multipartFormData.append(imageData, withName: "licence_image", fileName: licence_image_name, mimeType: "image/jpeg")
                }
                
                let date3 = Date();
                
                let registeration_image_name = String(format: "iOSid%@(%ld)registeration_image.jpeg", "StateHelper.getUserId()", Int(date3.timeIntervalSince1970))
                
                if  let imageData = UIImageJPEGRepresentation(registeration_image, 0.6)
                {
                    multipartFormData.append(imageData, withName: "registeration_image", fileName: registeration_image_name, mimeType: "image/jpg")
                }
                for (key, value) in parameters1
                {
                    multipartFormData.append(String(describing: value).data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }, to: strURL, headers: header_parameters, encodingCompletion: { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON
                        { response in
                            if let JSON:NSDictionary = response.result.value as? NSDictionary
                            {
                                
                                completionHandler(JSON)
                            }
                            else
                            {
                                
                                errorHandler(response.result.error! as NSError)
                            }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Loader.hideLoader(controller: controller);
                    Toast().showToast(message: "Auth request failed with error: \nThe network connection is lost", duration: 3);
                }
                
            })
        }
        else
        {
            controller.stopLoader();
//            if StateHelper.getLang() == "1"
//            {
                 Toast().showToast(message: "Your device does not have working internet connection !!", duration: 3)
//            }
//            else
//            {
//                controller.showError(msg: "جهازك غير متصل بالإنترنت");
//            }
        }
    }
}
