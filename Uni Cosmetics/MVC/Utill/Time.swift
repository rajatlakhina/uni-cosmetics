//
//  Time.swift
//  Bubble Car Wash
//
//  Created by Orem on 24/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import Foundation

struct Time {
    
    var startDate: Date
    var endDate: Date
    var step: DateComponents
    
    var calendar = Calendar.autoupdatingCurrent
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    init(startDate: Date, endDate: Date, step: DateComponents) {
        self.startDate = startDate
        self.endDate   = endDate
        self.step      = step
    }
    
    var timeIntervals : [Date] {
        guard self.startDate <= self.endDate else {
            return []
        }
        
        var result = [self.startDate]
        var date = self.startDate
        while date < self.endDate {
            date = self.calendar.date(byAdding: step, to: date)!
            result.append(date)
        }
        
        return result
    }
    
    var timeRepresentation : [String]
    {
        return self.timeIntervals.map { self.dateFormatter.string(from: $0) }
    }
}
