//
//  GoogleHelper.swift
//  Mart
//
//  Created by Rahul Singla on 21/12/17.
//  Copyright © 2017 Imbibe User. All rights reserved.
//

import Foundation

class GoogleHelper
{
    static func executePlacesSearch(params: [String: AnyObject],
        completionHandler: @escaping ((_ predictions: [[String: Any]]) -> ())) -> URLSessionDataTask
    {
        var params1 = params;
        params1["key"] = googleApiKey as AnyObject;
        params1["country"] = "UK" as AnyObject;
        params1["type"] = "establishment" as AnyObject;
        params1["strictbounds"] = "true" as AnyObject;
        
        let paramString = ServerConection.toQueryString(dict: params1);
        
        let url = URL(string: _googleAPIlink + "?" + paramString);
        
        let task = URLSession.shared.dataTask(with:url!, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.global(qos: .userInitiated).async
                {
                    DispatchQueue.main.async
                        {
                            do
                            {
                                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any];
                                let predictions = json["predictions"] as? [[String: Any]] ?? [];
                                
                                completionHandler(predictions);
                            }
                            catch let error as NSError
                            {
                                print(error)
                            }
                    }
            }
        });
        
        task.resume();
        
        return (task);
    }
    
    static func getPlaceDetails(place_id: String,
        completionHandler: @escaping ((_ data: Data) -> ()))
    {
        let originalString = String(format:"https://maps.googleapis.com/maps/api/place/details/json?key=%@&place_id=%@", googleApiKey, place_id);
        let url = URL(string: originalString.replacingOccurrences(of: " ", with: "%20"))
        URLSession.shared.dataTask(with:url!, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.global(qos: .userInitiated).async
                {DispatchQueue.main.async
                    {
                        completionHandler(data);
                    }
            }
        }).resume()
    }
}
