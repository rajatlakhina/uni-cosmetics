//
//  TimeInterval.swift
//  Uni Cosmetics
//
//  Created by Rajat on 30/10/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation

extension TimeInterval
{
    private var milliseconds: Int
    {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    private var seconds: Int
    {
        return (Int(self) % 60)
    }
    
    private var minutes: Int
    {
        return ((Int(self) / 60 ) % 60)
    }
    
    private var hours: Int
    {
        return (Int(self) / 3600)
    }
    
    private var secondsDay: Int
    {
        return ((Int(self) % 3600) % 60)
    }
    
    private var minutesDay: Int
    {
        return ((Int(self) % 3600 ) / 60)
    }
    
    private var hoursDay: Int
    {
        return ((Int(self) % 86400) / 3600)
    }
    
    private var days: Int
    {
        return (Int(self) / 86400) / 30
    }
    
    var stringTime: String
    {
        if hours != 0
        {
            return "\(hours): \(minutes): \(seconds)"
            //return "\(hours)h \(minutes)m \(seconds)s"
        }
        else if minutes != 0
        {
            return "00: \(minutes): \(seconds)"
            //return "\(minutes)m \(seconds)s"
        }
//        else if milliseconds != 0
//        {
//            return "\(seconds)s \(milliseconds)ms"
//        }
        else
        {
            return "00: 00: \(seconds)"
            //return "\(seconds)"
        }
    }
    
    var stringDaysTime: String
    {
        if days != 0
        {
            return "\(days): \(hoursDay): \(minutesDay): \(secondsDay)"
        }
        if hoursDay != 0
        {
            return "00: \(hoursDay): \(minutesDay): \(secondsDay)"
            //return "\(hours)h \(minutes)m \(seconds)s"
        }
        else if minutesDay != 0
        {
            return "00: 00: \(minutesDay): \(secondsDay)"
            //return "\(minutes)m \(seconds)s"
        }
            //        else if milliseconds != 0
            //        {
            //            return "\(seconds)s \(milliseconds)ms"
            //        }
        else
        {
            return "00: 00: 00: \(secondsDay)"
            //return "\(seconds)"
        }
    }
}
