//
//  Data.swift
//  Crap Chronicles
//
//  Created by Orem on 07/09/18.
//  Copyright © 2018 Orem. All rights reserved.
//

import Foundation
import UIKit
extension Data
{
    var htmlToAttributedString: NSAttributedString?
    {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
