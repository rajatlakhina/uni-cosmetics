//
//  ApiHelper.swift
//  Uni Cosmetics
//
//  Created by Rajat on 13/12/18.
//  Copyright © 2018 Rajat. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection

class ApiHelper
{
    public static func aboutUs(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((AboutUs) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeApiCall(strURL: _aboutUs, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = AboutUs(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval)
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
        
    }
    
    public static func getHomeData(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((Home) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _home, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = Home(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getProductDetails(
        lang:String,
        id:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((NSDictionary) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _productDetails + id, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = (data["content"] as! NSDictionary);
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getCategoriesData(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (([Catagory]) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _getCategories, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = ApiObject.deserializeArray(array: (data["content"] as! NSArray)) as [Catagory]
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getUpcomingDeals(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (([Product]) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _getUpcomingDeals, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = ApiObject.deserializeArray(array: (data["content"] as! NSArray)) as [Product]
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getCategoriesList(
        lang:String,
        cat_id:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (([ProductCatagories]) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _getCatagoryData+cat_id, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = ApiObject.deserializeArray(array: (data["content"] as! NSArray)) as [ProductCatagories]
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getProductList(
        lang:String,
        cat_id:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((ProductCatagories) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _getProductList+cat_id, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = ProductCatagories(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getBrands(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (([Brand]) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _getBrands, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = ApiObject.deserializeArray(array: (data["content"] as! NSArray)) as [Brand]
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func getAllDeals(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((Home) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeGetApiCall(strURL: _allDeals, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let content = Home(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(content)
        }, errorHandler: errorHandler)
    }
    
    public static func faq(
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((FAQ) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeApiCall(strURL: _faqs, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            let data = (dict["data"] as! NSDictionary)
            let msg = FAQ(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(msg)
        }, errorHandler: errorHandler)
        
    }
    
    public static func information(
        url:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping ((NSDictionary) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["lang": lang];
        ServerConection.makeApiCall(strURL: url, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let msg = data["content"] as! NSDictionary;
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            completionHandler(msg)
        }, errorHandler: errorHandler)
        
    }
    
    public static func signUp(
        first_name:String,
        last_name:String,
        email:String,
        password:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["email": email, "first_name": first_name, "last_name": last_name, "password": password, "lang":lang];
        ServerConection.makeApiCall(strURL: _signUp, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let msg = User(dictionary: (data["content"] as! NSDictionary));
            
            CartHelper.setCurrentUser(user: msg)
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func login(
        email:String,
        password:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["email": email, "password": password, "lang": lang,"deviceType": "iOS", "deviceToken": StateHelper.getDeviceToken()];
        ServerConection.makeApiCall(strURL: _login, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let msg = User(dictionary: (data["content"] as! NSDictionary));
            
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            CartHelper.setCurrentUser(user: msg);
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func forgotPassword(
        email:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["useremail": email, "lang": lang];
        ServerConection.makeApiCall(strURL: _forgotPassword, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            //let data = (dict["data"] as! NSDictionary)
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func changePassword(
        user_id:String,
        oldPassword:String,
        newPassword:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["user": user_id,"password": oldPassword, "newpassword": newPassword,"lang":lang];
        ServerConection.makeApiCall(strURL: _changePassword, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func editProfile(
        user:String,
        first_name:String,
        last_name:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["user_id": user, "first_name": first_name, "last_name": last_name, "lang": lang];
        ServerConection.makeApiCall(strURL: _editProfile, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            //let data = (dict["data"] as! NSDictionary)
            
            
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            
            let data = (dict["data"] as! NSDictionary)
            let msg = User(dictionary: (data["content"] as! NSDictionary));
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            let current_user = CartHelper.getCurrentUser();
            current_user.first_name = msg.first_name;
            current_user.last_name = msg.last_name;
            
            CartHelper.setCurrentUser(user: current_user);
            
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func logout(
        user_id:String,
        lang:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["user_id": user_id, "lang": lang];
        ServerConection.makeApiCall(strURL: _logout, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            completionHandler()
        }, errorHandler: errorHandler)
        
    }
    
    public static func getAddress(
        user_id:String,
        lang:String,
        type:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (([Address]) -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["user_id": user_id, "lang": lang, "type": type];
        ServerConection.makeApiCall(strURL: _getAddress, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let unixTimestamp = (data["server_time"] as! TimeInterval);
            _serverTimeInterval = unixTimestamp;
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            if !CommonFunctions.isNull(s: data["content"])
            {
                let msg = ApiObject.deserializeArray(array: (data["content"] as! NSArray)) as [Address]
                completionHandler(msg)
            }
            else
            {
                let msg = [Address]()
                completionHandler(msg)
            }
            //Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            
        }, errorHandler: errorHandler)
        
    }
    
    public static func saveAddress(
        lang:String,
        address:Address,
        delete:String,
        controller: UIViewControllerBase,
        completionHandler: @escaping (() -> ()),
        errorHandler: @escaping ((_ dict: NSDictionary?, _ error: Error?, _ response: DataResponse<Any>?) -> ()))
    {
        
        let dictBody:[String:Any] = ["user_id": address.user_id, "lang": lang, "type": address.type, "id" : address.id, "first_name": address.first_name, "last_name": address.last_name, "company":address.company, "address_1": address.address_1, "address_2":address.address_2, "city": address.city, "state": address.state, "country": address.country, "email": address.email, "postcode": address.postcode, "phone": address.phone, "delete": delete];
        ServerConection.makeApiCall(strURL: _saveAddress, parameters: dictBody as [String:AnyObject], header_parameters: [:], controller: controller , completionHandler:{
            (dict) -> () in
            
            let data = (dict["data"] as! NSDictionary)
            let unixTimestamp = (dict["server_time"] as! TimeInterval);
            ConstantFunctions.setServerTime(unixTimestamp: unixTimestamp);
            
            Toast().showToast(message: dict["mesg"] as! String, duration: 3);
            
            completionHandler()
            
        }, errorHandler: errorHandler)
        
    }
}
